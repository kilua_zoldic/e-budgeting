<?php
include "config/koneksi.php";
include "control/class.php";

$company = new Master();
session_start();
$_SESSION['sessionid'];
//if(empty($_SESSION['username'])){
//	header ('location:index.php');
//}
//else {
	
	?>

<!DOCTYPE html>

<html>
<head>

	<meta charset="UTF-8">
	<title>e-Budgeting</title>
	<meta http-equiv="Pragma" content="cache">
	<link rel="stylesheet" type="text/css" href="assets/themes/metro-blue/easyui.css">
	<link rel="stylesheet" type="text/css" href="assets/themes/icon.css">

	<!--<link rel="stylesheet" type="text/css" href="demo.css">-->
	
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/1.5jquery.easyui.min.js"></script>
	<script type="text/javascript" src="assets/js/glob.js"></script>

	
	
	
<!---SSO-->	

<script>
 var idgrid = '#menu_tree';	

</script>

<style>

 
select option:disabled {
	color: #000;
}
.ui-button { margin-left: -1px; }
.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; }

	
</style>
</head>
<body onload="addTab('Dashboard','content.php');">

 <div id="cc" class="easyui-layout" fit="true" border="false" style="width:100%;height:100%;"> 


    

 
 <div data-options="region:'north',split:false" collapsible="false" style="height:76px;">
	<div class="easyui-panel"  style="padding:5px;width:100%;" border="false">
		<!--<a href="javascript:void(0)" onclick="addTab('Home','content.php');" class="easyui-linkbutton" data-options="iconCls:'icon-home',plain:true">Home</a>
		<a href="#" class="easyui-menubutton" data-options="menu:'#mm1',iconCls:'icon-edit'">Action</a>-->
		<!--<span style="float:right;" data-options="">Sales System V 2.1</span>-->
<!--		<span  style="float:left;padding:5px;" ><img src="images/Untitledcopy.png" style="height:48px;" /></span>-->
		<span style="float:right;margin-top:20px" data-options="">
		<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',iconCls:'icon-accmanag'">
		<?php echo $_SESSION['username'];?></a></span>
		
	</div>
	
	
	 <div id="mm2" style="width:150px;">
		<div data-options="iconCls:'icon-redo'"><a href="javascript:void(0)" onclick="addTab('Profile','content.php?act=profile');" class="easyui" style="text-decoration: none;color: #000;" ><span>Profile</span></a></div>
        <div data-options="iconCls:'icon-undo'" ><a href="logout.php?destroy=<?php echo $_SESSION['sessionid'];?> " style="text-decoration: none;color: #000;">Log Out</a></div>
    </div>	
  </div>  

   <div class="easyui-accordion" data-options="region:'west',split:false" collapsible="true" style="width:220px;">
<?php if($_SESSION['id_level'] == '1'){?>
   <div title="Core System Configuration" data-options="split:true,iconCls:'icon-setting'" style="overflow:auto;padding:5px;">
	<ul class="easyui-tree" >
	<li data-options="state:'closed'"><span>Menu Management</span>
		<ul >
		<!--<li data-options="iconCls:'icon-files'">
			<a href="javascript:void(0)" onclick="addTab('Module','content.php?act=modul');" class="easyui" style="text-decoration: none;color: #000;" ><span>Module</span></a>
		</li>
		<li data-options="iconCls:'icon-files'">
			<a href="javascript:void(0)" onclick="addTab('Master Menu','content.php?act=master_menu');" class="easyui" style="text-decoration: none;color: #000;" ><span>Master Menu</span></a>
		</li>-->
		<li data-options="iconCls:'icon-files'">
			<a href="javascript:void(0)" onclick="addTab('Maintain Menu','content.php?act=tree');" class="easyui" style="text-decoration: none;color: #000;" ><span>Maintain Menu</span></a>
		</li>
		<li data-options="iconCls:'icon-files'">
			<a href="javascript:void(0)" onclick="addTab('Upload Module','content.php?act=upmodul');" class="easyui" style="text-decoration: none;color: #000;" ><span>Upload Module</span></a>
		</li>
		</ul>
		</li>
		<li data-options="state:'closed'"><span>Roles Management</span>
		<ul >
		<li data-options="iconCls:'icon-files'">
			<a href="javascript:void(0)" onclick="addTab('Maintain Role','content.php?act=level');" class="easyui" style="text-decoration: none;color: #000;" ><span>Maintain Roles</span></a>
		</li>
		<li data-options="iconCls:'icon-files'">
			<a href="javascript:void(0)" onclick="addTab('Access Control List','content.php?act=acl');" class="easyui" style="text-decoration: none;color: #000;" ><span>Access Control List</span></a>
		</li>
		</ul>
		</li>
		
			</ul>
			

	</div>
	<!--data-options='state:\"closed\"'-->
	<?php } else { }
	$data = $company->oyee(); 
  
	
    function get_menu( &$site_url, $data, $parent = 0) {  
              $i = 1;  
              $html = " <ul id='menu_tree'>";   
              if (@$data[$parent]) {   
                  if($parent > 0)$html = " <ul >"; 
					
                  $i++;  
                  foreach ($data[$parent] as $v) {  
                       $child = get_menu( $site_url, $data, $v->id);  
                       $html .= "<li >";  
							
							
                           if($parent > 0 && $v->menu !="") $html .= "<span><a href='javascript:void(0);' onclick='addTab(\"$v->title\",[\"content.php?act=$v->id\"]);' class='easyui' style='text-decoration: none;color: #000;'>$v->title</a> </span>";
                          
						   else if($v->menu !="") $html .= "<span><a href='javascript:void(0);' onclick='addTab(\"$v->title\",[\"content.php?act=$v->id\"]);' class='easyui' style='text-decoration: none;color: #000;'>$v->title</a> </span>";
						   else $html .= "<span>$v->title</span>";  
							
                           if ($child) {  
                               $i--;  
                               $html .= $child;  
                           } 
						   
                       $html .= '</li>';  
                  }  
                 // if($parent > 0)$html .='<li></li>';  
                  $html .= "</ul>";  
                  return $html;  
              }   
            else {  
                  return false;  
              }  
          }   
	?>
  <div title="Home" data-options="split:true,iconCls:'icon-home'" style="overflow:auto;padding:5px;">
<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="expandAll(idgrid)" style="width:80px;">Expand All</a>
<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="collapseAll(idgrid)" style="width:90px;">Collapse All</a>

  <?php  
		    echo get_menu($site_url,$data);  
	?>
	</div>  
    <div title="Calendar" data-options="iconCls:'icon-calendar'"  class="easyui-calendar" style="width:200px;height:40%;"></div>
	</div>  
 <div class="easyui-tabs" data-options="region:'center',border:false"  style="width:100%;height:100%;" id="tt" >
	<div title="Dashboard" fit="true" plain="true" border="false" style="height:100%;width:100%;">
	<?php
//select count(*) as tot, b.name_tbp from document_tree a inner join tabel_bp b on a.id_tbp = b.id_tbp where a.parentId = '0' group by a.id_tbp
// now we are in 3rd row
//$get = mysql_query("select id from document_tree where parentId = '0'");
//while ($h = mysql_fetch_array($get)){
//$query = "update document_tree
//cross join (select @rownumber := 0) r
//set refno = (@rownumber := @rownumber + 1)
//where parentId='0' ";
//$result = mysql_query($query);
//}
//where parentId='$h[id]'";
/*$get = mysql_query("select id_tbp from tabel_bp ");
while ($h = mysql_fetch_array($get)){
$query="UPDATE document_tree AS t
JOIN
(
    SELECT @rownum:=@rownum+1 rownum, id, refno
    FROM document_tree
    CROSS JOIN (select @rownum := 0) r
    WHERE parentId='0' and id_tbp='$h[id_tbp]'
) AS r ON t.id = r.id
SET t.refno = r.rownum";
$result = mysql_query($query);
}*/
/*
$m ="
UPDATE document_tree,
(
SELECT  @rownum:=@rownum+1 rownum,id_tbp, count( * ) AS ac
FROM document_tree
CROSS JOIN (select @rownum := 0) r
where parentId='0'
GROUP BY id_tbp
) AS t1
SET document_tree.refno = t1.rownum WHERE document_tree.id_tbp = t1.id_tbp AND document_tree.parentId = '0'";
mysql_query($m);*/
?>
 </div>
 
 </div>  

</body>


</html>
<?php
//}
?>