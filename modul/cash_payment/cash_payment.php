<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 12/18/16
 * Time: 3:47 PM
 */
?>

    <table id="detkeg"
           data-options="singleSelect:true,fit:true,fitColumns:true"
           showFooter="true" toolbar="#tb" border="false" idField="id_payment_master_group" sortName="payment_no" pagination="true"
           rownumbers="true" pageSize="50"  maximizable="true"  style="width:100%;height:250px">
        <thead>
        <tr>
            <th data-options="field:'check_date',editor:{type:'datebox',options:{required:true,validType:'validDate[\'dd-MM-yyyy\']' }}" width="150">Date</th>
            <th data-options="field:'id_payment_master_group'," hidden="true" hwidth="80">ID</th>
            <th data-options="field:'payment_no',sortable:true" width="150">Payment No</th>
            <th data-options="field:'rek_no',sortable:true" width="150">No Rek</th>
            <th data-options="field:'status_paid',formatter:payment_method,sortable:true" width="150">Payment Method</th>
            <th data-options="field:'bank_name',sortable:true" width="150">Bank</th>
            <th data-options="field:'paid',align:'right',formatter:formatPrice,sortable:true" width="130">Paid</th>
            <th data-options="field:'settlement',align:'right',formatter:formatPrice,sortable:true" width="130">Settlement</th>
            <th data-options="field:'total_settlement',align:'right',formatter:formatPrice,sortable:true" width="130">Total Settlement</th>
            <th data-options="field:'cash_advance',align:'right',formatter:formatPrice,sortable:true" width="130">CA</th>
            <th data-options="field:'balance',align:'right',formatter:formatPrice,sortable:true" width="150">Balance</th>
        </tr>
        </thead>
    </table>
<!--    <div id="tb" style="padding:2px 5px;">-->
<!--        Date From: <input class="easyui-datebox" style="width:110px"/>-->
<!--        To: <input class="easyui-datebox" style="width:110px"/>-->
<!--        Bank:-->
<!--        <input id="bankid" style="width:100px;" data-options="prompt:'Bank.'" name="bank_name"/>-->
<!--        Payment No: <input type="text" id="payment_no" name="payment_no"/>-->
<!--        <a href="#" class="easyui-linkbutton" iconCls="icon-search">Search</a>-->
<!--    </div>-->


<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#printca').linkbutton('disable');
    });
</script>
<script>
//    $('#care').combobox({
//        valueField: 'care',
//        onSelect: function (rec) {
//            $('#detkeg').edatagrid({
//                url:'modul/cash_payment/cash_paymentaction.php?act=getlist',
//                pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
//                emptyMsg: 'No Records Found',
//            });
//        }
//    });
    $('#detkeg').edatagrid({
        url:'modul/cash_payment/cash_paymentaction.php?act=getlist',
        pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
        emptyMsg: 'No Records Found',
        updateUrl: 'modul/cash_payment/cash_paymentaction.php?act=update',
        onSuccess:function(index,row){
            $.messager.show({
                title:'Info',
                msg:row.success
            });
            $('#detkeg').edatagrid('reload');
        },
        onError: function(index,row){
            $.messager.show({
                title:'Info',
                msg: row.errorMsg
            });
        }
    }).edatagrid('enableCellEditing').edatagrid('gotoCell', {
        index: 0,
        field: 'id_payment_master_group'
    });

    $('#bankid').combobox({
        url:'modul/bank/bankaction.php?act=listbank',
        valueField:'idbank',
        textField:'bank_name'
    });

    $('#payment_no').combobox({
        url:'control/view.php?act=getCode',
        valueField:'payment_no',
        textField:'payment_no'
    });

    function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.refno + '</span><br/>' +
            '<span style="color:#888"> Nama Kegiatan : ' + row.nama_kegiatan + '</span><br/>';
        return s;
    }

    function confirmca(){
        //to get the loaded data
        //alert(url);
        $.messager.confirm('Confirm','Are you sure you want to Confirm this ?',function(r){
            if (r){
                var dateObj = new Date();
                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                var day = dateObj.getUTCDate();
                var year = dateObj.getUTCFullYear();

                newdate =  day + "-" + month + "-" + year;
                $('#datenow').textbox('setValue',newdate);
                $('#cano').textbox('setValue','P-1701000001');
                $('#printca').linkbutton('enable');
            }
        });
    }
    function printca(){
        alert('a');
    }

    function clearca(){
        $('#detkeg').datagrid({
            url:'',
        });
        $('#refno').combobox('setValue','');
        $('#name_tbp').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#cano').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
</script>
