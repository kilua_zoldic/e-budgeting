<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 12/18/16
 * Time: 3:47 PM
 */
?>
                <table id="detkeg"
                       data-options="singleSelect:true,fit:true,fitColumns:true"
                       showFooter="true" toolbar="#tb" idField="id_payment_master_group" sortName="payment_no" pagination="true"
                       rownumbers="true" pageSize="50" style="width:100%;height:250px">
                    <thead>
                    <tr>
                        <th data-options="field:'check_date',editor:{type:'datebox',options:{required:true,validType:'validDate[\'dd-MM-yyyy\']' }}" width="150">Date</th>
                        <th data-options="field:'id_payment_master_group'" hidden="true" hwidth="80">ID</th>
                        <th data-options="field:'payment_no'" width="150">Payment No</th>
<!--                        <th data-options="field:'bp'" width="150">Bidang Pelayanan</th>-->
                        <th data-options="field:'rek_no'" width="150">No Rek</th>
                        <th data-options="field:'status_paid',formatter:payment_method" width="150">Payment Method</th>
                        <th data-options="field:'bank_name'" width="150">Bank</th>
<!--                        <th data-options="field:'bd'" width="150">Badan Pelayanan</th>-->
<!--                        <th data-options="field:'sub_total',align:'right',formatter:formatPrice" width="130"> Anggaran</th>-->
                        <th data-options="field:'paid',align:'right',formatter:formatPrice" width="130">Paid</th>
                        <th data-options="field:'settlement',align:'right',formatter:formatPrice" width="130">Settlement</th>
                        <th data-options="field:'total_settlement',align:'right',formatter:formatPrice" width="130">Total Settlement</th>
                        <th data-options="field:'car_req',align:'right',formatter:formatPrice" width="130">CA</th>
                        <th data-options="field:'balance',align:'right',formatter:formatPrice" width="150">Balance</th>
                            </tr>
                    </thead>
                </table>
            <div id="tb" style="padding:2px 5px;">
                Date From: <input class="easyui-datebox" style="width:110px">
                To: <input class="easyui-datebox" style="width:110px">
                Language:
                <select class="easyui-combobox" panelHeight="auto" style="width:100px">
                    <option value="java">Java</option>
                    <option value="c">C</option>
                    <option value="basic">Basic</option>
                    <option value="perl">Perl</option>
                    <option value="python">Python</option>
                </select>
                <a href="#" class="easyui-linkbutton" iconCls="icon-search">Search</a>
            </div>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->


<div id="dlgpayment" class="easyui-dialog" style="width:100%;height:60%;"  maximizable="true" closed="true"  buttons="#dlgpayment-buttons">
    <table id="calist" style="width:100%;height:450px;" fit="true"
           rownumbers="true" showFooter="true" fitColumns="true" toolbar="#toolbartree"
           idField="id" showFooter="true" Field="cash_advance" sortName="cash_advance" singleSelect="false" sortOrder="asc" >
        <thead >
        <tr>
            <th data-options="field:'cash_advance'" width="150">Cash Advance No</th>
<!--            <th data-options="field:'bp'" width="150">Bidang Pelayanan</th>-->
<!--            <th data-options="field:'bd'" width="150">Badan Pelayanan</th>-->
<!--            <th data-options="field:'total',formatter:formatPrice" width="150">Anggaran</th>-->
            <th data-options="field:'norek'" width="150">No Rek</th>
            <th data-options="field:'payment_method'" width="150">Payment Method</th>
            <th data-options="field:'bank'" width="150">Bank</th>
            <th data-options="field:'paid',align:'right',formatter:formatPrice" width="150">Paid</th>
            <th data-options="field:'settle',align:'right',formatter:formatPrice" width="150">Settlement</th>
            <th data-options="field:'ca',align:'right',formatter:formatPrice" width="150">CA</th>
            <th data-options="field:'balance',align:'center',formatter:formatPrice" width="150">Balance</th>
        </tr>

        </thead>
    </table>

</div>
<div id="dlgpayment-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgpayment').dialog('close')" style="width:90px">Close</a>
</div>

<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#printca').linkbutton('disable');
    });
</script>
<script>
//    $('#care').combobox({
//        valueField: 'care',
//        onSelect: function (rec) {
//            $('#detkeg').edatagrid({
//                url:'modul/cash_payment/cash_paymentaction.php?act=getlist',
//                pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
//                emptyMsg: 'No Records Found',
//            });
//        }
//    });
    $('#detkeg').edatagrid({
        url:'modul/cash_payment/cash_paymentaction.php?act=getlist',
        pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
        emptyMsg: 'No Records Found',
    });



    function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.refno + '</span><br/>' +
            '<span style="color:#888"> Nama Kegiatan : ' + row.nama_kegiatan + '</span><br/>';
        return s;
    }

    function confirmca(){
        //to get the loaded data
        //alert(url);
        $.messager.confirm('Confirm','Are you sure you want to Confirm this ?',function(r){
            if (r){
                var dateObj = new Date();
                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                var day = dateObj.getUTCDate();
                var year = dateObj.getUTCFullYear();

                newdate =  day + "-" + month + "-" + year;
                $('#datenow').textbox('setValue',newdate);
                $('#cano').textbox('setValue','P-1701000001');
                $('#printca').linkbutton('enable');
            }
        });
    }
    function printca(){
        alert('a');
    }

    function clearca(){
        $('#detkeg').datagrid({
            url:'',
        });
        $('#refno').combobox('setValue','');
        $('#name_tbp').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#cano').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
</script>
