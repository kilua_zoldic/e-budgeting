    <table id="proker" style="width:100%;height:100%;" fit="true" border="false" fitColumns="true"
			rownumbers="true" pageSize="50" showFooter="true"  pagination="true" singleSelect="true"
            idField="id_proker" prokerField="id_proker" toolbar="#toolbar">
        <thead>
            <tr>
                <th field="name_tbp" width="100" sortable="true">Nama Badan Pelayanan</th>
                <th field="name_tbd" width="100" sortable="true" >Nama Bidang</th>
                <th field="periode" width="100" sortable="true" >Periode</th>
				<th field="create_by" width="100" sortable="true" >Create By</th>
                <th field="create_date" width="100" sortable="true" >Create Time</th>
                <th field="update_by" width="100" sortable="true" >Update By</th>
                <th field="update_date" width="100" sortable="true" >Update Date</th>
                <th field="status_proker" width="100" sortable="true" formatter="status_proker">Status</th>
        </tr>
        </thead>
    </table>
<div id="toolbar">
	<table>
		<tr>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newproker()" >New </a></td>
		<!--<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editproker()" >Edit </a></td>-->
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeproker()" >Remove </a></td>
		</tr>
	</table>
</div>
<!--toolbar="#dlg-toolbar"-->
<div id="dlg" class="easyui-dialog" style="width:100%;height:100%;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
	<div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
		<form id="fm" method="post" novalidate>
		<fieldset class="scheduler-border">
		<legend class="scheduler-border" >Kategori</legend>
		<div class="control-group">
		<table cellpadding="5">
		<tr>
		<td>Nama Badan Pelayanan </td>
		<td>: <input name="name_tbp" style="width:200px;" id="tbp" class="easyui-combobox" data-options="prompt:'Nama Badan Pelayanan.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Nama Bidang</td>
		<td>: <input name="name_tbd" style="width:200px;" id="tbd" class="easyui-combobox" data-options="prompt:'Nama Bidang.'" required="true"></td>
		</tr>
		
		</table>
		</div>
		</fieldset>
		
		<table id="dgc" style="width:100%;height:300px" 
		toolbar="#toolbardgc" pagination="true"  pageSize="50"
		rownumbers="true" fitColumns="auto" singleSelect="true"  idField="id_rp_proker" sortName="id_rp_proker" sortOrder="asc">
		<thead>
		<tr>
		<th data-options="field:'id_rp_proker',width:80,sortable:true,hidden:true">ID sub</th>
		<th data-options="field:'id_sub_menu',width:80,sortable:true,hidden:true">ID sub</th>
		<th data-options="field:'username',width:200,sortable:true">Roles</th>
		<th data-options="field:'view',width:100,sortable:true,formatter:status_master_menu_detail">View</th>
		<th data-options="field:'ins',width:100,sortable:true,formatter:status_master_menu_detail">Add</th>
		<th data-options="field:'edit',width:100,sortable:true,formatter:status_master_menu_detail,">Edit</th>
		<th data-options="field:'delet',width:100,sortable:true,formatter:status_master_menu_detail,">Delete</th>
		</tr>
		</thead>
		</table>
		<div id="toolbardgc">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newrole()" >New </a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dgc').edatagrid('saveRow')" >Confirm Row</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dgc').edatagrid('cancelRow')" >Undo </a></td>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removerole()" >Remove </a></td>
		</div>
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="saveproker()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>

<div id="dlgrole" class="easyui-dialog" style="width:50%;height:60%;padding:10px 15px"  maximizable="true" toolbar="#dlgrole-toolbar" closed="true"  buttons="#dlgrole">
		<div id="dlgrole-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
				   <a href="javascript:void(0)" class="easyui-linkbutton" id="edtrole" data-options="iconCls:'icon-edit',plain:true" onclick="editfieldrole()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
	<form id="fmrole" method="post" novalidate>
	<table cellpadding="5">
	<tr>
	<td>Username</td><td>: <input id="username" name="username" style="width:200px;"  data-options="prompt:'Username.'" required="true" ></td>
	</tr>
	<tr>
	<td>View</td>
	<td>: <input name="view" style="width:200px;" value="1" class="cb" type="checkbox"></td></tr>
	</tr>
	<tr>
	<td>Add</td>
	<td>: <input name="ins" style="width:200px;" value="1" class="cb" type="checkbox" ></td></tr>
	<tr>
	<td>Edit</td>
	<td>: <input name="edit" style="width:200px;" value="1" class="cb" type="checkbox" ></td></tr>
	<tr>
	<td>Remove</td>
	<td>: <input name="delet" style="width:200px;" value="1" class="cb"  type="checkbox" ></td></tr>
	</table>
	</form>
</div>
<div id="dlgrole">
<a href="javascript:void(0)" class="easyui-linkbutton svrole" iconCls="icon-ok" onclick="saverole()" style="width:90px">Save</a>                   
				
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgrole').dialog('close')" style="width:90px">Close</a>
</div>	
<script>
var url;
var urlrole;
var dg = proker;
$('#proker').datagrid({
	url:'modul/tbproker/tbprokeraction.php?act=list',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index){
				//editproker(index);
				detproker(index);
			},
	view: myview,
	emptyMsg: 'No Records Found',
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
                }	
	});
	
function combotbptbd(){
	$('#tbp').combobox({
	url:'control/view.php?act=tbp',
	valueField:'id_tbp',
	textField:'name_tbp',
	});
	$('#tbd').combobox({
	url:'control/view.php?act=tbd',
	valueField:'id_tbd',
	textField:'name_tbd',
	});
	
}

function roletbl(row){
	$('#dgc').edatagrid({
		autoSave: false,
		url: 'modul/tbproker/tbprokeraction.php?act=lisrolet&id_proker='+row.id_proker,
		saveUrl: 'modul/tbproker/tbprokeraction.php?act=createroles&id_proker='+row.id_proker,
		updateUrl: 'modul/tbproker/tbprokeraction.php?act=updateroles',	
		destroyUrl: 'modul/tbproker/tbprokeraction.php?act=deleteroles',
		
		onAfterEdit:function(index,row){
			$('#dgc').edatagrid('reload');
			//$('#dg').edatagrid('reload');
		},
		destroyMsg:{
			norecord:{
				title:'Warning',
				msg:'No record is selected.'
			},
			confirm:{
				title:'Confirm',
				msg:'Are you sure you want to delete?'
			}
		},
	onError: function(index,row){
		$.messager.show({
			title:'Info',
			msg:row.msg
		});
	},
	onBeginEdit:function(index,row){
		$.messager.show({
			title:'Warning',
			msg:'Editing Mode'
		});
	},
	onCancelEdit:function(index,row){
		$.messager.show({
			title:'Info',
			msg:'Cancel Editing Mode'
		});
	}
	});	
}
function newproker(){
	$('#dlg').dialog('open').dialog('setTitle','New');
	$('#tbd').combobox('enable');
	$('#tbp').combobox('enable');
	$('#edt').linkbutton('disable');
	$('.sv').linkbutton('enable');
	combotbptbd();
	//$('#dgc').edatagrid("hideColumn","view"); 
	var random = Math.floor(Math.random()*9999);
	$('#dgc').datagrid({
		autoSave: false,
		url: 'modul/tbproker/tbprokeraction.php?act=lisrolet&random=<?php echo $_SESSION['random'];?>',
	});
	$('#fm').form('clear');
	url = 'modul/tbproker/tbprokeraction.php?act=create&random=<?php echo $_SESSION['random'];?>';
}

/* DETAIL PROKER DOCUMENT*/
function detproker(index){
	var row = $('#proker').datagrid('getData').rows[index];    
	if(row)
	{
	var random = Math.floor(Math.random()*9999);	
	$('#dlg').dialog('open').dialog('setTitle','View');
	combotbptbd();
	$('#fm').form('load',row);
	$('#dgc').datagrid({
		autoSave: false,
		url: 'modul/tbproker/tbprokeraction.php?act=lisrolet&id_proker='+row.id_proker+'&random=<?php echo $_SESSION['random'];?>',
		onDblClickRow:function(index,row){
				//editproker(index);
				detrole(index);
			},
	});
	}
}



/*END OF DETAIL PROKER DOCUMENT*/


function editproker(index){

	var row = $('#proker').datagrid('getData').rows[index];    
	if(row)
	{
	combotbptbd();
	
	$('#dlg').dialog('open').dialog('setTitle','View / '+row.name_tbp+ ' / ' +row.name_tbd);
	$('#fm').form('load',row);
	$('#edt').linkbutton('enable');
	//$('.datagrid-header-inner').hide();
	$('#tbp').combobox('disable');
	$('#tbd').combobox('disable');
	$('.sv').linkbutton('disable');
	$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
	
	
	url = 'modul/tbproker/tbprokeraction.php?act=update&id='+row.id_proker;
		}		
}
function editfield(){
	$('#tbp').combobox('enable');
	$('#tbd').combobox('enable');
	$('.sv').linkbutton('enable');
}
	function saveproker(){
			//to get the loaded data
			$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlg').dialog('close');		// close the dialog
						$('#proker').datagrid('reload');	// reload the user data
					}
				}
			});}
			});
}

function removeproker(){
var row = $('#proker').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/tbproker/tbprokeraction.php?act=deleteproker',{id:row.id_proker},function(result){
if (result.success){
$('#proker').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function editfieldrole(){
	$('#username').combobox('enable');
	$('.cb').prop('disabled', false);
	$('.svrole').linkbutton('enable');
}

function newrole(){
	//var random = Math.floor(Math.random()*9999);
	var row = $('#proker').datagrid('getSelected');
	$('#dlgrole').dialog('open').dialog('setTitle','New Access Control');
	$('#fmrole').form('clear');
	$('#edt').linkbutton('disable');
	$('#username').combobox({
	url:'modul/user/useraction.php?act=userlist',
	valueField:'id_a',
	textField:'username',
	});
	if(row){
	urlrole = 'modul/tbproker/tbprokeraction.php?act=createroles&random=<?php echo $_SESSION['random'];?>&id_proker='+row.id_proker;
	}
	else {
	urlrole = 'modul/tbproker/tbprokeraction.php?act=createroles&random=<?php echo $_SESSION['random'];?>&id_proker=0';
	}
}

function detrole(index){

	var row = $('#dgc').datagrid('getData').rows[index];    
	if(row)
	{
	$('#dlgrole').dialog('open').dialog('setTitle','View ' );
	$('#username').combobox({
	url:'modul/user/useraction.php?act=userlist',
	valueField:'id_a',
	textField:'username',
	});
	$('#fmrole').form('load',row);
	$('#edt').linkbutton('enable');
	$('#username').combobox('disable');
	$('.cb').prop('disabled', 'disabled');
	$('.svrole').linkbutton('disable');
	
	$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
	
	
	urlrole = 'modul/tbproker/tbprokeraction.php?act=updaterole&id_rp_proker='+row.id_rp_proker;
		}		
}
function saverole(){
		//to get the loaded data
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
		//alert(urlrole);
		$('#fmrole').form('submit',{
			
				url: urlrole,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlgrole').dialog('close');		// close the dialog
						$('#dgc').datagrid('reload');	// reload the user data
					}
				}
		});}
	});
}
function removerole(){
var row = $('#dgc').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/tbproker/tbprokeraction.php?act=deleterole',{id:row.id_rp_proker},function(result){
if (result.success){
$('#dgc').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}
</script>