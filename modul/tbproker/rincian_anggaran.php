  <table id="rincian" title="Rincian Anggaran" style="width:98%;height:250px" toolbar="#toolbarrincian" rownumbers="true"
            data-options="singleSelect:true,collapsible:true" idField="id_child" showFooter="true"  pagination="true" >
        <thead>
            <tr>
               
				<th data-options="field:'cost_name',width:200">Unit Name</th>
				<th data-options="field:'cost_price',width:100,formatter:formatPrice" align="right">Cost</th>
                <th data-options="field:'total_human',width:80,align:'right'">Jumlah Unit</th>
                <th data-options="field:'total_event',width:80,align:'right'">Jumlah Event</th>
                <th data-options="field:'biaya',width:150,formatter:formatPrice" align="right"> Sub Total (Cost * Unit * Event)</th>
                <th data-options="field:'desc_cost',width:250">Description</th>
                <th data-options="field:'note',width:250">Note</th>
            </tr>
        </thead>
    </table>
	<div id="toolbarrincian">
		<table>
			<tr>
			<td><a href="javascript:void(0)" class="easyui-linkbutton docproker" iconCls="icon-add" plain="true" onclick="newrincian()" >New </a></td>
			<!--<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="edittbd()" >Edit </a></td>-->
			<td><a href="javascript:void(0)" class="easyui-linkbutton deldoc" iconCls="icon-remove" plain="true" onclick="removechild()" >Remove </a></td>
			</tr>
		</table>
	</div>	 

