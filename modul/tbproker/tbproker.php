    <table id="proker" style="width:100%;height:100%;" fit="true" border="false" fitColumns="true"
			rownumbers="true"  showFooter="true"  singleSelect="true" toolbar="#toolbarproker"
            idField="id" treeField="name_tbd">
        <thead>
            <tr>
                <th field="name_tbd" width="100" sortable="true">Nama Badan Pelayanan</th>
                <th field="desc_tbd" width="100" sortable="true">Description</th>
          
        </tr>
        </thead>
    </table>
	<div id="toolbarproker">
		<table>
		<tr>
		<td><a href="javascript:void(0)" class="easyui-linkbutton " iconCls="icon-search" plain="true" onclick="detproker()" >Detail </a></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="expandAllgrid()" style="width:80px;">Expand All</a></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="collapseAllgrid()" style="width:90px;">Collapse All</a></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="refresh()" style="width:90px;">Refresh Table</a></td>
		
		</tr>
			</table>
		</div>
<!--toolbar="#dlg-toolbar"-->


<div id="dlgx" class="easyui-dialog" style="width:100%;height:100%;"  maximizable="true" closed="true"  buttons="#dlgx-buttons">
	
	<!--<form id="fmx" method="post" novalidate>
		<fieldset class="scheduler-border">
		<legend class="scheduler-border" >Kategori</legend>
		<div class="control-group">
		<table cellpadding="5">
		<tr>
		<td>Nama Badan Pelayanan </td>
		<td>: <input name="name_tbdsec" style="width:200px;" class="easyui-textbox db"  data-options="prompt:'Nama Badan Pelayanan.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Nama Bidang</td>
		<td>: <input name="name_tbpsec" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Nama Bidang.'" required="true"></td>
		</tr>
		
		</table>
		</div>
		</fieldset>
		
		
		</form>-->
		<!--end-->

		<table id="tree" style="width:100%;height:450px;" fit="true"
            rownumbers="true" showFooter="true" fitColumns="false" toolbar="#toolbartree"
            idField="id" treeField="nama_kegiatan" sortName="nama_kegiatan" singleSelect="false" sortOrder="asc" >
        <thead frozen="true">
            <tr>
				<th field="nama_kegiatan" width="150">Nama Kegiatan</th>
            </tr>
			
        </thead>
       <thead>
            <tr>
				
				<th field="cost_name" rowspan='2' width="150" align='center'>Rincian Anggaran</th>
                <th field="time" rowspan='2' width="120" align='center'>Waktu</th>
                <th colspan="2" align='center'>Jumlah </th>
                <th colspan="2" align='center'>Anggaran</th>
				<th rowspan='2' align='center' data-options="field:'refno',width:'100'">Ref No</th>
				<th rowspan='2' align='center' data-options="field:'jenis',width:'300',formatter:jenis">Jenis Kegiatan</th>
				<th rowspan='2' align='center' data-options="field:'note',width:'300'">Note</th>
			
            </tr>
			<tr>
                <th field="total_human" width="100" align="right">Jumlah Orang</th>
                <th field="total_event" width="100" align="right">Jumlah Event</th>
                <th field="sub_total" width="100" align="right" data-options="formatter:formatPrice">Sub</th>
                <th field="totalx" width="100" align="right" data-options="formatter:formatPrice">Total</th>
            </tr>
        </thead>
		</table>
		<div id="toolbartree">
			<table>
				<tr>
				<td><a href="javascript:void(0)" class="easyui-linkbutton " iconCls="icon-search" plain="true" onclick="detdocproker()" >Detail </a></td>
				<td><a href="javascript:void(0)" class="docproker easyui-linkbutton " iconCls="icon-add" plain="true" onclick="newdocproker()" >New </a></td>
				<td><a href="javascript:void(0)" class="deldoc easyui-linkbutton " iconCls="icon-remove" plain="true" onclick="removedocproker()" >Remove </a></td>
				<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="expandAllgridc()" style="width:80px;">Expand All</a></td>
				<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="collapseAllgridc()" style="width:90px;">Collapse All</a></td>
				<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="refreshc()" style="width:90px;">Refresh Table</a></td>
				<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="print()" style="width:90px;">Generate Report</a></td>
				</tr>
			</table>
		</div>
</div>
<div id="dlgx-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgx').dialog('close')" style="width:90px">Close</a>
</div>




<?php include 'dialog_document.php';?>
<?php include 'dialog_rincian.php';?>

<script>
	var ins = 'disable';
	var edit = 'disable';
	var svt = 'disable';
	var delet = 'disable';
<?php if($_SESSION['id_level'] == 1 OR $_SESSION['id_level'] == 2){ ?>
	ins = 'enable';
	edit = 'enable';
	svt = 'enable';
	delet = 'enable';
	<?php } ?>
var url;

var urlmaster;
var dg = proker;

function expandAllgrid(){
	$('#proker').treegrid('expandAll');
			
}
function collapseAllgrid(){
	$('#proker').treegrid('collapseAll');
			
}
function expandAllgridc(){
	$('#tree').treegrid('expandAll');
			
}
function collapseAllgridc(){
	$('#tree').treegrid('collapseAll');
			
}
function refresh(){
	$('#proker').treegrid('reload');
			
}
function refreshc(){
	$('#tree').treegrid('reload');
			
}
$("#custom").change(function(){
		var chat = $("#custom").val();
		if(chat == '0'){
			$("#xx").show();
			$("#xxstandart").hide();
		}
		else {
			$("#xx").hide();
			$("#xxstandart").show();
			}
    });
$('#zxc').combobox({
			  onChange: function(e){
				if(e ==  1){
					$('#time').textbox('show'); 
					$('#xc').datetimebox('hide'); 
				}
				else if(e == 2) {
				$('#xc').datetimebox('show'); 
				$('#time').textbox('hide'); 
				}
				else {
				$('#time').textbox('hide'); 
				$('#xc').datetimebox('hide'); 
				}
			}
	});
<?php
//$id = '19';
//$disb = '';
//if($company->privilege_level2('ins',$id) == '1'){
//	$disb = 'enable';
//}
//else if($company->privilege_level2('edit',$id) == '1'){
//	$disb = 'enable';
//}
?>
function editddoc(){
//	$('#tbp').combobox('enable');
//	$('#tbd').combobox('enable');
$('#zxc').combobox('enable');
$('.db').textbox('enable');
$('.sdc').linkbutton('enable');
}

$('#proker').treegrid({
	url:'modul/tbproker/tbprokeraction.php?act=listrolex',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index){
				//editproker(index);
				detproker(index);
			},
	onLoadSuccess: function(row,data){
            $('#proker').treegrid('collapseAll')
        },		
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
                }	
	});

$('#proker').treegrid('enableFilter');
$('#dlgx').dialog({
  onBeforeClose:function(){
    var p = $(this);
		
	$('#proker').treegrid('reload');
		var opts = p.panel('options');
        var onBeforeClose = opts.onBeforeClose;
        opts.onBeforeClose = function(){};
       $('#dlgx').dialog('close');
        opts.onBeforeClose = onBeforeClose;
	
    return false;
  }
}); 

/* DETAIL PROKER DOCUMENT*/
function detproker(index){
	var row = $('#proker').treegrid('getSelected');//.rows[index];
//	console.log(row.id_master_proker);
//	var idmp;
	if(row.parentId != 0)
	{
	$('#dlgx').dialog('open').dialog('setTitle','Document ' +row.name_tbd );
	$('#fmx').form('load',row);
	$('.db').textbox('disable');
	$('#zxc').combobox('disable');

	//$('.docproker').linkbutton('<?php //echo $company->privilege_level2('ins');?>');
	//$('.deldoc').linkbutton('<?php //echo $company->privilege_level2('delet');?>');

	if(row.status == 'enable'){
		<?php
//		$idmp = '<script>idmp</script>';
//		$id = "<script>row.id</script>";
		?>
		
		if(row.ins == '1'){
			ins = 'enable';
			svt = 'enable';
		}
		if(row.edit == '1'){
			edit = 'enable';
			svt = 'enable';
		}
		if(row.delet == '1'){
			delet = 'enable';
		}
		$('.docproker').linkbutton(ins);
		$('.deldoc').linkbutton(delet);
		$('.sdc').linkbutton(svt);


	}
	else {
	$('.docproker').linkbutton('disable');
	$('.deldoc').linkbutton('disable');
	$('.sdc').linkbutton('disable');
	}
	$('#tree').treegrid({
	url:'modul/tbproker/tbprokeraction.php?act=aaa&id='+row.id, 	
	//title:'Dokumen ' +row.name_tbd,
	singleSelect:true,
	onDblClickRow:function(index){
				//editproker(index);
				detdocproker(index);
			},
	onLoadError:function(){
		//alert('a');
	}
	});
	//url = 'modul/tbproker/tbprokeraction.php?act=update&id='+row.id;
	}
}
/*
function treegrid(row)
{
	$('#tree').datagrid({
	url:'modul/tbproker/tbprokeraction.php?act=aaa&id='+row.id_proker, 	
	title:'Dokumen '+row.name_tbp+' - '+row.name_tbd+' -  Periode '+row.periode + row.id_proker,
	singleSelect:true,
	onDblClickRow:function(index,row){
				//editproker(index);
				detdocproker(index);
			}

			
	});
}	
*/


function detdocproker()
{
	var row = $('#proker').treegrid('getSelected');  
	var node = $('#tree').treegrid('getSelected');
	//var row = $('#proker').datagrid('getData').rows[index];  
//	var row;
	$('.sdc').linkbutton('disable');
	$('.db').textbox('disable');
	$('#zxc').combobox('disable');
	$('#edt').linkbutton(edit);
	if(node.parentId !== '0'){
	}
	else {
	$('#dlgxx').dialog('open').dialog('setTitle','View ' +node.nama_kegiatan );
	$('#fmxx').form('clear');
	$('#fmxx').form('load',node);

	//$('.docproker').linkbutton('<?php echo $company->privilege_level2('ins');?>');
	//$('.deldoc').linkbutton('<?php echo $company->privilege_level2('delet');?>');
	//CKEDITOR.instances.description.setData(node.description);
	$('#rincian').datagrid({
	url:'modul/tbproker/tbprokeraction.php?act=listdoc&parentId='+node.id,
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				//editproker(index);
				detrincian(index,row);
			},
	view: myview,
	emptyMsg: 'No Records Found',		
	});
	
	url = 'modul/tbproker/tbprokeraction.php?act=updatedocproker&id='+node.id;
	
	}
}

function newdocproker() 
{
	$('#edt').linkbutton('disable');
	$('#tree').treegrid('clearSelections');
	$('#time').textbox('hide'); 
	$('#xc').datetimebox('hide');
	$('.db').textbox('enable');
	$('#zxc').combobox('enable');
	$('.sdc').linkbutton('enable');
	//CKEDITOR.instances.description.setData('');
	var row = $('#proker').treegrid('getSelected');//.rows[index]; 
	$('#dlgxx').dialog('open').dialog('setTitle','New Document ');
	$('#fmxx').form('clear');

	//$('#rincian').hide();
	//$('#toolbarrincian').hide();
	$('#rincian').datagrid({
	url:'modul/tbproker/tbprokeraction.php?act=nlc&id_proker='+row.id+'&random=<?php echo $_SESSION['random'];?>',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				//editproker(index);
				detrincian(index);
			},
	
	});
	url = 'modul/tbproker/tbprokeraction.php?act=zxc&id_tbd='+row.parentId+'&id_tbp='+row.id_tbp+'&id_proker='+row.id+'&random=<?php echo $_SESSION['random'];?>';
}
$('#dlgxx').dialog({
  onBeforeClose:function(){
    var p = $(this);
   
		$.post('modul/tbproker/tbprokeraction.php?act=deletechildrandom',{id:<?php echo $_SESSION['random'];?>},function(result){
		if (result.success){
		$('#tree').treegrid('reload'); // reload the user data
		} else {
		$.messager.show({ // show error message
		title: 'Error',
		msg: result.errorMsg
		});
		}
		},'json');
		var opts = p.panel('options');
        var onBeforeClose = opts.onBeforeClose;
        opts.onBeforeClose = function(){};
       $('#dlgxx').dialog('close');
        opts.onBeforeClose = onBeforeClose;
	
    return false;
  }
}); 
function savedocproker(){
			//to get the loaded data
			//alert(url);
			$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
			$('#fmxx').form('submit',{
				url: url, // ini dari mana ya?
				onSubmit: function(){
					return $('#fmxx').form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlgxx').dialog('close');		// close the dialog
						$('#tree').treegrid('reload');	// reload the user data
						$('#proker').datagrid('reload');	// reload the user data
						
						 
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
					}
				}
			});}
			});
}
	
function akxx(obj){
	 if (typeof obj === "undefined") obj = {};

    var th = obj.th, te = obj.te,cp = obj.cp,
	sp = th * te * cp ;

    $('#sp').numberbox('setValue', sp);
		
}

/*END OF DETAIL PROKER DOCUMENT*/



function removedocproker(){
var row = $('#tree').treegrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/tbproker/tbprokeraction.php?act=deletedocproker',{id:row.id},function(result){
if (result.success){
$('#tree').treegrid('reload'); // reload the user data
$('#tree').treegrid('clearSelections'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}
function child_doc(node){
$('#rincian').datagrid({
	url:'modul/tbproker/tbprokeraction.php?act=listdoc&parentId='+node.id,
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				//editproker(index);
				//detproker(index);
			},
			
	});
}

function print(){
		var row = $('#proker').treegrid('getSelected');//.rows[index];  
		 url = "modul/report/pdf_report.php?period="+row.id_period+'&bdd='+row.parentId+'&bpd='+row.id_tbp;
       window.open(url);
}
</script>