<div id="dlgsapi" class="easyui-dialog" style="width:60%;height:85%;padding:10px 15px"  maximizable="true" toolbar="#dlgx-toolbar" closed="true"  buttons="#dlgsapi">
	<div id="dlgx-toolbar" style="padding:2px 0">
		<table cellpadding="0" cellspacing="0" style="width:100%">
			<tr>
				<td style="padding-left:2px">
					<a href="javascript:void(0)" class="easyui-linkbutton" id="edtc" data-options="iconCls:'icon-edit',plain:true" onclick="editfieldlgsapi()" >Edit</a>
				</td>
			</tr>
		</table>
	</div>
	<form id="fmrincianl" method="post" novalidate>
		<table cellpadding="5">
			<tr>
				<td>Unit Name </td><td>: <input id="costtype" name="id_cost" style="width:200px;"  data-options="formatter: formatItem,prompt:'Unit Name.'" required="true" ></td>

			</tr>

			<tr id="do_hide">
				<td id="do_hide">   </td><td>: <input name="desc_cost" class="easyui-textbox" id="do" style="width:300px;height:70px;" data-options="multiline:true,prompt:'Unit Name.'"></td>
			</tr>
			<tr id="ho_hide">
				<td id="ho_hide">Cost  </td><td>: <input name="sub_total"  style="width:200px;" class="easyui-numberbox" id="ho" data-options="precision:2,groupSeparator:'.',decimalSeparator:',',prompt:'Cost.'" ></td>
			</tr>

			<tr id="cp_hide">
				<td id="cp_hide">Cost  </td><td>: <input name="cost_price" class="easyui-numberbox" style="width:200px;" id="cp" data-options="precision:2,groupSeparator:'.',decimalSeparator:',',prompt:'Cost.'" disabled></td>
			</tr>
			<tr id="th_hide">
				<td id="th_hide">Jumlah Unit </td><td>: <input name="total_human"  style="width:200px;" id="th" data-options="prompt:'Jumlah Unit.'" required="true"></td>
			</tr>
			<tr id="te_hide">
				<td id="te_hide">Jumlah Event </td><td>: <input name="total_event"  style="width:200px;" id="te" data-options="prompt:'Jumlah Event.'" required="true"></td>
			</tr>
			<tr>
				<td>Sub Total</td><td>: <input name="x"  style="width:200px;" id="sp" class="easyui-numberbox" data-options="precision:2,groupSeparator:'.',decimalSeparator:',',prompt:'Sub.'" disabled></td>
			</tr>
			<tr>
				<td>Note</td><td>: <input name="note"  style="width:300px;height:70px;" class="easyui-textbox note" data-options="multiline:true,prompt:'Note.'"></td>
			</tr>
		</table>
	</form>
</div>
<div id="dlgsapi">
	<a href="javascript:void(0)" class="easyui-linkbutton sdcsapi" iconCls="icon-ok" onclick="savechild()" style="width:90px">Save</a>

	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgsapi').dialog('close')" style="width:90px">Close</a>
</div>

<script>
	var urlchild;
	function formatCCN(row){
		var s = '<span style="font-weight:bold">' + row.cost_code_number + '</span><br/>' +
			'<span style="color:#888"> ' + row.cost_code_name + '</span><br/>';
		return s;
	}
	function formatItem(row){
		var s = '<span style="font-weight:bold">' + row.cost_name + '</span><br/>' +
			'<span style="color:#888"> Unit : ' + row.unit + '</span><br/>'+
			'<span style="color:#888"> Note : ' + row.description + '</span><br/>'+
			'<span style="color:#888"> Cost : ' + row.cost_price + '</span>';
		return s;
	}

	function newfieldlgsapi()
	{
//		$('#costtype').combobox('enable');
//		$('#te').textbox('enable');
		$('.note').textbox('enable');
		$('#do').textbox('enable');
//		$('#ho').numberbox('enable');
//		$('#th').textbox('enable');
		$('#ho').textbox('enable');
//		$('#te').textbox('enable');
		$('.sdcsapi').linkbutton('enable');
	}
	function editfieldlgsapi()
	{
		$('#costtype').combobox('enable');
		$('#te').textbox('enable');
		$('.note').textbox('enable');
		$('#do').textbox('enable');
		$('#ho').numberbox('enable');
		$('#th').textbox('enable');
		$('#ho').textbox('enable');
		$('#te').textbox('enable');
		$('.sdcsapi').linkbutton('enable');
	}
	function akxx(obj){
		if (typeof obj === "undefined") obj = {};

		var th = obj.th,
			te = obj.te,
			cp = obj.cp,
			sp = th * te * cp ;


		$('#sp').numberbox('setValue', sp);

	}
	function akho(obj){
		if (typeof obj === "undefined") obj = {};

		var ho = obj.ho,
			th = obj.th,
			te = obj.te,
			sp = ho*te*th  ;


		$('#sp').numberbox('setValue', sp);

	}
	function newrincian(){
		newfieldlgsapi();
		var node = $('#tree').treegrid('getSelected');
		var row = $('#proker').datagrid('getSelected');//.rows[index];

		$('#edtc').linkbutton('disable');
		$('#dlgsapi').dialog('open').dialog('setTitle','New');
		$('#costtype').combobox({
			url:'control/view.php?act=costtype',
			valueField:'id_cost',
			textField:'cost_name',
			onSelect: function(rec){
				if(rec.id_cost ==  5){
					$('#do_hide').show();

					$('#ho_hide').show();
					$('#th_hide').show();
					$('#te_hide').show();
					$('#cp_hide').hide();
					$('#ho,#th,#te').numberbox({
						onChange: function(e){
							var ho = parseInt($('#ho').numberbox("getValue")) || 0;
							var th = parseInt($('#th').numberbox("getValue")) || 0;
							var te = parseInt($('#te').numberbox("getValue")) || 0;
							var obj = {
								ho: ho, th: th,te: te
							}
							akho(obj);
						}
					});
				}
				else {
					$('#cp').numberbox('setValue',rec.cost_price);
					$('#do_hide').hide();
					$('#ho_hide').hide();
					$('#cp_hide').show();
					$('#th_hide').show();
					$('#te_hide').show();
					$('#th, #te,#cp').numberbox({
						onChange: function(e){
							var th = parseInt($('#th').numberbox("getValue")) || 0;
							var te = parseInt($('#te').numberbox("getValue")) || 0;
							var cp = parseInt($('#cp').numberbox("getValue")) || 0;
							var obj = {
								th: th, te: te, cp: cp
							}

							akxx(obj);

						}
					});
				}
			}
		});
		$('#fmrincianl').form('clear');
		$('#do_hide').hide();
		$('#ho_hide').hide();
		$('#cp_hide').hide();
		$('#th_hide').hide();
		$('#te_hide').hide();
		$('#edtc').linkbutton('disable');
		if(node){
			urlchild = 'modul/tbproker/tbprokeraction.php?act=createchilddocproker&parentId='+node.id+'&id_proker='+row.id+'&id_tbd='+row.parentId+'&id_tbp='+row.id_tbp;
		}
		else {
			urlchild = 'modul/tbproker/tbprokeraction.php?act=createchilddocprokernew&random=<?php echo $_SESSION['random'];?>&id_proker='+row.id+'&id_tbd='+row.parentId+'&id_tbp='+row.id_tbp;
		}
	}

	function detrincian(index,row){
		$('#edtc').linkbutton(edit);
		var node = $('#rincian').datagrid('getSelected');//.rows[index];
		$('#do_hide').hide();
		$('#ho_hide').hide();
		$('#cp_hide').hide();
		$('#th_hide').hide();
		$('#te_hide').hide();
		$('#costtype').combobox({
			url:'control/view.php?act=costtype',
			valueField:'id_cost',
			textField:'cost_name',
			onSelect: function(rec){
				if(rec.id_cost == 5){
					$('#do_hide').show();
					$('#ho_hide').show();
					$('#th_hide').show();
					$('#te_hide').show();
					$('#cp_hide').hide();
					$('#costtype').combobox('setValue',rec.id_cost);

					$('#ho,#th,#te').numberbox({
						onChange: function(e){
							var ho = parseInt($('#ho').numberbox("getValue")) || 0;
							var th = parseInt($('#th').numberbox("getValue")) || 0;
							var te = parseInt($('#te').numberbox("getValue")) || 0;
							var obj = {
								ho: ho, th: th,te: te
							}
							akho(obj);
						}
					});
				}
				else {

					$('#cp').numberbox('setValue',rec.cost_price);
					$('#do_hide').hide();
					$('#ho_hide').hide();
					$('#cp_hide').show();
					$('#th_hide').show();
					$('#te_hide').show();
					$('#th, #te,#cp').numberbox({
						onChange: function(e){
							var th = parseInt($('#th').numberbox("getValue")) || 0;
							var te = parseInt($('#te').numberbox("getValue")) || 0;
							var cp = parseInt($('#cp').numberbox("getValue")) || 0;
							var obj = {
								th: th, te: te, cp: cp
							}

							akxx(obj);

						}
					});
				}
			}
		});

		$('#dlgsapi').dialog('open').dialog('setTitle','View');
		$('#costtype').combobox('disable');
		$('#do').textbox('disable');
		$('#cp').numberbox('disable');
		$('.note').textbox('disable');
		$('.sdcsapi').linkbutton('disable');

		$('#fmrincianl').form('load',node);

		if(node.id_cost == 5){
			$('#do_hide').show();
			$('#ho_hide').show();
			$('#th_hide').show();
			$('#te_hide').show();
			$('#cp_hide').hide();
			$('#costtype').combobox('setValue',node.id_cost);
			$('#do').textbox('setValue',node.desc_costc);
			$('#ho').numberbox('setValue',node.cost_price);

			$('#ho,#th,#te').numberbox({
				onChange: function(e){
					var ho = parseInt($('#ho').numberbox("getValue")) || 0;
					var th = parseInt($('#th').numberbox("getValue")) || 0;
					var te = parseInt($('#te').numberbox("getValue")) || 0;
					var obj = {
						ho: ho, th: th,te: te
					}
					akho(obj);
				}
			});
		}
		else {
			$('#cp').numberbox('setValue',node.cost_price);
			$('#do_hide').hide();
			$('#ho_hide').hide();
			$('#cp_hide').show();
			$('#th_hide').show();
			$('#te_hide').show();
			$('#th, #te,#cp').numberbox({
				onChange: function(e){
					var th = parseInt($('#th').numberbox("getValue")) || 0;
					var te = parseInt($('#te').numberbox("getValue")) || 0;
					var cp = parseInt($('#cp').numberbox("getValue")) || 0;
					var obj = {
						th: th, te: te, cp: cp
					}

					akxx(obj);

				}
			});
		}
		$('#th').textbox('disable');
		$('#ho').textbox('disable');
		$('#te').textbox('disable');
		var hitung = node.cost_price*node.total_human*node.total_event;
		$('#sp').numberbox('setValue',hitung);
		urlchild = 'modul/tbproker/tbprokeraction.php?act=updatechild&id='+node.id+'&parentId='+node.parentId;

	}
	function savechild(){
		//to get the loaded data
		$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
				$('#fmrincianl').form('submit',{
					url:urlchild,
					onSubmit: function(){
						return $('#fmrincianl').form('validate');
					},
					success: function(result){
						var result = eval('('+result+')');
						if (result.errorMsg){
							$.messager.show({
								title: 'Error',
								msg: result.errorMsg
							});
						} else {
							$.messager.show({
								title: 'Success',
								msg: result.success
							});
							$('#dlgsapi').dialog('close');		// close the dialog
							$('#rincian').datagrid('reload');
							$('#tree').treegrid('reload');
						}
					}
				});

			}
		});
	}

	function removechild(){
		var row = $('#rincian').datagrid('getSelected');
		if (row){
			$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
				if (r){
					$.post('modul/tbproker/tbprokeraction.php?act=deletechild',{id:row.id},function(result){
						if (result.success){
							$('#rincian').datagrid('reload'); // reload the user data
							$('#tree').treegrid('reload'); // reload the user data
						} else {
							$.messager.show({ // show error message
								title: 'Error',
								msg: result.errorMsg
							});
						}
					},'json');
				}
			});
		}
	}
</script>