
	<div style="margin-bottom:10px">
		<!--<a href="javascript:void(0)" class="easyui-menubutton" style="width:70px;height:78px;" data-options="size:'large',iconCls:'icon-load',iconAlign:'top',plain:false,menu:'#mm'">Load</a>-->
<!--		<a href="javascript:void(0)" class="easyui-linkbutton" style="width:70px;height:78px;" data-options="size:'large',iconCls:'icon-layout',iconAlign:'top',plain:false" onclick="javascript:$('#pg').pivotgrid('layout')">Layout</a>-->

		<tr>
			<td>Period</td>
			<td>: <input name="period" style="width:200px;" id="period" data-options="prompt:'Period.'" required="true"/></td>
		</tr>
		<select  name="pvt" style="width:200px;" id="pvt" data-options="prompt:'Select.'" required="true">
			<option></option>
			<option value="0">Bidang Pelayanan</option>
			<option value="1">Badan Pelayanan</option>
		</select>
	</div>
	<table id="pg" style="width:100%;height:500px" rownumbers="true"   ></table>
	<input type="button" onclick="exportExcel()" value="Export to Excel " />
	<script>
		function exportExcel() {
			var row=$('#pg').pivotgrid('getData');//to get the loaded data
			if (row) {
				var test= [];
				$.each(row, function(index, value) {
					test.push( value);
//				url = "modul/pivot/excel.php?entry="+test;
//				window.open(url);
				});
				console.log(test);
				localStorage.setItem('testObject', JSON.stringify(test));
				url = "modul/pivot/excel.php";
				window.open(url);
			}
		}
	</script>
	<script type="text/javascript">

			$('#period').combobox({
				url:'control/view.php?act=period',
				valueField:'id_period',
				textField:'period',
				onSelect: function (id) {
					$('#pvt').combobox({
						valueField: 'pvt',
						onSelect: function (rec) {
							if (rec.pvt == 0) {
								load1(id.id_period);

							}
							else if(rec.pvt == 1) {
								load2(id.id_period);
							}
						}

					});
				}
			});


		function load1(event){
			$('#pg').pivotgrid({
				url:'control/view.php?act=pivot_tbp&id='+event,
				method:'get',
				showFooter: true,
				valueFieldWidth:'150',
				pivot:{
					rows:['cost_name'],
					columns:['name_tbp'],
					filters:['total','cost_name'],

					values:[
						{field:'total'}
					]
				},
				forzenColumnTitle:'<span style="font-weight:bold">Pivot Grid</span>',
				valuePrecision:0,
			})
		}
		function load2(event){
			$('#pg').pivotgrid({
				url:'control/view.php?act=pivot&id='+event,
				method:'get',
				valueFieldWidth:'150',
				pivot:{
					rows:['cost_name'],
					columns:['name_tbd'],
					filters:['total','cost_name'],
					values:[
						{field:'total'}
					]
				},
				forzenColumnTitle:'<span style="font-weight:bold">Pivot Grid</span>',
				min:0,
				precision:2,
				groupSeparator:',',
				valueStyler:function(value,row,index){
//					if (value>1000000 && value< 9999999){
//						return 'background:#D8FFD8'
//					}
//					else if (value>10000000 && value< 99999999 ){
//						return 'background:#4286f4'
//					}
//					else if (value>100000000 && value< 999999999 ){
//						return 'background:#f44242'
//					}
				}
			})
		}
	</script>
	<script type="text/javascript" src="modul/pivot/jquery.pivotgrid.js"></script>