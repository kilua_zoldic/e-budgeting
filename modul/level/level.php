<table id="dg" class="easyui-datagrid" style="width:100%;height:100%" url="modul/level/levelaction.php?act=list"
toolbar="#toolbar" pagination="true" border="false" pagesize="50" fit="true"
rownumbers="true" fitColumns="true" singleSelect="true"  fit="true" idField="id_level" sortName="id_level" sortOrder="asc">
<thead>
<tr>

<th data-options="field:'id_level',width:80,sortable:true,hidden:true">ID Role</th>
<th data-options="field:'name',width:80,sortable:true">Role</th>
</tr>
</thead>
</table>
<div id="toolbar">
	<table>
		<tr>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editlevel()" >Edit</a></td>
		<!--<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="delete_level()" >Remove</a></td>-->
		</tr>
	</table>
</div>
<div id="dlg" class="easyui-dialog" style="width:470px;height:280px;padding:10px 20px" closed="true" buttons="#dlg-buttons">

		<form id="fm" method="post" novalidate enctype="multipart/form-data">
		<table cellpadding="5">
		<tr>
		<td>Name</td>
		<td>: <input name="name" style="width:200px;" class="easyui-textbox" required="true"></td>
		</tr>
		
		</table>
	
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savelevel()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>
 <script type="text/javascript">

var url;
function newlevel(){
$('#dlg').dialog('open').dialog('setTitle','New Role');
$('#fm').form('clear');
url = 'modul/level/levelaction.php?act=create';
}
function editlevel(){
var row = $('#dg').datagrid('getSelected');
if (row){
$('#dlg').dialog('open').dialog('setTitle','Edit Role');
$('#fm').form('load',row);
url = 'modul/level/levelaction.php?act=update&id='+row.id_level;
}
}
function status_modul(status_modul,row,index){
	if(row.status_modul == '1'){
		return '<span> Active</span>';
	}
	else {
	 return '<span> InActive</span>';
	}
}
 function savelevel(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');
						// reload the user data
						$.messager.show({
							title: 'Error',
							msg: result.success
						});
					}
				}
			});
}


	function delete_level() {
    var rows = $('#dg').edatagrid('getSelections');
    if (rows) {
        $.messager.confirm('Confirm', 'Are you sure you want to delete this?', function (r) {
            if (r) {
                var ss = $.map(rows, function (item, idx) {
                    return item.id_level;
                });
                /*$.messager.alert('Info', ss.join('<br/>')); for testing*/
                $.post('modul/level/levelaction.php?act=delete', {
                    'id[]': ss
                }, function (result) {
                   
                    if (result.success) {
                        $('#dg').edatagrid('reload'); // reload the user data
						 $.messager.show({ // show error message
                            title: 'Success',
                            msg: result.success
                        });
                    } else {
                        $.messager.show({ // show error message
                            title: 'Error',
                            msg: result.msg
                        });
                    }
                }, 'json').fail(function (jxhr, status, error) {
                    alert('error: ' + status + ':' + error)
                });
            }
        });
    }
}	
</script>
