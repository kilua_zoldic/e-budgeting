    <table id="tree" style="width:100%;height:100%" fit="true" border="false" fitColumns="true"
          
            rownumbers="true"
            idField="id" treeField="title" toolbar="#toolbar">
        <thead>
            <tr>
                <th field="id" hidden="true" width="100" >ID</th>
                <th field="title" width="100" >Title</th>
             
            </tr>
        </thead>
    </table>
<div id="toolbar">
	<!--<table>
		<tr>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="edittree()" >Edit </a></td>
		</tr>
	</table>-->
</div>
<!--toolbar="#dlg-toolbar"-->
<div id="dlg" class="easyui-dialog" style="width:60%;height:480px;"  resizable="true" maximizable="true" closed="true"  buttons="#dlg-buttons">
	<!-- <div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editparent()" >Edit Parent</a>
                </td>
            </tr>
        </table>
    </div>-->
		<form id="fm" method="post" novalidate>
		<table id="dgc" style="width:100%;height:300px" 
		toolbar="#toolbardgc" pagination="true"  pageSize="50"
		rownumbers="true" fitColumns="auto" singleSelect="true"  idField="id_rp" sortName="id_rp" sortOrder="asc">
		<thead>
		<tr>
		<th data-options="field:'id_rp',width:80,sortable:true,hidden:true">ID sub</th>
		<th data-options="field:'id_sub_menu',width:80,sortable:true,hidden:true">ID sub</th>
		<th data-options="field:'id_level',width:200,sortable:true,formatter:function(value,row){
				return row.name;
			},editor:{type:'combobox',
							options:{
								url:'modul/level/levelaction.php?act=list_level',
								valueField:'id_level',
								textField:'name',
								required:true,
							}}">Roles</th>
		<th data-options="field:'view',width:100,sortable:true,formatter:status_master_menu_detail,editor:{type:'checkbox',
							options:{
								 on: '1',
                        			off: '0',
								required:true
		}}">View</th>
		<th data-options="field:'ins',width:100,sortable:true,formatter:status_master_menu_detail,editor:{type:'checkbox',
							options:{
								 on: '1',
                        			off: '0',
								required:true,
		}}">Add</th>
		<th data-options="field:'edit',width:100,sortable:true,formatter:status_master_menu_detail,editor:{type:'checkbox',
							options:{
								 on: '1',
                        		 off: '0',
								required:true
		}}">Edit</th>
		<th data-options="field:'delet',width:100,sortable:true,formatter:status_master_menu_detail,editor:{type:'checkbox',
							options:{
								   on: '1',
                        			off: '0',
								required:true
		}}">Delete</th>
		</tr>
		</thead>
		</table>
		<div id="toolbardgc">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dgc').edatagrid('addRow')" >New </a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dgc').edatagrid('saveRow')" >Confirm Row</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dgc').edatagrid('cancelRow')" >Undo </a></td>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dgc').edatagrid('destroyRow')" >Remove </a></td>
		</div>
		</form>
</div>
<div id="dlg-buttons">

<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>

<script>
function status_master_menu_detail(val,row){
	if (val == '1' ){
		return '<span>Active</span>';
		} 
	else {
		return '<span>InActive</span>';
		}
	
}
var status_master_menu = [
		    {status:'1',status_text:'Active',selected: true},
		    {status:'0',status_text:'InActive'}
		];	
function status_sub(status_sub_menu,row,index){
	if(row.status_sub_menu == '1'){
		return '<span> Active</span>';
	}
	else {
	 return '<span> InActive</span>';
	}
}

$('#tree').treegrid({
	  url:'modul/tree/x.php',
	onDblClickRow:function(index,row){
				edittree(index);
				
			}
	});

function edittree(){

		var row = $('#tree').datagrid('getSelected');
		if(row)
		{
		
		$('#dlg').dialog('open').dialog('setTitle','Roles Permissions');
		$('#fm').form('load',row);
		$('#edt').linkbutton('enable');
		//$('.comtree').hide();
		//combo();
		//$('#combolink').combobox('setValue', row.mod_name);
		//url = 'modul/tree/treeaction.php?act=update&id='+row.id;
		
		$('#dgc').edatagrid({
		autoSave: false,
		url: 'modul/tree/treeaction.php?act=lisrolet&id='+row.id,
		saveUrl: 'modul/tree/treeaction.php?act=createroles&id='+row.id,
		updateUrl: 'modul/tree/treeaction.php?act=updateroles',	
		destroyUrl: 'modul/tree/treeaction.php?act=deleteroles',
		
		onAfterEdit:function(index,row){
			$('#dgc').edatagrid('reload');
			//$('#dg').edatagrid('reload');
		},
		destroyMsg:{
			norecord:{
				title:'Warning',
				msg:'No record is selected.'
			},
			confirm:{
				title:'Confirm',
				msg:'Are you sure you want to delete?'
			}
		},
	onError: function(index,row){
		$.messager.show({
			title:'Info',
			msg:row.msg
		});
	},
	onBeginEdit:function(index,row){
		$.messager.show({
			title:'Warning',
			msg:'Editing Mode'
		});
	},
	onCancelEdit:function(index,row){
		$.messager.show({
			title:'Info',
			msg:'Cancel Editing Mode'
		});
	}
	});	
		}		
}
function editparent(){
$('.comtree').show();	
}


function removetree(){
var row = $('#tree').treegrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/tree/treeaction.php?act=delete',{id:row.id},function(result){
if (result.success){
$('#tree').treegrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}
</script>