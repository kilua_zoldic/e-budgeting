    <table id="tree" style="width:100%;height:100%" fit="true" border="false" fitColumns="true"
			rownumbers="true" singleSelect="true"
            idField="id" treeField="title" toolbar="#toolbar">
        <thead>
            <tr>
                <th field="id" hidden="true" width="100" >ID</th>
                <th field="title" width="100" >Title</th>
                <th field="sort" width="100" sortable="true" >Sort</th>
             
            </tr>
        </thead>
    </table>
<div id="toolbar">
	<table>
		<tr>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="expandAllx()" style="width:80px;">Expand All</a></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="collapseAllx()" style="width:90px;">Collapse All</a></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newtree()" >New </a></td>
		<!--<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="edittree()" >Edit </a></td>-->
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removetree()" >Remove </a></td>
		</tr>
	</table>
</div>
<!--toolbar="#dlg-toolbar"-->
<div id="dlg" class="easyui-dialog" style="width:50%;height:380px;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
	<div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editparent()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
		<form id="fm" method="post" novalidate>
		<table cellpadding="5">
		<tr>
		<td>Menu Name</td>
		<td>: <input name="title" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Menu Name.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Parent to</td>
		<td>: <select id="combotree" style="width:200px" name="parentId" data-options="prompt:'Select Parent.'"></td>
		</tr>
		
		<tr>
		<td>Module Directory</td>
		<td>: <select id="combolink" style="width:200px" name="mod_name" data-options="prompt:'Select Module.'"></td>
		</tr>
		<tr>
		<td>Module File </td>
		<td>: <input id="cc2" class="easyui-combobox"  style="width:200px" name="file_mod" data-options="prompt:'Select File Module.',valueField:'file_mod',textField:'file_mod'"></td></tr>
		<tr>
		<td>Sort</td>
		<td>: <input name="sort" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Sort.'" required="true"></td>
		</tr>
		
		</table>
		<!--
		<table id="dgc" title="Role Permissions"  style="width:100%;height:300px" 
		toolbar="#toolbardgc" pagination="true"  pageSize="50"
		rownumbers="true" fitColumns="false" singleSelect="true"   idField="id_master_menu" sortName="id_master_menu" sortOrder="asc">
		<thead>
		<tr>
		<th data-options="field:'id_rp',width:80,sortable:true,hidden:true">ID sub</th>
		<th data-options="field:'id_sub_menu',width:80,sortable:true,hidden:true">ID sub</th>
		<th data-options="field:'id_a',width:100,sortable:true,formatter:function(value,row){
				return row.username;
			},editor:{type:'combobox',
							options:{
								url:'modul/modul/modulaction.php?act=username',
								valueField:'id_a',
								textField:'username',
								required:true
							}}">User</th>
		<th data-options="field:'view',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">View</th>
		<th data-options="field:'ins',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Add</th>
		<th data-options="field:'edit',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Edit</th>
		<th data-options="field:'delet',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Delete</th>
		<th data-options="field:'upload',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Upload</th>
		<th data-options="field:'srch',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Search</th>
		</tr>
		</thead>
		</table>
		<div id="toolbardgc">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dgc').edatagrid('addRow')" >New </a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dgc').edatagrid('saveRow')" >Confirm </a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dgc').edatagrid('cancelRow')" >Undo </a></td>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dgc').edatagrid('destroyRow')" >Remove </a></td>
		</div>-->
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savetree()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>

<script>
function status_master_menu_detail(val,row){
	if (val == '1' ){
		return '<span>Active</span>';
		} 
	else {
		return '<span>InActive</span>';
		}
	
}
var status_master_menu = [
		    {status:'1',status_text:'Active'},
		    {status:'0',status_text:'InActive'}
		];	
function status_sub(status_sub_menu,row,index){
	if(row.status_sub_menu == '1'){
		return '<span> Active</span>';
	}
	else {
	 return '<span> InActive</span>';
	}
}
function combo(){
	$('#combotree').combotree({
	url:'modul/tree/treeaction.php?act=aaa',
	valueField:'id',
	textField:'text',
	});
	$('#combolink').combobox({
	url:'control/view.php?act=filenya',
	valueField:'mod_name',
	textField:'mod_name',

	onSelect: function(rec){
	var url='modul/tree/c.php?id='+rec.mod_name;
	$('#cc2').combobox('reload', url);
	}
	});	
}

function expandAllx(){
	$('#tree').treegrid('expandAll');
			
}

function collapseAllx(){
	$('#tree').treegrid('collapseAll');
			
}
$('#tree').treegrid({
	url:'modul/tree/x.php',
	onDblClickRow:function(index,row){
				edittree(index);
			}
	});
function newtree(){
	$('#dlg').dialog('open').dialog('setTitle','New Main Menu');
	$('#fm').form('clear');
	$('#edt').linkbutton('disable');
	$('.comtree').show();	
	combo();
	$('#combotree').combotree('enable');
	$('#combolink').combobox('enable');
	$('#cc2').combobox('enable');
	$('.db').textbox('enable');
	$('.sv').linkbutton('enable');
	url = 'modul/tree/treeaction.php?act=create';
}

function edittree(){

		var row = $('#tree').datagrid('getSelected');
		if(row)
		{
		
		$('#dlg').dialog('open').dialog('setTitle','View / '+row.title);
		$('#fm').form('load',row);
		$('#edt').linkbutton('enable');
		$('.comtree').hide();
		combo();
		$('#combolink').combobox('setValue', row.mod_name);
		url = 'modul/tree/treeaction.php?act=update&id='+row.id;
		
		$('.db').textbox('disable');
		$('#combotree').combotree('disable');
		$('#combolink').combobox('disable');
		$('#cc2').combobox('disable');
		$('.sv').linkbutton('disable');
		/*$('#dgc').edatagrid({
		autoSave: false,
		url: 'modul/tree/treeaction.php?act=lisrolet&id='+row.id,
		saveUrl: 'modul/tree/treeaction.php?act=createroles&id='+row.id,
		updateUrl: 'modul/tree/treeaction.php?act=updateroles',	
		destroyUrl: 'modul/tree/treeaction.php?act=deleteroles',
		onAfterEdit:function(index,row){
			$('#dgc').edatagrid('reload');
			//$('#dg').edatagrid('reload');
		},
	destroyMsg:{
			norecord:{
				title:'Warning',
				msg:'No record is selected.'
			},
			confirm:{
				title:'Confirm',
				msg:'Are you sure you want to delete?'
			}
		},
	onError: function(index,row){
		$.messager.show({
			title:'Info',
			msg:row.msg
		});
	},
	onBeginEdit:function(index,row){
		$.messager.show({
			title:'Warning',
			msg:'Editing Mode'
		});
	},
	onCancelEdit:function(index,row){
		$.messager.show({
			title:'Info',
			msg:'Cancel Editing Mode'
		});
	}
	});*/	
		}		
}
function editparent(){
	$('.db').textbox('enable');
	$('#combotree').combotree('enable');
	$('#combolink').combobox('enable');
	$('#cc2').combobox('enable');
	$('.sv').linkbutton('enable');
}
	function savetree(){
			//to get the loaded data
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlg').dialog('close');		// close the dialog
						$('#tree').treegrid('reload');	// reload the user data
					}
				}
			});
}

function removetree(){
var row = $('#tree').treegrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/tree/treeaction.php?act=delete',{id:row.id},function(result){
if (result.success){
$('#tree').treegrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}
</script>