    <table id="bp" style="width:100%;height:100%" fit="true" border="false" fitColumns="true"
          
            rownumbers="true"
            idField="id_bp" bpField="field1" toolbar="#toolbar">
        <thead>
            <tr>
                <th field="id_bp" hidden="true" width="100" >ID</th>
                <th field="field1" width="100" >Title</th>
                <th field="field2" width="100" sortable="true" >Sort</th>
             
            </tr>
        </thead>
    </table>
<div id="toolbar">
	<table>
		<tr>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newbp()" >New </a></td>
		<!--<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editbp()" >Edit </a></td>-->
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removebp()" >Remove </a></td>
		</tr>
	</table>
</div>
<!--toolbar="#dlg-toolbar"-->
<div id="dlg" class="easyui-dialog" style="width:50%;height:380px;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
	<div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editparent()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
		<form id="fm" method="post" novalidate>
		<table cellpadding="5">
		<tr>
		<td>Field 1</td>
		<td>: <input name="field1" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Field 1.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Field 2</td>
		<td>:  <input name="field2" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Field 2.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Field 3</td>
		<td>:  <input name="field3" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Field 3.'" required="true"></td>
		</tr>
		<tr>
		<td>Field 4</td>
		<td>:  <input name="field4" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Field 4.'" required="true"></td></tr>
		<tr>
		<td>Field 5</td>
		<td>:  <input name="field5" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Field 5.'" required="true"></td>
		</tr>
		
		</table>
		<!--
		<table id="dgc" title="Role Permissions"  style="width:100%;height:300px" 
		toolbar="#toolbardgc" pagination="true"  pageSize="50"
		rownumbers="true" fitColumns="false" singleSelect="true"   idField="id_master_menu" sortName="id_master_menu" sortOrder="asc">
		<thead>
		<tr>
		<th data-options="field:'id_rp',width:80,sortable:true,hidden:true">ID sub</th>
		<th data-options="field:'id_sub_menu',width:80,sortable:true,hidden:true">ID sub</th>
		<th data-options="field:'id_a',width:100,sortable:true,formatter:function(value,row){
				return row.username;
			},editor:{type:'combobox',
							options:{
								url:'modul/modul/modulaction.php?act=username',
								valueField:'id_a',
								textField:'username',
								required:true
							}}">User</th>
		<th data-options="field:'view',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">View</th>
		<th data-options="field:'ins',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Add</th>
		<th data-options="field:'edit',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Edit</th>
		<th data-options="field:'delet',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Delete</th>
		<th data-options="field:'upload',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Upload</th>
		<th data-options="field:'srch',width:70,sortable:true,formatter:status_master_menu_detail,editor:{type:'combobox',
							options:{
								valueField:'status',
								textField:'status_text',
								data:status_master_menu,
								required:true
		}}">Search</th>
		</tr>
		</thead>
		</table>
		<div id="toolbardgc">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dgc').edatagrid('addRow')" >New </a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dgc').edatagrid('saveRow')" >Confirm </a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dgc').edatagrid('cancelRow')" >Undo </a></td>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dgc').edatagrid('destroyRow')" >Remove </a></td>
		</div>-->
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savebp()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>

<script>
function status_master_menu_detail(val,row){
	if (val == '1' ){
		return '<span>Active</span>';
		} 
	else {
		return '<span>InActive</span>';
		}
	
}
var status_master_menu = [
		    {status:'1',status_text:'Active'},
		    {status:'0',status_text:'InActive'}
		];	
function status_sub(status_sub_menu,row,index){
	if(row.status_sub_menu == '1'){
		return '<span> Active</span>';
	}
	else {
	 return '<span> InActive</span>';
	}
}

$('#bp').datagrid({
	url:'modul/bp/bpaction.php',
	onDblClickRow:function(index,row){
				editbp(index);
			}
	});
function newbp(){
	$('#dlg').dialog('open').dialog('setTitle','New Main Menu');
	$('#fm').form('clear');
	$('#edt').linkbutton('disable');
	
	url = 'modul/bp/bpaction.php?act=create';
}

function editbp(){

		var row = $('#bp').datagrid('getSelected');
		if(row)
		{
		
		$('#dlg').dialog('open').dialog('setTitle','View / '+row.title);
		$('#fm').form('load',row);
		$('#edt').linkbutton('enable');
		$('.combp').hide();
		combo();
		$('#combolink').combobox('setValue', row.mod_name);
		url = 'modul/bp/bpaction.php?act=update&id='+row.id;
		
		$('.db').textbox('disable');
		$('#combobp').combobp('disable');
		$('#combolink').combobox('disable');
		$('#cc2').combobox('disable');
		$('.sv').linkbutton('disable');
		/*$('#dgc').edatagrid({
		autoSave: false,
		url: 'modul/bp/bpaction.php?act=lisrolet&id='+row.id,
		saveUrl: 'modul/bp/bpaction.php?act=createroles&id='+row.id,
		updateUrl: 'modul/bp/bpaction.php?act=updateroles',	
		destroyUrl: 'modul/bp/bpaction.php?act=deleteroles',
		onAfterEdit:function(index,row){
			$('#dgc').edatagrid('reload');
			//$('#dg').edatagrid('reload');
		},
	destroyMsg:{
			norecord:{
				title:'Warning',
				msg:'No record is selected.'
			},
			confirm:{
				title:'Confirm',
				msg:'Are you sure you want to delete?'
			}
		},
	onError: function(index,row){
		$.messager.show({
			title:'Info',
			msg:row.msg
		});
	},
	onBeginEdit:function(index,row){
		$.messager.show({
			title:'Warning',
			msg:'Editing Mode'
		});
	},
	onCancelEdit:function(index,row){
		$.messager.show({
			title:'Info',
			msg:'Cancel Editing Mode'
		});
	}
	});*/	
		}		
}
function editparent(){
	$('.db').textbox('enable');
	$('#combobp').combobp('enable');
	$('#combolink').combobox('enable');
	$('#cc2').combobox('enable');
	$('.sv').linkbutton('enable');
}
	function savebp(){
			//to get the loaded data
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlg').dialog('close');		// close the dialog
						$('#bp').bpgrid('reload');	// reload the user data
					}
				}
			});
}

function removebp(){
var row = $('#bp').bpgrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/bp/bpaction.php?act=delete',{id:row.id},function(result){
if (result.success){
$('#bp').bpgrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}
</script>