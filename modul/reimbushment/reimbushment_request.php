<form id="fmreim" method="post" novalidate>
<div class="easyui-layout" style="width:100%;height:780px;">
	<div data-options="region:'north'" style="height:150px;width: 100%;border:0">
		<div data-options="region:'west',split:true" title="West" style="width:100%;">
			<table cellpadding="5" style="float:left;">

				<tr>
					<td>No Ref</td> <td>: <input id="refno" required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'Ref No.'" name="refno"/></td>
				</tr>

				<tr>
					<td>Description</td> <td>: <input name="nama_kegiatan" id="nk" class="easyui-textbox" style="width:250px;" data-options="prompt:'Nama Kegiatan.'" disabled></td>
				</tr>

			</table>
		</div>
		<div data-options="region:'east',split:true" title="West" style="width:100%;background-color: red;">
		<table cellpadding="5" style="float:right;margin-right: 90px;">
			<tr>
				<td>Date</td> <td>: <input name="datenow" id="datenow" class="easyui-textbox" style="width:250px;" data-options="prompt:'Date No.'" readonly></td>
			</tr>
			<tr>
				<td>Reimbushment Request No</td> <td>: <input name="cano" id="cano" class="easyui-textbox" style="width:250px;" data-options="prompt:'Reimbushment No.'" readonly></td>
			</tr>
			<tr>
				<td>BP</td> <td>: <input name="name_tbp" id="name_tbp" class="easyui-textbox" style="width:250px;" data-options="prompt:'Bidang Pelayanan.'" readonly></td>
			</tr>
			<tr>
				<td>BD</td> <td>: <input name="name_tbd" id="name_tbd" class="easyui-textbox" style="width:250px;" data-options="prompt:'Bidang Pelayanan.'" readonly></td>
			</tr>
		</table>
		</div>
	</div>

	<div data-options="region:'center',border:false,plain:false" style="height:450px;" >
		<div class="easyui-tabs" id="tt" data-options="border:false,plain:true" style="height:350px;" >
			<div title="Rincian"style="padding:5px;" >
				<table id="detkeg"
					   data-options="singleSelect:true,fit:true,fitColumns:true"
					   showFooter="true" idField="id_reim_temp" tbpField="cost_name" pagination="true"
					   rownumbers="true" pageSize="50">
					<thead>
					<tr>
						<th field="ck" checkbox="true"></th>
						<th data-options="field:'id_reim_temp'" hidden="true" width="80">ID</th>
						<th data-options="field:'cost_name'" width="150">Rincian Anggaran</th>
						<th data-options="field:'total',formatter:formatPrice" width="150" >Anggaran</th>
						<th data-options="field:'ca_req',formatter:formatPrice,align:'right'" width="150">CA Request</th>
						<th data-options="field:'paid',formatter:formatPrice,align:'right'" width="150">Paid</th>
						<th data-options="field:'settlement',formatter:formatPrice,align:'right',editor:{type:'numberbox',options:{precision:2}}" width="150">Settlement</th>
						<th data-options="field:'balance',formatter:formatPrice,align:'center'" width="150">Balance</th>
						<th data-options="field:'remarks',align:'center',editor:{type:'textbox'}" width="150">Remarks</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div data-options="region:'south',split:false,border:false" style="height:250px;">
		<table cellpadding="5" style="float:left;">
			<tr>
				<td>Paid To</td><td>: <select name="typepm" style="width:200px;" id="typepm" class="easyui-combobox" data-options="prompt:'Type.'" required="true">
						<option ></option>
						<option value="1">Cash</option>
						<option value="2">Transfer</option></select></td>
			</tr>
			<tr class="trf">
				<td>Rek No</td><td>: <input  id="rek_no" class="easyui-textbox" style="width:200px;" data-options="prompt:'Rek No.'" name="rek_no"/></td>
			</tr>

			<tr class="trf">
				<td>Bank </td><td>: <input id="bankid" style="width:200px;" data-options="prompt:'Bank.'" name="bank_name"/></td>
			</tr>
			<tr class="trf">
				<td>A / n</td><td>: <input id="an" class="easyui-textbox" style="width:200px;" data-options="prompt:'A/n.'" name="an"/></td>
			</tr>
		<tr>
			<td></td>
		<td>
		<a href="javascript:void(0)" id="confirm" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmca()" style="width:90px">Confirm</a>
		<a id="printcash" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printcash()" style="width:90px">Print</a>
		<a id="printtrf" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printrf()" style="width:90px">Print</a>
		<a href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="clearca()" style="width:90px">Clear</a>
		</td>
		</tr>
		</table>
	</div>
</div>
</form>
<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#printcash').linkbutton('disable');
		$('#printtrf').linkbutton('disable');
		$('#detkeg').hide();
		$('#printcash').hide();
		$('#printtrf').hide();
		$('.hide').hide();
		$('.trf').hide();
		$('#bankid').combobox();
	});
</script>
<script>
	$('#typepm').combobox({
		valueField: 'typepm',
		onSelect: function (rec) {
			$('.hide').hide();
			$('.trf').hide();
			if (rec.typepm == 1) {
				$('.hide').show();
				$('.trf').hide();

			}
			else if(rec.typepm == 2) {
				$('.trf').show();
				$('.hide').show();
				$('#bankid').combobox({
					url:'modul/bank/bankaction.php?act=listbank',
					valueField:'idbank',
					textField:'bank_name'
				});

			}
		}
	});

	$('#refno').combobox({
		url: 'control/view.php?act=listrefno',
		valueField: 'id',
		textField: 'refno',
		onSelect: function(rec){
			$('#cano').textbox('setValue','');
			$('#datenow').textbox('setValue','');
			$('#detkeg').edatagrid({
				url:'modul/reimbushment/reimbushment_action.php?act=reim_temp&id='+rec.id,
				pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
				emptyMsg: 'No Records Found',
				updateUrl: 'modul/reimbushment/reimbushment_action.php?act=update',
				onSuccess:function(index,row){
					$.messager.show({
						title:'Info',
						msg:row.success
					});
					$('#detkeg').edatagrid('reload');
				},
				onError: function(index,row){
					$.messager.show({
						title:'Info',
						msg: row.errorMsg
					});
				}
			}).edatagrid('enableCellEditing').edatagrid('gotoCell', {
				index: 0,
				field: 'id_ca_temp',
			});
			var dateObj = new Date();
			var month = dateObj.getUTCMonth() + 1; //months from 1-12
			var day = dateObj.getUTCDate();
			var year = dateObj.getUTCFullYear();

			newdate =  day + "-" + month + "-" + year;
			var refno = $('#refno').combobox('getText');

			$('#refnotext').textbox('setValue',refno);
			$('#id_tbp').textbox('setValue',rec.id_tbp);
			$('#name_tbp').textbox('setValue',rec.name_tbp);
			$('#id_tbd').textbox('setValue',rec.id_tbd);
			$('#name_tbd').textbox('setValue',rec.name_tbd);
			$('#nk').textbox('setValue',rec.nama_kegiatan);
			$('#datenow').textbox('setValue',newdate);
			$('#an').textbox('getValue');
			$('#bankid').combobox('getValue');
			$('#typepm').combobox('getValue');
			$('#rek_no').textbox('getValue');

		}

	});

	function formatItem(row){
		var s = '<span style="font-weight:bold">' + row.refno + '</span><br/>' +
			'<span style="color:#888"> Nama Kegiatan : ' + row.nama_kegiatan + '</span><br/>';
		return s;
	}

	function confirmca(){
		var cekpaidto = $('#typepm').combobox('getValue');
		if(cekpaidto == '1' || cekpaidto == '2'){
			$.messager.confirm('Confirm', 'Are you sure you want to Confirm this ?', function (r) {
				if (r) {

					if(cekpaidto == '1') {
						$('#printcash').show();
						$('#printtrf').hide();
						$('#printcash').linkbutton('enable');
					}
					else {
						$('#printtrf').show();
						$('#printcash').hide();
						$('#printtrf').linkbutton('enable');
					}
					$('#fmreim').form('submit',{
						url : 'modul/reimbushment/reimbushment_action.php?act=create',
						onSubmit: function(){
							return $('#fmreim').form('validate');
						},
						success: function(result){
							var result = eval('('+result+')');

							if (result.errorMsg){
								$.messager.show({
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								//$('#dlgxx').dialog('close');		// close the dialog
								$('#confirm').hide();
								$('#confirm').linkbutton('disable');
//								$('#refno').combobox('clear');
								$('#cano').textbox('setValue', result.gennum);
								$('#detkeg').datagrid('loadData', {"total":0,"rows":[]});
								$.messager.show({
									title: 'Success',
									msg: result.success
								});
							}
						}
					});

				}

			});
		}
		else {
			$.messager.show({
				title: 'Error',
				msg: 'Paid to required'
			});
		}
	}
	function testing(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: "modul/reimbushment/proses.php",
			data:{detail:JSON.stringify($('#detkeg').datagrid('getRows'))},
			cache: false,
			success: function(msg){
			}
		});

//

	}
	function clearca(){
		$('#detkeg').datagrid({
			url:'',
			emptyMsg: 'No Records Found',
		});
		$('#refno').combobox('setValue','');
		$('#name_tbd').textbox('setValue','');
		$('#datenow').textbox('setValue','');
		$('#name_tbp').textbox('setValue','');
		$('#nk').textbox('setValue','');
		$('#cano').textbox('setValue','');
		$('#printca').linkbutton('disable');
	}
	function printcash(){
		url = "modul/cashadvance/reimbushment_report.php?bp="+$('#name_tbp').textbox('getValue')+"&bd="+$('#name_tbd').textbox('getValue')+"&nk="+$('#nk').textbox('getValue')+
													"&cano="+$('#cano').textbox('getValue')+"&refno="+$('#refno').combobox('getText')+"&typepm="+$('#typepm').combobox('getValue');
		window.open(url);
	}
	function printrf(){
		url = "modul/cashadvance/reimbushment_report.php?bp="+$('#name_tbp').textbox('getValue')+"&bd="+$('#name_tbd').textbox('getValue')+"&nk="+$('#nk').textbox('getValue')+
													"&cano="+$('#cano').textbox('getValue')+"&refno="+$('#refno').combobox('getText')+"&typepm="+$('#typepm').combobox('getValue')+
													"&bankid="+$('#bankid').combobox('getValue')+"&rek_no="+$('#rek_no').textbox('getValue')+"&an="+$('#an').textbox('getValue');
		window.open(url);
	}
</script>