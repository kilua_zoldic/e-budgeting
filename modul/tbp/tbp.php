    <table id="tbp" style="width:100%;height:100%;" fit="true" border="false" fitColumns="true"
			rownumbers="true" pageSize="50" showFooter="true"  pagination="true" singleSelect="true"
            idField="id_tbp" tbpField="name_tbp" toolbar="#toolbar">
        <thead>
            <tr>
                <th field="id_tbp" hidden="true" width="100" >ID</th>
                <th field="name_tbp" width="100" sortable="true">Name</th>
                <th field="desc_tbp" width="100" sortable="true" >Description</th>
                <th field="create_by" width="100" sortable="true" >Create By</th>
                <th field="create_date" width="100" sortable="true" >Create Time</th>
                <th field="update_by" width="100" sortable="true" >Update By</th>
                <th field="update_date" width="100" sortable="true" >Update Date</th>
                <th field="status_tbp" width="100" sortable="true" formatter="status_tbp">Status</th>
             
            </tr>
        </thead>
    </table>

<div id="toolbar">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newtbp()" >New </a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removetbp()" >Remove </a>
	<!--<table cellpadding="5">
	<tr>
	<td>Name</td><td>:
		<input class="easyui-textbox" style="width:160px"  id="name_tbp" name="name_tbp" data-options="prompt:'Name.'"></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" style="width:90px" onclick="search_tbp()">Search</a></td>
	</tr>
	</table>-->
</div>
<!--toolbar="#dlg-toolbar"-->
<div id="dlg" class="easyui-dialog" style="width:50%;height:380px;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
	<div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
		<form id="fm" method="post" novalidate>
		<table cellpadding="5">
		<tr>
		<td>Name </td>
		<td>: <input name="name_tbp" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Name.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Description</td>
		<td>:  <input name="desc_tbp" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Description.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Status</td>
		<td>:  <select  name="status_tbp" style="width:200px;" class="easyui-combobox dbc" data-options="prompt:'Status.'" required="true">
		<option></option>
		<option value="0">InActive</option>
		<option value="1">Active</option>
		</select>
		</td>
		</tr>
		
		</table>
		
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savetbp()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>

<script>
var dg = tbp;
function status_tbp(val,row){
	if (val == '1' ){
		return '<span>Active</span>';
		} 
	else {
		return '<span>InActive</span>';
		}
	
}

$('#tbp').datagrid({
	url:'modul/tbp/tbpaction.php?act=list',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				edittbp(index);
			},
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
                }		
	});
$('#tbp').datagrid('enableFilter',[{
				field:'status_tbp',
				type:'combobox',
				options:{
					panelHeight:'auto',
					data:[{value:'',text:'All'},{value:'1',text:'Active'},{value:'0',text:'InActive'}],
					onChange:function(value){
						if (value == ''){
							$('#tbp').datagrid('removeFilterRule', 'status_tbp');
						} else {
							$('#tbp').datagrid('addFilterRule', {
								field: 'status_tbp',
								op: 'equal',
								value: value
							});
						}
						$('#tbp').datagrid('doFilter');
					}
				}
			}]);	

function newtbp(){
	$('#dlg').dialog('open').dialog('setTitle','New');
	$('#fm').form('clear');
	$('.db').textbox('enable');
	$('.dbc').combobox('enable');
	$('#edt').linkbutton('disable');
	$('.sv').linkbutton('enable');
	
	url = 'modul/tbp/tbpaction.php?act=create';
}

function edittbp(){

		var row = $('#tbp').datagrid('getSelected');
		if(row)
		{
		
		$('#dlg').dialog('open').dialog('setTitle','View / '+row.name_tbp+ ' / ' +row.desc_tbp);
		$('#fm').form('load',row);
		$('#edt').linkbutton('enable');
		url = 'modul/tbp/tbpaction.php?act=update&id='+row.id_tbp;
		$('.db').textbox('disable');
		$('.dbc').combobox('disable');	
		$('.sv').linkbutton('disable');		
		$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
		}		
}
function editfield(){
	$('.db').textbox('enable');
	$('.dbc').combobox('enable');
	$('.sv').linkbutton('enable');
}
	function savetbp(){
			//to get the loaded data
			$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlg').dialog('close');		// close the dialog
						$('#tbp').datagrid('reload');	// reload the user data
					}
				}
			});}
			});
}

function removetbp(){
var row = $('#tbp').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/tbp/tbpaction.php?act=delete',{id:row.id_tbp},function(result){
if (result.success){
$('#tbp').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function search_tbp(){
$('#tbp').datagrid('load',{
	name_tbp: $('#name_tbp').val()
	});
}
</script>