<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 12/18/16
 * Time: 3:47 PM
 */
?>

<form>
        <div class="easyui-layout" style="width:auto;height:500px;">
            <div data-options="region:'north'" style="height:150px;width: 100%;border:0">
                <div data-options="region:'west',split:true" title="West" style="width:100%;">
                    <table cellpadding="5" style="float:left;">
                        <tr>
                            <td>Cash / Transfer</td> <td>: <select  name="care" id="care"  style="width:200px;"  data-options="prompt:'Cash / Reimbursement.'" required="true">
                                    <option></option>
                                    <option value="0">Cash</option>
                                    <option value="1">Transfer</option>
                                </select></td>
                        </tr>
                    </table>
                </div>

            </div>
            <div data-options="region:'south',split:false,border:false" style="height:160px;">
                <table cellpadding="5" style="float:left;">
                    <tr>
                        <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmca()" style="width:90px">Confirm</a></td>
                    </tr>
                </table>

            </div>
        <div data-options="region:'center',border:false,plain:false" style="height:200px;" >
<!--            <div class="easyui-tabs" data-options="border:false,plain:true" style="height:200px;" >-->
<!--                <div title="Rincian" style="padding:5px;" >-->
                <table id="detkeg"
                       data-options="singleSelect:true,fit:true,fitColumns:true,showFooter:true" style="width:700px;height:250px">
                    <thead>
                    <tr>
                        <th data-options="field:'cx',editor:{type:'datebox',options:{required:true,validType:'validDate[\'dd-MM-yyyy\']' }}" width="150">Confirm</th>
                        <th data-options="field:'id'" hidden="true" width="80">ID</th>
                        <th data-options="field:'payment_no'" width="150">Payment No</th>
<!--                        <th data-options="field:'bp'" width="150">Bidang Pelayanan</th>-->
                        <th data-options="field:'norek'" width="150">No Rek</th>
                        <th data-options="field:'payment_method'" width="150">Payment Method</th>
                        <th data-options="field:'bank'" width="150">Bank</th>
<!--                        <th data-options="field:'bd'" width="150">Badan Pelayanan</th>-->
<!--                        <th data-options="field:'sub_total',align:'right',formatter:formatPrice" width="130"> Anggaran</th>-->
                        <th data-options="field:'paid',align:'right',formatter:formatPrice" width="130">Paid</th>
                        <th data-options="field:'settle',align:'right',formatter:formatPrice" width="130">Settlement</th>
                        <th data-options="field:'ts',align:'right',formatter:formatPrice" width="130">Total Settlement</th>
                        <th data-options="field:'ca',align:'right',formatter:formatPrice" width="130">CA</th>
                        <th data-options="field:'status',align:'center'" width="150">Balance</th>
                            </tr>
                    </thead>
                </table>
<!--                    </div>-->
<!--                </div>-->
            </div>

        </div>
</form>

<div id="dlgpayment" class="easyui-dialog" style="width:100%;height:60%;"  maximizable="true" closed="true"  buttons="#dlgpayment-buttons">
    <table id="calist" style="width:100%;height:450px;" fit="true"
           rownumbers="true" showFooter="true" fitColumns="true" toolbar="#toolbartree"
           idField="id" showFooter="true" Field="cash_advance" sortName="cash_advance" singleSelect="false" sortOrder="asc" >
        <thead >
        <tr>
            <th data-options="field:'cash_advance'" width="150">Cash Advance No</th>
<!--            <th data-options="field:'bp'" width="150">Bidang Pelayanan</th>-->
<!--            <th data-options="field:'bd'" width="150">Badan Pelayanan</th>-->
<!--            <th data-options="field:'total',formatter:formatPrice" width="150">Anggaran</th>-->
            <th data-options="field:'norek'" width="150">No Rek</th>
            <th data-options="field:'payment_method'" width="150">Payment Method</th>
            <th data-options="field:'bank'" width="150">Bank</th>
            <th data-options="field:'paid',align:'right',formatter:formatPrice" width="150">Paid</th>
            <th data-options="field:'settle',align:'right',formatter:formatPrice" width="150">Settlement</th>
            <th data-options="field:'ca',align:'right',formatter:formatPrice" width="150">CA</th>
            <th data-options="field:'balance',align:'center',formatter:formatPrice" width="150">Balance</th>
        </tr>

        </thead>
    </table>

</div>
<div id="dlgpayment-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgpayment').dialog('close')" style="width:90px">Close</a>
</div>

<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.extend($.fn.datebox.defaults.rules, {
            validDate: {
                validator: function(value){
                    var date = $.fn.datebox.defaults.parser(value);
                    var s = $.fn.datebox.defaults.formatter(date);
                    return s==value;
                },
                message: 'Please input the correct date format'
            }
        });
        $('#printca').linkbutton('disable');
    });
</script>
<script>
    $('#care').combobox({
        valueField: 'care',
        onSelect: function (rec) {

            if (rec.care == 1) {
                $('#detkeg').edatagrid({
                    url:'modul/bank_payment/listpayment.json',
                    pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
                    emptyMsg: 'No Records Found',
                });
//                var dateObj = new Date();
//                var month = dateObj.getUTCMonth() + 1; //months from 1-12
//                var day = dateObj.getUTCDate();
//                var year = dateObj.getUTCFullYear();
//
//                newdate =  day + "-" + month + "-" + year;
//                $('#datenow').textbox('setValue',newdate);
//                $('#cano').textbox('setValue','P-1701000001');
//                $('#nogiro').textbox('setValue','1234567');
            }
            else if(rec.care == 0) {
                alert('Data Kosong')
            }
        }
    });
    $('#detkeg').datagrid({
    });



    function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.refno + '</span><br/>' +
            '<span style="color:#888"> Nama Kegiatan : ' + row.nama_kegiatan + '</span><br/>';
        return s;
    }

    function confirmca(){
        //to get the loaded data
        //alert(url);
        $.messager.confirm('Confirm','Are you sure you want to Confirm this ?',function(r){
            if (r){
                var dateObj = new Date();
                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                var day = dateObj.getUTCDate();
                var year = dateObj.getUTCFullYear();

                newdate =  day + "-" + month + "-" + year;
                $('#datenow').textbox('setValue',newdate);
                $('#cano').textbox('setValue','P-1701000001');
                $('#printca').linkbutton('enable');
            }
        });
    }
    function printca(){
        alert('a');
    }

    function clearca(){
        $('#detkeg').datagrid({
            url:'',
        });
        $('#refno').combobox('setValue','');
        $('#name_tbp').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#cano').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
</script>
