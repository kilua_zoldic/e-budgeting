<table id="arkat" fit="true" border="false" style="width:100%;height:100%;" pageSize="50"
       toolbar="#toolbararkat"  treeField="arkat_name" pagination="true"  maximizable="true"  fitColumns="true"
       rownumbers="true" singleSelect="true"  idField="idar_kategori" showFooter="true" sortName="idar_kategori" sortOrder="asc"
>
    <thead>
    <tr>
        <th field="code" width="100" sortable="true" >Code</th>
        <th data-options="field:'arkat_name',width:150,sortable:true">Name</th>
        <th field="kategori" width="100" formatter="kategori_status" sortable="true" >Kategori</th>
        <th field="status" width="100" formatter="status_proker" sortable="true" >Status</th>
    </tr>
    </thead>
</table>
<div id="toolbararkat">
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newarkat()">New Kategori</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deletearkat()">Remove Kategori</a>
    <!--<table cellpadding="5">
        <tr>
        <td>Unit Name</td><td>:
            <input class="easyui-textbox" style="width:160px"  id="cost_name" data-options="prompt:'Unit Name.'"></td>
            <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" style="width:90px" onclick="search_ct()">Search</a></td>
        </tr>
    </table>-->
</div>

<div id="dlg" class="easyui-dialog" style="width:50%;height:350px;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
    <div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
    <form id="fm" method="post" novalidate>
        <table cellpadding="5">
            <tr>
                <td>Code</td>
                <td>: <input name="code" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Code.'" required="true"></td>
            </tr>
            <tr>
                <td>Kategori Name </td>
                <td>: <input name="arkat_name" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Kategori Name.'" required="true"></td>
            </tr>
            <tr>
                <td>Parent to</td>
                <td>: <select id="combotree" style="width:200px" name="parentId" data-options="prompt:'Select Parent.'"></td>
            </tr>
            <tr>
                <td>Kategori</td>
                <td>:  <select  name="kategori" style="width:200px;" class="easyui-combobox dbc" data-options="prompt:'Kategori.'" required="true">
                        <option></option>
                        <option value="2">Kebaktian</option>
                        <option value="1">Rupa - Rupa</option>
                    </select>
                </td>
            </tr>
            <tr>
            <td>Status</td>
            <td>:  <select  name="status" style="width:200px;" class="easyui-combobox dbc" data-options="prompt:'Status.'" required="true">
                    <option></option>
                    <option value="0">InActive</option>
                    <option value="1">Active</option>
                </select>
            </td>
            </tr>

        </table>

    </form>
</div>
<div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savearkat()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>
<script>
    var url;
    var dg = arkat;

    $('#arkat').treegrid({
        url: 'modul/ar_kategori/arkataction.php?act=list',
        onDblClickRow:function(index,row){
            editarkat(index);
        },
        onHeaderContextMenu: function(e, field){
            e.preventDefault();
            if (!cmenu){
                createColumnMenu(dg);
            }
            cmenu.menu('show', {
                left:e.pageX,
                top:e.pageY
            });
        }
    });
    
    function kategori_status(val,row){
        console.log(row);
        if (row.kategori == '1' ){
            return '<span>Rupa - Rupa</span>';
        }
        else  {
            return '<span>Kebaktian</span>';
        }

    }

    $('#arkat').treegrid('enableFilter',[{
        field:'status_arkat',
        type:'combobox',
        options:{
            panelHeight:'auto',
            data:[{value:'',text:'All'},{value:'1',text:'Active'},{value:'0',text:'InActive'}],
            onChange:function(value){
                if (value == ''){
                    $('#arkat').treegrid('removeFilterRule', 'status_arkat');
                } else {
                    $('#arkat').treegrid('addFilterRule', {
                        field: 'status_arkat',
                        op: 'equal',
                        value: value
                    });
                }
                $('#arkat').treegrid('doFilter');
            }
        }
    }]);
    function newarkat(){
        $('#dlg').dialog('open').dialog('setTitle','New Kategori');
        $('#fm').form('clear');
        combo();
        $('.db').textbox('enable');
        $('.dbc').combobox('enable');
        $('#edt').linkbutton('disable');
        $('.sv').linkbutton('enable');

        url = 'modul/ar_kategori/arkataction.php?act=create';
    }

    function editarkat(){
        var row = $('#arkat').treegrid('getSelected');
        if(row)
        {
            $('#dlg').dialog('open').dialog('setTitle','View / '+row.arkat_name);
            combo();
            $('#fm').form('load',row);
            $('#edt').linkbutton('enable');
            url = 'modul/ar_kategori/arkataction.php?act=update&id='+row.idar_kategori;
            $('.db').textbox('disable');
            $('.dbc').combobox('disable');
            $('.sv').linkbutton('disable');
            $('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
        }
    }

    function savearkat(){
        //to get the loaded data
        $.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
            if (r){
                $('#fm').form('submit',{
                    url: url,
                    onSubmit: function(){
                        return $(this).form('validate');
                    },
                    success: function(result){
                        var result = eval('('+result+')');
                        if (result.errorMsg){
                            $.messager.show({
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        } else {
                            $.messager.show({
                                title: 'Success',
                                msg: result.success
                            });
                            $('#dlg').dialog('close');		// close the dialog
                            $('#arkat').treegrid('reload');	// reload the user data
                        }
                    }
                });}
        });
    }

    function deletearkat(){
        var row = $('#arkat').treegrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
                if (r){
                    $.post('modul/ar_kategori/arkataction.php?act=delete',{id:row.idar_kategori},function(result){
                        if (result.success){
                            $('#arkat').treegrid('reload'); // reload the user data
                        } else {
                            $.messager.show({ // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }

    function search_ct(){
        $('#arkat').treegrid('load',{
            cost_name: $('#arkat_name').val()
        });
    }

    function combo(){
        $('#combotree').combotree({
            url:'modul/ar_kategori/arkataction.php?act=listparentkat',
            valueField:'id',
            textField:'text',
        });
    }

</script>