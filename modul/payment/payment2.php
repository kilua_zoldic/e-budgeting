<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 12/18/16
 * Time: 3:47 PM
 */
?>

<form>
        <div class="easyui-layout" style="width:auto;height:500px;">
            <div data-options="region:'north'" style="height:150px;width: 100%;border:0">
                <div data-options="region:'west',split:true" title="West" style="width:100%;">
                    <table cellpadding="5" style="float:left;">
                        <tr>
                            <td>Badan Pelayanan</td> <td>:
                                <input required="true" id="bp" style="width:200px;" data-options="prompt:'Badan Pelayanan.'" class="easyui-combobox" name="name_tbp"/></td>
                        </tr>
<!--                        <tr>-->
<!--                            <td>Cash Advance No</td> <td>:-->
<!--                                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" onclick="modal();" style="width:40px"></a></td>-->
<!--                        </tr>-->
                        <tr>
                            <td>Cash / Transfer</td> <td>: <select  name="care" id="care" style="width:200px;"  data-options="prompt:'Cash / Transfer.'" required="true">
                                    <option></option>
                                    <option value="0">Cash</option>
                                    <option value="1">Transfer</option>
                                </select></td>
                        </tr>
                        <tr >
                               <td>Bank Pengirim</td><td>: <input required="true" id="bankid" style="width:200px;" data-options="prompt:'Bank.'" name="bank_name"/></td>
                        </tr>
                       
                    </table>
                    <table cellpadding="5" style="float:right;margin-right: 90px;">
                        <tr>
                            <td>Date</td> <td>: <input name="datenow" id="datenow" class="easyui-textbox" style="width:250px;" data-options="prompt:'Date.'" readonly></td>
                        </tr>
                        <tr>
                            <td>Payment No</td> <td>: <input name="cano" id="cano" class="easyui-textbox" style="width:250px;" data-options="prompt:'Payment No.'" readonly></td>
                        </tr>
                        <tr>
                            <td>No Giro</td> <td>: <input name="nogiro" id="nogiro" class="easyui-textbox" style="width:250px;" data-options="prompt:'No Giro.'" ></td>
                        </tr>
                    </table>
                </div>

            </div>
            <div data-options="region:'south',split:false,border:false" style="height:160px;">
                <table cellpadding="5" style="float:left;">
                    <tr>
                        <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmca()" style="width:90px">Confirm</a></td>
                    </tr>
                </table>
                
            </div>
        <div data-options="region:'center',border:false,plain:false" style="height:200px;" >
<!--            <div class="easyui-tabs" data-options="border:false,plain:true" style="height:200px;" >-->
<!--                <div title="Rincian" style="padding:5px;" >-->
                <table id="detkeg"
                       data-options="singleSelect:false,fit:true,fitColumns:true,showFooter:true" style="width:700px;height:250px">
                    <thead>
                    <tr>
                        <th field="ck" checkbox="true"></th>
                        <th data-options="field:'id'" hidden="true" hwidth="80">ID</th>
                        <th data-options="field:'cash_advance'" width="150"> No</th>
<!--                        <th data-options="field:'bp'" width="150">Bidang Pelayanan</th>-->
                        <th data-options="field:'norek'" width="150">No Rek</th>
                        <th data-options="field:'payment_method'" width="150">Payment Method</th>
                        <th data-options="field:'bank'" width="150">Bank</th>
<!--                        <th data-options="field:'bd'" width="150">Badan Pelayanan</th>-->
<!--                        <th data-options="field:'sub_total',align:'right',formatter:formatPrice" width="130"> Anggaran</th>-->
                        <th data-options="field:'paid',align:'right',formatter:formatPrice" width="130">Paid</th>
                        <th data-options="field:'settle',align:'right',formatter:formatPrice" width="130">Settlement</th>
                        <th data-options="field:'ts',align:'right',formatter:formatPrice" width="130">Total Settlement</th>
                        <th data-options="field:'ca',align:'right',formatter:formatPrice" width="130">CA</th>
                        <th data-options="field:'status',align:'center'" width="150">Balance</th>
                            </tr>
                    </thead>
                </table>
<!--                    </div>-->
<!--                </div>-->
            </div>

        </div>
</form>



<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#printca').linkbutton('disable');
        $('.hide').hide();
        $('.trf').hide();

    });
</script>
<script>
    $('#bp').combobox({
        url:'control/view.php?act=tbp',
        valueField:'id_tbp',
        textField:'name_tbp',
    });
    var listpayment;
    $('#care').combobox({
        valueField: 'id',
        onSelect: function (rec) {
            if(rec.id == 0) {

                listpayment='modul/payment/list_payment.json';
            }
            else if(rec.id == 1 ){
                listpayment='modul/payment/list_paymentreimbushment.json';
            }
            $('#detkeg').edatagrid({
                url: listpayment,
                pageList: [10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500],
                emptyMsg: 'No Records Found'
            });
        }

    });
//    $('#typepm').combobox({
//        valueField: 'typepm',
//        onSelect: function (rec) {
//            $('.hide').hide();
//            $('.trf').hide();
//            if (rec.typepm == 1) {
//                $('.hide').show();
//                $('.trf').hide();
//
//            }
//            else if(rec.typepm == 2) {
//                $('.trf').show();
//                $('.hide').show();
//                $('#bankid').combobox({
//                    url:'modul/bank/bankaction.php?act=listbank',
//                    valueField:'idbank',
//                    textField:'bank_name'
//                });
//            }
//        }
//    });
    $('#detkeg').datagrid({
    });
    $('#bankid').combobox({
        url:'modul/bank/bankaction.php?act=listbank',
        valueField:'idbank',
        textField:'bank_name'
    });

    $('#refno').combobox({
        url: 'modul/cashadvance/cashadvanceaction.php?act=list_refno',
        valueField: 'id',
        textField: 'refno',
        onSelect: function(rec){
            $('#name_tbp').textbox('setValue',rec.name_tbp);
            $('#nk').textbox('setValue',rec.nama_kegiatan);
//			$('#time').textbox('setValue',rec.wktu);
//			$('#dcost').textbox('setValue',rec.desc_cost);
//			$('#jenis').combobox('setValue',rec.jenis);
//			$('#total').numberbox('setValue','17000000');
//			$('#rev').numberbox('setValue','10000000');
            $('#detkeg').edatagrid({
                url:'modul/cashadvance/cashadvanceaction.php?act=detkeg&id='+rec.id,
                pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
//				onDblClickRow:function(index,row){
//					//editproker(index);
//					detrincian(index,row);
//				},
//				view: myview,
                emptyMsg: 'No Records Found',
            });

        }

    });

    function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.refno + '</span><br/>' +
            '<span style="color:#888"> Nama Kegiatan : ' + row.nama_kegiatan + '</span><br/>';
        return s;
    }

    function confirmca(){
        //to get the loaded data
        //alert(url);
        $.messager.confirm('Confirm','Are you sure you want to Confirm this ?',function(r){
            if (r){
//				$('#fmxx').form('submit',{
//					url: url, // ini dari mana ya?
//					onSubmit: function(){
//						return $('#fmxx').form('validate');
//					},
//					success: function(result){
//						var result = eval('('+result+')');
//						if (result.errorMsg){
//							$.messager.show({
//								title: 'Error',
//								msg: result.errorMsg
//							});
//						} else {
//							$('#dlgxx').dialog('close');		// close the dialog
//							$('#tree').treegrid('reload');	// reload the user data
//							$('#proker').datagrid('reload');	// reload the user data
//
//
//							$.messager.show({
//								title: 'Success',
//								msg: result.success
//							});
//						}
//					}
//				});
                var dateObj = new Date();
                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                var day = dateObj.getUTCDate();
                var year = dateObj.getUTCFullYear();

                newdate =  day + "-" + month + "-" + year;
                $('#datenow').textbox('setValue',newdate);
                $('#cano').textbox('setValue','P-1701000001');
                $('#printca').linkbutton('enable');
            }
        });
    }
    function printca(){
        alert('a');
    }

    function clearca(){
        $('#detkeg').datagrid({
            url:'',
        });
        $('#refno').combobox('setValue','');
        $('#name_tbp').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#cano').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
</script>
