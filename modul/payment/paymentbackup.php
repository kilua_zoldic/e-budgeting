<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 12/18/16
 * Time: 3:47 PM
 */
?>

<form>
<!--    <div class="easyui-layout" style="width:100%;height:600px;">-->
<!--        <div data-options="region:'north'" style="height:90px;width: 100%;border:0">-->
<!--            <div data-options="region:'west',split:true" title="West" style="width:100%;">-->
                <table cellpadding="5" style="float:left;">
                    <tr>
                        <td>Payment Cash Advance</td> <td>: <input class="easyui-textbox" style="width:100%;" data-options="
                    labelPosition: 'top',
                    prompt: 'Input something here!',
                    iconWidth: 22,
                    icons: [{
                        iconCls:'icon-search',
                        handler: function(e){
                            modal();
                        }
                    }]
                    "></td>
                    </tr>
                </table>
<!--            </div>-->
<!--        </div>-->

<!--        <div data-options="region:'center',border:false,plain:false" style="height:300px;" >-->
<!--            <div class="easyui-tabs" data-options="border:false,plain:true" style="height:300px;" >-->
<!--                <div title="Rincian" style="padding:5px;" >-->
                    <table id="detkeg"
                           data-options="method:'get',singleSelect:true,fit:true,fitColumns:true">
                        <thead>
                        <tr>
                            <th data-options="field:'id'" width="80">ID</th>
                            <th data-options="field:'cost_name'" width="150">Cash Advance No</th>
                            <th data-options="field:'cost_name'" width="150">Total Anggaran</th>
                            <th data-options="field:'unitcost',align:'right'" width="150">Paid</th>
                            <th data-options="field:'attr1',align:'right'" width="150">Settlement</th>
                            <th data-options="field:'attr1',align:'right'" width="150">CA</th>
                            <th data-options="field:'status',align:'center'" width="150">Balance</th>
<!--                            <th data-options="field:'cost_price',editor:{type:'numberbox',options:{precision:2}}" width="150" >Total Anggaran</th>-->
                            <!--						<th data-options="field:'listprice',align:'right'" width="150">Total Anggaran</th>-->
                            <!--						<th data-options="field:'unitcost',align:'right'" width="150">Paid</th>-->
                            <!--						<th data-options="field:'attr1',align:'right'" width="150">Settlement</th>-->
                            <!--						<th data-options="field:'attr1',align:'right'" width="150">CA</th>-->
                            <!--						<th data-options="field:'status',align:'center'" width="150">Balance</th>-->
                        </tr>
                        </thead>
                    </table>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div data-options="region:'south',split:true,border:false" style="height:50px;">-->
            <tr>
            <td>Payment Method</td><td>: <input name="cano" id="cano" class="easyui-textbox" style="width:250px;" data-options="prompt:'Payment Method.'" readonly></td>
            </tr>
            <tr>
            <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmca()" style="width:90px">Confirm</a>
            <a id="printca" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printca()" style="width:90px">Print</a>
            <a href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="clearca()" style="width:90px">Clear</a></td></tr>
<!--        </div>-->
<!--    </div>-->
</form>

<div id="dlgpayment" class="easyui-dialog" style="width:100%;height:100%;"  maximizable="true" closed="true"  buttons="#dlgpayment-buttons">
    <table id="calist" style="width:100%;height:450px;" fit="true"
           rownumbers="true" showFooter="true" fitColumns="true" toolbar="#toolbartree"
           idField="id" Field="ca" sortName="ca" singleSelect="false" sortOrder="asc" >
        <thead >
        <tr>
            <th data-options="field:'cost_name'" width="150">Cash Advance No</th>
            <th data-options="field:'cost_name'" width="150">Total Anggaran</th>
            <th data-options="field:'unitcost',align:'right'" width="150">Paid</th>
            <th data-options="field:'attr1',align:'right'" width="150">Settlement</th>
            <th data-options="field:'attr1',align:'right'" width="150">CA</th>
            <th data-options="field:'status',align:'center'" width="150">Balance</th>
        </tr>

        </thead>
    </table>

</div>
<div id="dlgpayment-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgpayment').dialog('close')" style="width:90px">Close</a>
</div>

<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#printca').linkbutton('disable');
    });
</script>
<script>

    $('#detkeg').datagrid({
        url:'',
        emptyMsg: 'No Records Found',
    });
    function modal(){
        $('#dlgpayment').dialog('open').dialog('setTitle','List Cash Advance ' );
        $('#calist').datagrid({
            url:'',
        });
    }
    $('#refno').combobox({
        url: 'modul/cashadvance/cashadvanceaction.php?act=list_refno',
        valueField: 'id',
        textField: 'refno',
        onSelect: function(rec){
            $('#name_tbp').textbox('setValue',rec.name_tbp);
            $('#nk').textbox('setValue',rec.nama_kegiatan);
//			$('#time').textbox('setValue',rec.wktu);
//			$('#dcost').textbox('setValue',rec.desc_cost);
//			$('#jenis').combobox('setValue',rec.jenis);
//			$('#total').numberbox('setValue','17000000');
//			$('#rev').numberbox('setValue','10000000');
            $('#detkeg').datagrid({
                url:'modul/cashadvance/cashadvanceaction.php?act=detkeg&id='+rec.id,
                pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
//				onDblClickRow:function(index,row){
//					//editproker(index);
//					detrincian(index,row);
//				},
//				view: myview,
                emptyMsg: 'No Records Found',
            });
            $('#detkeg').datagrid('enableCellEditing').datagrid('gotoCell', {
                index: 0,
                field: 'id'
            });
        }

    });

    function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.refno + '</span><br/>' +
            '<span style="color:#888"> Nama Kegiatan : ' + row.nama_kegiatan + '</span><br/>';
        return s;
    }

    function confirmca(){
        //to get the loaded data
        //alert(url);
        $.messager.confirm('Confirm','Are you sure you want to Confirm this ?',function(r){
            if (r){
//				$('#fmxx').form('submit',{
//					url: url, // ini dari mana ya?
//					onSubmit: function(){
//						return $('#fmxx').form('validate');
//					},
//					success: function(result){
//						var result = eval('('+result+')');
//						if (result.errorMsg){
//							$.messager.show({
//								title: 'Error',
//								msg: result.errorMsg
//							});
//						} else {
//							$('#dlgxx').dialog('close');		// close the dialog
//							$('#tree').treegrid('reload');	// reload the user data
//							$('#proker').datagrid('reload');	// reload the user data
//
//
//							$.messager.show({
//								title: 'Success',
//								msg: result.success
//							});
//						}
//					}
//				});
                $('#cano').textbox('setValue','123');
                $('#printca').linkbutton('enable');
            }
        });
    }
    function printca(){
        alert('a');
    }

    function clearca(){
        $('#detkeg').datagrid({
            url:'',
            emptyMsg: 'No Records Found',
        });
        $('#refno').combobox('setValue','');
        $('#name_tbp').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#cano').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
</script>
