<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 12/18/16
 * Time: 3:47 PM
 */
?>

<form id="fmpayment" method="post" novalidate>
        <div class="easyui-layout" style="width:auto;height:500px;">
            <div data-options="region:'north'" style="height:150px;width: 100%;border:0">
                <div data-options="region:'west',split:true" title="West" style="width:100%;">
                    <table cellpadding="5" style="float:left;">
                        <tr>
                            <td>Badan Pelayanan</td> <td>:
                                <input required="true" id="bp" style="width:200px;" data-options="prompt:'Badan Pelayanan.'" class="easyui-combobox" name="name_tbp"/></td>
                        </tr>
                        <tr>
                            <td>Cash / Transfer</td> <td>: <select  name="care" id="care" style="width:200px;"  data-options="prompt:'Cash / Transfer.'" required="true">
                                    <option></option>
                                    <option value="1">Cash</option>
                                    <option value="2">Transfer</option>
                                </select></td>
                        </tr>
                        <tr class="hide">
                            <td>Bank Pengirim</td><td>: <input  name='id_bank'  id="bankid" style="width:200px;" data-options="prompt:'Bank.'" name="bank_name"/></td>
                        </tr>

                       
                    </table>
                    <table cellpadding="5" style="float:right;margin-right: 90px;">
                        <tr>
                            <td>Date</td> <td>: <input name="datenow" id="datenow" class="easyui-textbox" style="width:250px;" data-options="prompt:'Date.'" readonly></td>
                        </tr>
                        <tr>
                            <td>Payment No</td> <td>: <input name="payment_no" id="payment_no" class="easyui-textbox" style="width:250px;" data-options="prompt:'Payment No.'" readonly></td>
                        </tr>
                        <tr class="hide">
                            <td>Voucher No</td> <td>: <input name="no_giro" id="no_giro" class="easyui-textbox" style="width:250px;" data-options="prompt:'No Giro.'" ></td>
                        </tr>
                    </table>
                </div>

            </div>
            <div data-options="region:'south',split:false,border:false" style="height:50px;">
                <table cellpadding="5" style="float:left;">
                    <tr>
                        <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmpayment()" style="width:90px">Confirm</a></td>
                    </tr>
                </table>
                
            </div>
        <div data-options="region:'center',border:false,plain:false" style="height:450px;" >
<!--            <div class="easyui-tabs" data-options="border:false,plain:true" style="height:200px;" >-->
<!--                <div title="Rincian" style="padding:5px;" >-->
                <table id="detkeg"
                       data-options="singleSelect:false,fit:true,fitColumns:true"
                       showFooter="true" idField="id_ca_master" sortName="ca_no" pagination="true"
                       rownumbers="true" pageSize="50"
                       style="width:700px;height:250px">
                    <thead>
                    <tr>
                        <th field="ck" checkbox="true"></th>
                        <th data-options="field:'id_ca_master'" hidden="true" hwidth="80">ID</th>
                        <th data-options="field:'ca_no'" width="150"> No</th>
<!--                        <th data-options="field:'bp'" width="150">Bidang Pelayanan</th>-->
                        <th data-options="field:'rek_no'" width="150">No Rek</th>
                        <th data-options="field:'status_paid',formatter:payment_method" width="150">Payment Method</th>
                        <th data-options="field:'bank'" width="150">Bank</th>
<!--                        <th data-options="field:'bd'" width="150">Badan Pelayanan</th>-->
<!--                        <th data-options="field:'sub_total',align:'right',formatter:formatPrice" width="130"> Anggaran</th>-->
                        <th data-options="field:'paid',align:'right',formatter:formatPrice" width="130">Paid</th>
                        <th data-options="field:'settlement',align:'right',formatter:formatPrice" width="130">Settlement</th>
                        <th data-options="field:'ts',align:'right',formatter:formatPrice" width="130">Total Settlement</th>
                        <th data-options="field:'ca_req',align:'right',formatter:formatPrice" width="130">CA</th>
                        <th data-options="field:'balance',align:'right',formatter:formatPrice" width="150">Balance</th>
                            </tr>
                    </thead>
                </table>
<!--                    </div>-->
<!--                </div>-->
            </div>

        </div>
</form>



<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#printca').linkbutton('disable');
        $('.bankid').hide();
        $('.hide').hide();
        $('.trf').hide();
    });
</script>
<script>
    $('#bankid').combobox({});
    $('#bp').combobox({
        url:'control/view.php?act=tbp',
        valueField:'id_tbp',
        textField:'name_tbp',
        onSelect : function(rectbp){
            $('#care').combobox({
                valueField: 'id',
                onSelect: function (rec) {
                    $('.hide').hide();
                    if(rec.id == 2){
                    $('.hide').show();
                    $('#bankid').combobox({
                        url:'modul/bank/bankaction.php?act=listbank',
                        valueField:'idbank',
                        textField:'bank_name',
                        onSelect: function () {
                                $('#detkeg').edatagrid({
                                url: 'modul/payment/paymentaction.php?act=getlist&status='+rec.id+'&id_tbp='+rectbp.id_tbp,
                                pageList: [10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500],
                                emptyMsg: 'No Records Found'
                            });
                        }
                    });
                    }
                    else {
                        $('#detkeg').edatagrid({
                            url: 'modul/payment/paymentaction.php?act=getlist&status='+rec.id+'&id_tbp='+rectbp.id_tbp,
                            pageList: [10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500],
                            emptyMsg: 'No Records Found'
                        });
                    }
                }

            });
        }
    });

//    $('#detkeg').edatagrid({
//        url: 'modul/payment/paymentaction.php?act=getlist&id_tbp='+rec.id_tbp,
//        pageList: [10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500],
//        emptyMsg: 'No Records Found'
//    });

    
    $('#detkeg').datagrid({
    });

    

    function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.refno + '</span><br/>' +
            '<span style="color:#888"> Nama Kegiatan : ' + row.nama_kegiatan + '</span><br/>';
        return s;
    }

    function confirmpayment(){
     
//        $.messager.confirm('Confirm','Are you sure you want to Confirm this ?',function(r){
//            if (r){
//                var dateObj = new Date();
//                var month = dateObj.getUTCMonth() + 1; //months from 1-12
//                var day = dateObj.getUTCDate();
//                var year = dateObj.getUTCFullYear();
//
//                newdate =  day + "-" + month + "-" + year;
//                $('#datenow').textbox('setValue',newdate);
//                $('#payment_no').textbox('setValue','P-1701000001');
//                $('#printca').linkbutton('enable');
//            }
//        });

        var rows = $('#detkeg').datagrid('getSelections');
        if (rows) {
            $.messager.confirm('Confirm', 'Are you sure you want to move this user?', function (r) {
                if (r) {

                    var ss = $.map(rows, function (item, idx) {
                        return item.id_ca_master;
                    });
                    var giro = $('#no_giro').textbox('getValue');
                    var care = $('#care').combobox('getValue');
                    var idbank = $('#bankid').combobox('getValue');
                    if(care == 1){
                        idbank = 0;
                    }

                    /*$.messager.alert('Info', ss.join('<br/>')); for testing*/
                    $.post('modul/payment/paymentaction.php?act=insert_payment', {
                        'id[]': ss,
                        'no_giro': giro,
                        'id_bank': idbank
                    }, function (result) {

                        if (result.success) {
                            $('#detkeg').datagrid('reload'); // reload the user data
                            $.messager.show({ // show error message
                                title: 'Success',
                                msg: result.success
                            });
                            $('#datenow').textbox('setValue',result.date); 
                            $('#payment_no').textbox('setValue',result.pa_no);
                            location.reload();
                        } else {
                            $.messager.show({ // show error message
                                title: 'Error',
                                msg: result.msg
                            });
                        }
                    }, 'json').fail(function (jxhr, status, error) {
                        $.messager.show({ // show error message
                            title: 'Error',
                            msg: 'Please Select First'
                        });
                    });
                }
            });
        }

    }
    function printca(){
        alert('a');
    }

    function clearca(){
        $('#detkeg').datagrid({
            url:'',
        });
        $('#refno').combobox('setValue','');
        $('#name_tbp').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#payment_no').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
</script>
