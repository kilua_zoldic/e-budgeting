<table id="cost_code" fit="true" border="false" fit="true" border="false" style="width:100%;height:100%;" pageSize="50"
toolbar="#toolbarcost_code" pagination="true"  maximizable="true"  fitColumns="true"
rownumbers="true" singleSelect="true"  idField="id_cost_code_code" showFooter="true" sortName="cost_code_number" sortOrder="asc"
>
<thead>
<tr>
<th data-options="field:'cost_code_number',width:150,sortable:true">Cost Code Number</th>
<th data-options="field:'cost_code_name',width:150,sortable:true">Cost Code Name</th>
<th field="create_by" width="100" sortable="true" >Create By</th>
<th field="create_date" width="100" sortable="true" >Create Time</th>
<th field="update_by" width="100" sortable="true" >Update By</th>
<th field="update_date" width="100" sortable="true" >Update Date</th>
</tr>
</thead>
</table>
<div id="toolbarcost_code">
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newcost_code()">New Cost</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deletecost()">Remove Cost</a>
<!--<table cellpadding="5">
	<tr>
	<td>Unit Name</td><td>:
		<input class="easyui-textbox" style="width:160px"  id="cost_name" data-options="prompt:'Unit Name.'"></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" style="width:90px" onclick="search_ct()">Search</a></td>
	</tr>
</table>-->	
</div>

<div id="dlgcost_code" class="easyui-dialog" style="width:50%;height:300px;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbarcost_code" closed="true"  buttons="#dlg-buttonscost_code">
	<div id="dlg-toolbarcost_code" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
		<form id="fm" method="post" novalidate>
		<table cellpadding="5">
		<tr>
		<td>Cost Code Number </td>
		<td>: <input name="cost_code_number" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Cost Code Number.'" required="true"></td>
		</tr>
		<tr>
		<td>Cost Code Name  </td>
		<td>: <input name="cost_code_name" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Cost Code Name.'" required="true"></td>
		</tr>
		
		</table>
		
		</form>
</div>
<div id="dlg-buttonscost_code">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savecost()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>
<script>
var url;
var dg = cost_code;

	$('#cost_code').datagrid({
		url: 'modul/costcode/costcodeaction.php?act=list',
	onDblClickRow:function(index,row){
				editcostcode(index);
			},
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
                }	
	});
			

$('#cost_code').datagrid('enableFilter',[{
				field:'status_costcode',
				type:'combobox',
				options:{
					panelHeight:'auto',
					data:[{value:'',text:'All'},{value:'1',text:'Active'},{value:'0',text:'InActive'}],
					onChange:function(value){
						$('#cost_code').datagrid('doFilter');
					}
				}
			}]);			
function newcost_code(){
	$('#dlgcost_code').dialog('open').dialog('setTitle','New Unit Cost');
	$('#fm').form('clear');
	$('.db').textbox('enable');
	$('.dbc').combobox('enable');
	$('#edt').linkbutton('disable');
	$('.sv').linkbutton('enable');
	
	url = 'modul/costcode/costcodeaction.php?act=create';
}

function editcostcode(){
	var row = $('#cost_code').datagrid('getSelected');
	if(row)
	{
	$('#dlgcost_code').dialog('open').dialog('setTitle','View / '+row.cost_code_name);
	$('#fm').form('load',row);
	$('#edt').linkbutton('enable');
	url = 'modul/costcode/costcodeaction.php?act=update&id='+row.id_cost_code;
	$('.db').textbox('disable');
	$('.dbc').combobox('disable');	
	$('.sv').linkbutton('disable');		
	$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
	}		
}

function savecost(){
	//to get the loaded data
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
		$('#fm').form('submit',{
			url: url,
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.errorMsg){
					$.messager.show({
						title: 'Error',
						msg: result.errorMsg
					});
				} else {
					$.messager.show({
						title: 'Success',
						msg: result.success
					});
					$('#dlgcost_code').dialog('close');		// close the dialog
					$('#cost_code').datagrid('reload');	// reload the user data
				}
			}
		});}
	});
}

function deletecost(){
var row = $('#cost_code').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/costcode/costcodeaction.php?act=delete',{id:row.id_cost_code},function(result){
if (result.success){
$('#cost_code').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function search_ct(){
$('#cost_code').datagrid('load',{
	cost_name: $('#cost_name').val()
	});
}
</script>