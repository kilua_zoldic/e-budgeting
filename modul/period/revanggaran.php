    <table id="period" style="width:100%;height:100%;" fit="true" border="false" fitColumns="true"
			rownumbers="true" pageSize="50" showFooter="true"  pagination="true" singleSelect="true"
            idField="id" Field="ref_no" toolbar="#toolbar">
        <thead>
            <tr>
				<th field="id" hidden="true" width="100" >ID</th>
				<th field="ref_no" width="100" sortable="true">Ref No</th>
				<th field="nama_kegiatan" width="100" sortable="true" >Nama Kegiatan</th>
				<th field="costtype" width="100" sortable="true" >Cost Name</th>
				<th field="total" width="100" data-options="formatter:formatPrice" sortable="true" >Total</th>
				<th field="status" width="100" sortable="true" formatter="status" >Status</th>
				<th field="create_by" width="100" sortable="true" >Create By</th>
				<th field="create_date" width="100" sortable="true" >Create Time</th>
				<th field="update_by" width="100" sortable="true" >Update By</th>
				<th field="update_date" width="100" sortable="true" >Update Date</th>
             
            </tr>
        </thead>
    </table>
	<div id="toolbar">
		<table>
			<tr>
				<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newperiod()" >New </a></td>
				<td><a href="javascript:void(0)" class="easyui-linkbutton " iconCls="icon-delete" plain="true" onclick="" >Remove </a></td>
				<!--<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="removeperiod()" >Delete </a></td>-->
			</tr>
		</table>
	</div>
<!--toolbar="#dlg-toolbar"-->
<div id="dlg" class="easyui-dialog" style="width:50%;height:350px;padding:10px 20px"  closed="true"  buttons="#dlg-buttons">
		<form id="fm" method="post" novalidate>
<!--		<span>Are you sure you want to Save this ?</span>-->

				<table cellpadding="5">
					<tr>
						<td>Ref No</td> <td>: <input id="refno" required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'Ref No.'" name="refno"/></td>
					</tr>
					<tr>
						<td>Cost Type</td> <td>: <input id="costtype" required="true" style="width:250px;" data-options="formatter: formatcost,prompt:'Cost Type.'" name="cost_name"/></td>
					</tr>
					<tr>
						<td>Status</td>
						<td>:  <select  name="status" style="width:200px;" class="easyui-combobox" data-options="prompt:'Status.'" required="true">
								<option></option>
								<option value="0">Penambahan</option>
								<option value="1">Pengurangan</option>
							</select>
						</td>
					</tr>
					<tr><td>Total Biaya </td><td>: <input class="easyui-numberbox" style="width:250px;"  id="total" data-options="prompt:'Total Biaya.',precision:2,groupSeparator:',',decimalSeparator:'.',prefix:'Rp '"></td></tr>

				</table>

		
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="saveperiod()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>
<div id="dlgedit" class="easyui-dialog" style="width:60%;height:350px;padding:10px 20px"   closed="true"  buttons="#dlgedit-buttons">
		<form id="fmedit" method="post" novalidate>
		<table cellpadding="5">
		<tr>
			<td>Ref No</td> <td>: <input id="refno" class="easyui-combobox"  required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'Ref No.'" name="ref_no"/></td>
		</tr>
		<tr>
			<td>Cost Type</td> <td>: <input id="costtype" required="true" style="width:250px;" data-options="formatter: formatcost,prompt:'Cost Type.'" name="cost_name"/></td>
		</tr>
		<tr>
		<td>Status</td>
		<td>:  <select  name="status" style="width:200px;" class="easyui-combobox" data-options="prompt:'Status.'" required="true">
		<option></option>
		<option value="0">Penambahan</option>
		<option value="1">Pengurangan</option>
		</select>
		</td>
		</tr>
		<tr><td>Total Biaya </td><td>: <input class="easyui-numberbox" style="width:250px;" name="total"  id="total" data-options="prompt:'Total Biaya.',precision:2,groupSeparator:',',decimalSeparator:'.',prefix:'Rp '"></td></tr>
		
		</table>
		
		</form>
</div>
<div id="dlgedit-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton " iconCls="icon-ok" onclick="updateperiod()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgedit').dialog('close')" style="width:90px">Close</a>
</div>
<script>
var dg = period;
var url;
$(function(){
$('.cv').linkbutton('<?php echo $company->GetYear('status_period');?>');
});

function status(val,row){
	if (val == '0' ){
		return '<span>Penambahan</span>';
		}
	else {
		return '<span>Pengurangan</span>';
		}

}
function formatItem(row){
	var s = '<span style="font-weight:bold">' + row.refno + '</span><br/>' +
		'<span style="color:#888"> Nama Kegiatan : ' + row.nama_kegiatan + '</span><br/>';
	return s;
}
function formatcost(row){
	var s = '<span style="font-weight:bold">' + row.cost_name + '</span><br/>' +
		'<span style="color:#888"> Total : ' + row.cost_price + '</span><br/>';
	return s;
}
$('#period').datagrid({
	url:'modul/period/listrev.json',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				editperiod(index,row);
			},
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
    },
	onBeforeLoad:function(param){

	}
	});

function newperiod(){
	$('#dlg').dialog('open').dialog('setTitle','Confirm');

	$('#refno').combobox({
		url: 'modul/cashadvance/cashadvanceaction.php?act=list_refno',
		valueField: 'id',
		textField: 'refno',
		onSelect: function(rec){
				$('#costtype').combobox({
					url: 'modul/period/periodaction.php?act=costtype&parentId='+rec.id,
					valueField: 'id_cost',
					textField: 'cost_name',
				});
		}
	});

	url = '';
}

function editperiod(){
	var row = $('#period').datagrid('getSelected');
		$('#dlgedit').dialog('open').dialog('setTitle','View');

	$('#refno').combobox({
		url: 'modul/cashadvance/cashadvanceaction.php?act=list_refno',
		valueField: 'id',
		textField: 'ref_no',
		onSelect: function(rec){
			$('#costtype').combobox({
				url: 'modul/period/periodaction.php?act=costtype&parentId='+rec.id,
				valueField: 'id_cost',
				textField: 'cost_name',
			});
		}
	});
		$('#fmedit').form('load',row);

}
function editfield(){
	$('.dbc').combobox('enable');
	$('.sv').linkbutton('enable');
}
function saveperiod(){
			//to get the loaded data
			//$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
		//	if (r){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlg').dialog('close');		// close the dialog
						$('#period').datagrid('reload');	// reload the user data
						location.reload('modul/period/period.php');
						location.reload('../backup/tbprokerbackup/tbproker.php');
					}
				}
			});
			//}
			//});
}
function updateperiod(){
			//to get the loaded data
		$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
			$('#fmedit').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlgedit').dialog('close');		// close the dialog
						$('#period').datagrid('reload');	// reload the user data
						location.reload('modul/period/period.php');
						location.reload('../backup/tbprokerbackup/tbproker.php');
					}
				}
			});
		}
	});
}
function removeperiod(){
var row = $('#period').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/period/periodaction.php?act=delete',{id:row.id_period},function(result){
if (result.success){
$('#period').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function generate(){
			//to get the loaded data
			$.messager.confirm('Confirm','Are you sure you want to Generate this ?',function(r){
			if (r){
			$.messager.progress({title:'Processing'});
			$.post('modul/period/periodaction.php?act=generate',{id:'1'},function(result){
			if (result.success){
			$.messager.show({ // show error message
			title: 'Success',
			msg: result.success,
		    showType:'fade',
                style:{
                    right:'',
                    bottom:''
                }
			});
			$('#period').datagrid('reload');
			$.messager.progress('close');
			} else {
			$.messager.show({ // show error message
			title: 'Error',
			msg: result.errorMsg
			});
			}
			},'json');
			}
			});
}

function clear_generate(){
			//to get the loaded data
			$.messager.confirm('Confirm','Are you sure you want to Clear Generate ?',function(r){
			if (r){
			$.messager.progress({title:'Processing'});
			$.post('modul/period/periodaction.php?act=clear_generate',{id:'1'},function(result){
			if (result.success){
			$.messager.show({ // show error message
			title: 'Success',
			msg: result.success,
		    showType:'fade',
                style:{
                    right:'',
                    bottom:''
                }
			});
			$('#period').datagrid('reload');
			$.messager.progress('close');
			} else {
			$.messager.show({ // show error message
			title: 'Error',
			msg: result.errorMsg
			});
			}
			},'json');
			}
			});
}
</script>