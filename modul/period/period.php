    <table id="period" style="width:100%;height:100%;" fit="true" border="false" fitColumns="true"
			rownumbers="true" pageSize="50" showFooter="true"  pagination="true" singleSelect="true"
            idField="id_period" periodField="from" toolbar="#toolbar">
        <thead>
            <tr>
                <th field="id_period" hidden="true" width="100" >ID</th>
				<th data-options="field:'x',width:80,sortable:true,formatter:generatebutton">Generate</th>
                <th field="periodenya" width="100" sortable="true">Period</th>
				<th field="status_period" width="100" sortable="true" formatter="status_period">Status</th>
                <th field="create_by" width="100" sortable="true" >Create By</th>
                <th field="create_date" width="100" sortable="true" >Create Time</th>
                <th field="update_by" width="100" sortable="true" >Update By</th>
                <th field="update_date" width="100" sortable="true" >Update Date</th>
             
            </tr>
        </thead>
    </table>
<div id="toolbar">
	<table>
		<tr>
		<td><a href="javascript:void(0)" class="easyui-linkbutton cv" iconCls="icon-add" plain="true" onclick="newperiod()" >New </a></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton " iconCls="icon-del" plain="true" onclick="clear_generate()" >Clear </a></td>
		<!--<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="removeperiod()" >Delete </a></td>-->
		</tr>
	</table>
</div>
 <div id="p" class="easyui-progressbar" style="width:400px;"></div>
<!--toolbar="#dlg-toolbar"-->
<div id="dlg" class="easyui-dialog" style="width:30%;height:150px;padding:10px 20px"  closed="true"  buttons="#dlg-buttons">
	<!--<div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>-->
		<form id="fm" method="post" novalidate>
		<span>Are you sure you want to Save this ?</span>
		<!--<table cellpadding="5">
		<tr>
		<td>From </td>
		<td>: <input name="from_year" style="width:200px;" class="easyui-combobox dbc" id="from" data-options="prompt:'From.'" required="true"></td>
		</tr>
		
		<tr>
		<td>To</td>
		<td>:  <input name="to_year" style="width:200px;" class="easyui-textbox" id="to" data-options="prompt:'To.'" disabled></td>
		</tr>
		
		<tr>
		<td>Status</td>
		<td>:  <select  name="status_period" style="width:200px;" class="easyui-combobox dbc" data-options="prompt:'Status.'" required="true">
		<option></option>
		<option value="0">Closed</option>
		<option value="1">Open</option>
		</select>
		</td>
		</tr>
		
		</table>-->
		
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="saveperiod()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>
<div id="dlgedit" class="easyui-dialog" style="width:30%;height:150px;padding:10px 20px"   closed="true"  buttons="#dlgedit-buttons">
		<form id="fmedit" method="post" novalidate>
		<tr>
		<td>Status</td>
		<td>:  <select  name="status_period" style="width:200px;" class="easyui-combobox" data-options="prompt:'Status.'" required="true">
		<option></option>
		<option value="0">Closed</option>
		<option value="1">Open</option>
		</select>
		</td>
		</tr>
		
		</table>
		
		</form>
</div>
<div id="dlgedit-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton " iconCls="icon-ok" onclick="updateperiod()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgedit').dialog('close')" style="width:90px">Close</a>
</div>
<script>
var dg = period;
var url;
$(function(){
$('.cv').linkbutton('<?php echo $company->GetYear('status_period');?>');
});
function generatebutton(val,row,index){
	if(row.status_period == 1){
		return '<button onclick="generate()" style="width:80px;height:25px;">Generate</button>';
	
	}
	else {
		return '<button style="width:80px;height:25px;" disabled>Generate</button>';
	}
}

function status_period(val,row){
	if (val == '1' ){
		return '<span>Open</span>';
		} 
	else {
		return '<span>Closed</span>';
		}
	
}

$('#period').datagrid({
	url:'modul/period/periodaction.php?act=list',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				editperiod(index);
			},
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
    },
	onBeforeLoad:function(param){

	}
	});
	

function newperiod(){
	$('#dlg').dialog('open').dialog('setTitle','Confirm');
	$('#fm').form('clear');
	/*$('#from').combobox({
		url:'control/view.php?act=year',
		valueField:'year',
		textField:'year',
		onSelect: function(rec){
		var ho = parseInt(rec.year);
		var t = ho+parseInt(1);
		$('#to').textbox('setValue',t);
		}
	});
	$('.dbc').combobox('enable');
	$('#edt').linkbutton('disable');
	$('.sv').linkbutton('enable');*/
	
	url = 'modul/period/periodaction.php?act=create';
}

function editperiod(){
		var row = $('#period').datagrid('getSelected');
	if(row)
		{
		/*	$('#from').combobox({
		url:'control/view.php?act=year',
		valueField:'year',
		textField:'year',
		onSelect: function(rec){
		var ho = parseInt(rec.year);
		var t = ho+parseInt(1);
		$('#to').textbox('setValue',t);
		}
	});	*/
		$('#dlgedit').dialog('open').dialog('setTitle','View / '+row.from_year+ ' / ' +row.to_year);
		$('#fmedit').form('load',row);
		//$('#edt').linkbutton('enable');
		url = 'modul/period/periodaction.php?act=update&id='+row.id_period;
		//$('.dbc').combobox('disable');
		//$('.sv').linkbutton('disable');		
		//$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
		}		
}
function editfield(){
	$('.dbc').combobox('enable');
	$('.sv').linkbutton('enable');
}
function saveperiod(){
			//to get the loaded data
			//$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
		//	if (r){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlg').dialog('close');		// close the dialog
						$('#period').datagrid('reload');	// reload the user data
						location.reload('modul/period/period.php'); 
						location.reload('../backup/tbprokerbackup/tbproker.php'); 
					}
				}
			});
			//}
			//});
}
function updateperiod(){
			//to get the loaded data
		$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
			$('#fmedit').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlgedit').dialog('close');		// close the dialog
						$('#period').datagrid('reload');	// reload the user data
						location.reload('modul/period/period.php'); 
						location.reload('../backup/tbprokerbackup/tbproker.php'); 
					}
				}
			});
		}
	});
}
function removeperiod(){
var row = $('#period').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/period/periodaction.php?act=delete',{id:row.id_period},function(result){
if (result.success){
$('#period').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function generate(){
			//to get the loaded data
			$.messager.confirm('Confirm','Are you sure you want to Generate this ?',function(r){
			if (r){
			$.messager.progress({title:'Processing'}); 	
			$.post('modul/period/periodaction.php?act=generate',{id:'1'},function(result){
			if (result.success){
			$.messager.show({ // show error message
			title: 'Success',
			msg: result.success,
		    showType:'fade',
                style:{
                    right:'',
                    bottom:''
                }	
			});
			$('#period').datagrid('reload');
			$.messager.progress('close');
			} else {
			$.messager.show({ // show error message
			title: 'Error',
			msg: result.errorMsg
			});
			}
			},'json');
			}
			});
}

function clear_generate(){
			//to get the loaded data
			$.messager.confirm('Confirm','Are you sure you want to Clear Generate ?',function(r){
			if (r){
			$.messager.progress({title:'Processing'}); 	
			$.post('modul/period/periodaction.php?act=clear_generate',{id:'1'},function(result){
			if (result.success){
			$.messager.show({ // show error message
			title: 'Success',
			msg: result.success,
		    showType:'fade',
                style:{
                    right:'',
                    bottom:''
                }	
			});
			$('#period').datagrid('reload');
			$.messager.progress('close');
			} else {
			$.messager.show({ // show error message
			title: 'Error',
			msg: result.errorMsg
			});
			}
			},'json');
			}
			});
}
</script>