
<?php

$array = array("modul","modul2");

function chmod_r($dir)
{
	$dp = opendir($dir);
	while($file = readdir($dp))
	{
		if (($file == ".") || ($file == "..")) continue;

		$path = $dir . "/" . $file;

		$is_dir = is_dir($path);
		set_perms($path, $is_dir);
		if($is_dir) chmod_r($path);
	}
	closedir($dp);
}

function set_perms($file, $is_dir)
{

	$perm = substr(sprintf("%o", fileperms($file)), -4);
	$dirPermissions = "777";
	$filePermissions = "0777";

	if(!$is_dir && $perm != $filePermissions)
	{
		chmod($file, octdec($dirPermissions));
		chmod($file, octdec($filePermissions));
	}

	flush();
}
if($_FILES["zip_file"]["name"]) {

	$filename = $_FILES["zip_file"]["name"];
	$source = $_FILES["zip_file"]["tmp_name"];
	$type = $_FILES["zip_file"]["type"];

	$name = explode(".", $filename);
	$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
	foreach($accepted_types as $mime_type) {
		if($mime_type == $type) {
			$okay = true;
			break;
		}
	}

	$continue = strtolower($name[1]) == 'zip' ? true : false;
	if(!$continue) {
		$message = "The file you are trying to upload is not a .zip file. Please try again.";
	}

	$target_path = $filename;  // change this to the correct site path
	if(move_uploaded_file($source,$target_path)) {
		$zip = new ZipArchive();
		$x = $zip->open($target_path);
		if ($x === true) {
			
			foreach($array as $key=> $value){
			$zip->extractTo($value."/"); 	// change this to the correct site path
//			chmod($value."/".$name[0], 0777);
			chmod_r(dirname($value."/".$name[0]));
			}

//			unlink($target_path);
		}
		$message = "Your .zip file was uploaded and unpacked.";
	} else {
		$message = "There was a problem with the upload. Please try again.";
	}
}
?>


<div class="easyui-panel" fit="true" border="false" style="width:100%;height:100%;padding:10px;">
	<form enctype="multipart/form-data" method="post" action="">

		<table cellpadding="5">
			<tr>
				<td>
					Upload Module (.zip)</td><td>: <input class="f1 easyui-filebox" name="zip_file" />
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" class="easyui-linkbutton c6" name="submit" value="Upload" style="width:65px;height:25px;" /></td></tr>
		</table>
	</form>
	<?php if($message) echo "<h1><p>$message</p></h1>"; ?>
</div>


<!--
<form enctype="multipart/form-data" method="post" action="">
<table cellpadding="5">
<input class="f1 easyui-filebox" name="zip_file" /></label>
<br />
<input type="submit" name="submit" value="Upload" />
</form>-->
