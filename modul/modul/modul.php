<table id="dg" class="easyui-datagrid" style="width:100%;height:100%" url="modul/modul/modulaction.php?act=list"
toolbar="#toolbar" pagination="true" border="false" pagesize="50" fit="true"
rownumbers="true" fitColumns="true" singleSelect="true"  fit="true" idField="id_modul" sortName="id_modul" sortOrder="asc">
<thead>
<tr>

<th data-options="field:'id_modul',width:80,sortable:true,hidden:true">ID Dept</th>
<th data-options="field:'nama_modul',width:80,sortable:true">Module Name</th>
<th data-options="field:'nama_folder_modul',width:100,sortable:true">Folder Name</th>
<th data-options="field:'status_modul',width:100,sortable:true">Status</th>
</tr>
</thead>
</table>
<div id="toolbar">
	<table>
		<tr>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newmodul()" >New</a></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editmodul()" >Edit</a></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroymodul()" >Remove</a></td>
		</tr>
	</table>
</div>
<div id="dlg" class="easyui-dialog" style="width:470px;height:280px;padding:10px 20px" closed="true" buttons="#dlg-buttons">

		<form id="fm" method="post" novalidate enctype="multipart/form-data">
		<table cellpadding="5">
		<tr>
		<td>Module Name</td>
		<td>: <input name="nama_modul" style="width:200px;" class="easyui-textbox" required="true"></td>
		</tr>
		<tr>
		<td>Link Modul</td>
		<td>: <input name="nama_folder_modul" class="easyui-textbox"  style="width:200px;" required="true"></td>
		</tr>
		<tr>
		<td>Status</td><td>: 
				<select class="easyui-combobox" name='status_modul' >							
						<option value='0' selected>InActive</option>
						<option value='1'>Active</option>
				</select>
		</td>
		</tr>
		</table>
	
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savemodul()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>
 <script type="text/javascript">

var url;
function newmodul(){
$('#dlg').dialog('open').dialog('setTitle','New Modul');
$('#fm').form('clear');
url = 'modul/modul/modulaction.php?act=create';
}
function editmodul(){
var row = $('#dg').datagrid('getSelected');
if (row){
$('#dlg').dialog('open').dialog('setTitle','Edit Modul');
$('#fm').form('load',row);
url = 'modul/modul/modulaction.php?act=update&id='+row.id_modul;
}
}
function status_modul(status_modul,row,index){
	if(row.status_modul == '1'){
		return '<span> Active</span>';
	}
	else {
	 return '<span> InActive</span>';
	}
}
 function savemodul(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
}

function destroymodul(){
var row = $('#dg').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this Modul?',function(r){
if (r){
$.post('modul/modul/modulaction.php?act=delete',{id:row.id_modul},function(result){
if (result.success){
$('#dg').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}
</script>
 <style type="text/css">
#fm{
margin:0;
padding:10px 30px;
}
.ftitle{
font-size:14px;
font-weight:bold;
padding:5px 0;
margin-bottom:10px;
border-bottom:1px solid #ccc;
}
.fitem{
margin-bottom:5px;
}
.fitem label{
display:inline-block;
width:80px;
}
.fitem input{
width:160px;
}
</style>