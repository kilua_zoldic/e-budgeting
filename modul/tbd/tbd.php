    <table id="tbd" style="width:100%;height:100%;" fit="true" border="false" fitColumns="true"
			rownumbers="true"  showFooter="true"  singleSelect="true"
            idField="id" treeField="name_tbd" toolbar="#toolbar">
        <thead>
            <tr>
                <th field="id" hidden="true" width="100" >ID</th>
                <th field="name_tbd" width="100" sortable="true">Name</th>
                <th field="desc_tbd" width="100" sortable="true" >Description</th>
                <!--<th field="create_by" width="100" sortable="true" >Create By</th>
                <th field="create_date" width="100" sortable="true" >Create Time</th>
                <th field="update_by" width="100" sortable="true" >Update By</th>
                <th field="update_date" width="100" sortable="true" >Update Date</th>-->
                <th field="status_tbd" width="100" sortable="true" formatter="status_tbd">Status</th>
             
            </tr>
        </thead>
    </table>
<div id="toolbar">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newtbd()" >New </a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removetbd()" >Remove </a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="expandAllgrid()" style="width:80px;">Expand All</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="collapseAllgrid()" style="width:90px;">Collapse All</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="refresh()" style="width:90px;">Refresh Table</a>
	
</div>
<!--toolbar="#dlg-toolbar"-->
<div id="dlg" class="easyui-dialog" style="width:100%;height:100%;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
	<div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
		<form id="fm" method="post" novalidate>
		<table cellpadding="5">
		<tr>
		<td>Name </td>
		<td>: <input name="name_tbd" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Name.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Description</td>
		<td>:  <input name="desc_tbd" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Description.'" required="true"></td>
		</tr>
		
		<tr>
		<td>Status</td>
		<td>:  <select  name="status_tbd" style="width:200px;" class="easyui-combobox dbc" data-options="prompt:'Status.'" required="true">
		<option></option>
		<option value="0">InActive</option>
		<option value="1">Active</option>
		</select>
		</td>
		</tr>
		</table>
		
		<br/>
		<fieldset class="scheduler-border">
		<legend class="scheduler-border">Bidang Pelayanan</legend>
		<div class="control-group">  
		<table id="childtbd" title=" " style="width:98%;height:250px" toolbar="#toolbarchildtbd" fitColumns="true" rownumbers="true"
            data-options="singleSelect:true,collapsible:true" idField="id" showFooter="true"  pagination="true" >
        <thead>
            <tr>
               
				<th data-options="field:'name_tbp',width:150">Name</th>
				<th data-options="field:'desc_tbp',width:150">Description</th>
            </tr>
        </thead>
		</table>
		<div id="toolbarchildtbd">
		<table>
			<tr>
			<td><a href="javascript:void(0)" class="easyui-linkbutton newchild" iconCls="icon-add" plain="true" onclick="newchild()" >New </a></td>
			<!--<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="edittbd()" >Edit </a></td>-->
			<td><a href="javascript:void(0)" class="easyui-linkbutton delchild" iconCls="icon-remove" plain="true" onclick="removechild()" >Remove </a></td>
			</tr>
		</table>
	</div>	 
	</div>	 
	</fieldset>
	</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savetbd()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>

<div id="dlgchild" class="easyui-dialog" style="width:50%;height:60%;padding:10px 15px"  maximizable="true" toolbar="#dlgchild-toolbar" closed="true"  buttons="#dlgchild">
		<div id="dlgchild-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
				   <a href="javascript:void(0)" class="easyui-linkbutton" id="edtchild" data-options="iconCls:'icon-edit',plain:true" onclick="editfieldchild()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
	<form id="fmchild" method="post" novalidate>
	<table cellpadding="5">
	<tr>
	<td>Nama Bidang</td><td>: <input id="tbp" name="name_tbp" style="width:200px;"  data-options="prompt:'Nama Bidang.'" required="true" ></td>
	</tr>
	<tr>
	<td>Description</td><td>: <input id="desc_tbp" class="easyui-textbox"  name="desc_tbp" style="width:300px;height:100px;"  data-options="multiline:true,prompt:'Description.'" disabled ></td>
	</tr>
	</table>
	</form>
</div>
<div id="dlgchild">
<a href="javascript:void(0)" class="easyui-linkbutton svchild" iconCls="icon-ok" onclick="savechild()" style="width:90px">Save</a>                   
				
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgchild').dialog('close')" style="width:90px">Close</a>
</div>	
<script>
var url;
var urlchild;
var dg = tbd;


function expandAllgrid(){
	$('#tbd').treegrid('expandAll');
			
}
function collapseAllgrid(){
	$('#tbd').treegrid('collapseAll');
			
}

function refresh(){
	$('#tbd').treegrid('reload');
			
}



$('#tbd').treegrid({
	url:'modul/tbd/tbdaction.php?act=list',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				edittbd(index);
			},
	onLoadSuccess: function(row,data){
            $('#tbd').treegrid('collapseAll');
			
        },		
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
                }		
	});
$('#tbd').treegrid('enableFilter',[{
				field:'status_tbd',
				type:'combobox',
				options:{
					panelHeight:'auto',
					data:[{value:'',text:'All'},{value:'1',text:'Active'},{value:'0',text:'InActive'}],
					onChange:function(value){
						if (value == ''){
							$('#tbd').treegrid('removeFilterRule', 'status_tbd');
						} else {
							$('#tbd').treegrid('addFilterRule', {
								field: 'status_tbd',
								op: 'equal',
								value: value
							});
						}
						$('#tbd').treegrid('doFilter');
					}
				}
			}]);

function newtbd(){
	$('#tree').treegrid('clearSelections');
	$('#dlg').dialog('open').dialog('setTitle','New');

	$('#fm').form('clear');
	$('.db').textbox('enable');
	$('.dbc').combobox('enable');
	$('#edt').linkbutton('disable');
	$('.sv').linkbutton('enable');
	$('.newchild').linkbutton('enable');		
	$('.delchild').linkbutton('enable');	
	url = 'modul/tbd/tbdaction.php?act=create&random=<?php echo $_SESSION['random'];?>';
	$('#childtbd').datagrid({
	url:'modul/tbd/tbdaction.php?act=listnewchild&random=<?php echo $_SESSION['random'];?>',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				//editproker(index);
				editchild(index);
			},
	
	});
}

function edittbd(){
	var row = $('#tbd').treegrid('getSelected');
		if(row.parentId == 0)
		{
		$('#dlg').dialog('open').dialog('setTitle','View / '+row.name_tbd+ ' / ' +row.id);
		$('#fm').form('load',row);
		$('#edt').linkbutton('enable');
		$('.db').textbox('disable');
		$('.dbc').combobox('disable');	
		$('.sv').linkbutton('disable');		
		//$('.newchild').linkbutton('disable');		
		//$('.delchild').linkbutton('disable');		
		$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
		$('#childtbd').datagrid({
		url:'modul/tbd/tbdaction.php?act=listchildtbd&id_tbd='+row.id,
		pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
		onDblClickRow:function(index,row){
					//editproker(index);
					editchild(index);
				},
		
		});
		
		url = 'modul/tbd/tbdaction.php?act=update&id='+row.id;
	}		
}

function savetbd(){
	//to get the loaded data
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
		$('#fm').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.errorMsg){
				$.messager.show({
				title: 'Error',
				msg: result.errorMsg
			});
			} else {
				$.messager.show({
				title: 'Success',
				msg: result.success
			});
			$('#dlg').dialog('close');		// close the dialog
			$('#tbd').treegrid('reload');	// reload the user data
		}
		}
	});}
	});
}



function removetbd(){
var row = $('#tbd').treegrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/tbd/tbdaction.php?act=delete',{id:row.id},function(result){
if (result.success){
$('#tbd').treegrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function editfieldchild(){
	
	$('.dbchild').combobox('enable');
	$('#tbp').combobox('enable');
	$('.svchild').linkbutton('enable');
}

function newchild(){
	var row = $('#tbd').treegrid('getSelected');
	$('#dlgchild').dialog('open').dialog('setTitle','New');
	$('#tbp').combobox({
	url:'control/view.php?act=tbp',
	valueField:'id_tbp',
	textField:'name_tbp',
	onSelect: function(rec){
            $('#desc_tbp').textbox('setValue',rec.desc_tbp);
        }
	});
	$('#fmchild').form('clear');
	$('.dbchild').combobox('enable');
	$('#tbp').combobox('enable');
	$('#edtchild').linkbutton('disable');
	$('.svchild').linkbutton('enable');	
	if(row){
	urlchild = 'modul/tbd/tbdaction.php?act=createchild&id='+row.id+'&random=0';
	}
	else {
	urlchild = 'modul/tbd/tbdaction.php?act=createchild&random=<?php echo $_SESSION['random'];?>';
	}
}

function editchild(){
	var row = $('#childtbd').treegrid('getSelected');
		if(row){
		$('#tbp').combobox({
		url:'control/view.php?act=tbp',
		valueField:'id_tbp',
		textField:'name_tbp',
		onSelect: function(rec){
			$('#desc_tbp').textbox('setValue',rec.desc_tbp);
			}
		});	
		$('#dlgchild').dialog('open').dialog('setTitle','View / '+row.name_tbp+ ' / ' +row.desc_tbp);
		$('#fmchild').form('load',row);
		$('#edtchild').linkbutton('enable');
		//$('.db').textbox('disable');
		$('.dbchild').combobox('disable');	
		$('#tbp').combobox('disable');	
		$('.svchild').linkbutton('disable');		
		$('.newchild').linkbutton('disable');		
		$('.delchild').linkbutton('disable');		
		$('#edtchild').linkbutton('<?php echo $company->privilege('edit');?>');
		
		urlchild = 'modul/tbd/tbdaction.php?act=updatechild&id='+row.id;
	}		
}

function savechild(){
			//to get the loaded data
			$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
			$('#fmchild').form('submit',{
				url: urlchild,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlgchild').dialog('close');		// close the dialog
						$('#childtbd').datagrid('reload');	// reload the user data
						$('#tbd').treegrid('reload');	// reload the user data
					}
				}
			});}
			});
}

function removechild(){
var row = $('#childtbd').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/tbd/tbdaction.php?act=deletechild',{id:row.id},function(result){
if (result.success){
$('#childtbd').datagrid('reload'); // reload the user data
$('#tbd').treegrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function search_tbd(){
$('#tbd').treegrid('load',{
	name_tbd: $('#name_tbd').val()
	});
}
</script>