 <?php
//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

session_start();
include '../../config/koneksi.php';

require_once('../../assets/tcpdf/tcpdf.php');
$datenow = date('d-M-Y');
 $typepm = 'Cash';
if($_REQUEST['typepm'] == 2 ){
    $typepm = 'Transfer';
}
// extend TCPF with custom functions

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('eBudgeting');
$pdf->SetAuthor('eBudgeting');
$pdf->SetTitle('Cash Advance Verification');
$pdf->SetSubject('Cash Advance Verification');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20, PDF_MARGIN_TOP, 15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
// set font


// add a page

$pdf->AddPage('L', 'A4');

$pdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));

$pdf->SetFont('times', '', 20);
$pdf->Cell(0, 0, 'Cash Advance Verification', 0, 1, 'C');
$pdf->Ln(5);

$pdf->SetFont('times', '', 10);

$pdf->Cell(10, 1, 'No Ref :'.$_REQUEST['refnotext'], 0, 1, 'L');
$pdf->Cell(10, 1, 'Description : '.$_REQUEST['nk'], 0, 1, 'L');
$pdf->Cell(10, 1, 'Bidang Pelayanan : '.$_REQUEST['bp'], 0, 1, 'L');
$pdf->Cell(10.2, 1, 'Badan Pelayanan : '.$_REQUEST['bd'], 0, 1, 'L');
$pdf->Cell(263, -30, 'Date :  '.$datenow, 0, 1, 'R', 0, '', 0, true, 'T', 'M');
$pdf->Cell(263, 38, 'Req No : '.$_REQUEST['cano'], 0, 1, 'R', 0, '', 0, true, 'T', 'M');
 //$pdf->Cell(180, 18, 'Cash Advance      :  '.$_REQUEST['cano'], 0, 1, 'R', 0, '', 0, true, 'T', 'M');
//$pdf->Cell(170, -11, 'Bidang Pelayanan :  '.$_REQUEST['bp'], 0, 1, 'R', 0, '', 0, true, 'T', 'M');
//$pdf->Cell(173.2, 18, 'Badan Pelayanan :  '.$_REQUEST['bd'], 0, 1, 'R', 0, '', 0, true, 'T', 'M');

//$pdf->Cell(0, -5, 'Badan Pelayanan : '.$_REQUEST['bd'], 0, 1, '', 0, 'R');
// $pdf->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
//$pdf->Cell(0, 1, 'ANGGARAN PROGRAM KERJA', 0, 1, 'C');
//$pdf->Cell(0, 2, 'PERIODE: ', 0, 1, 'C');
//$pdf->Cell(0, 1, 'Tanggal Print: '.$datenow, 0, 3, 'C');
$pdf->Ln(10);
// column titles
$pdf->SetFont('helvetica',  19);

//$pdf->Cell(0, 0, 'Marketing Expense Claim', 0, 1, 'C');
//$pdf->Ln(1);



$html = '<style> 
	table{ 
    width: 550px; 
    border-collapse: collapse; 
    margin-top:55px;
    }
	tr:nth-of-type(odd) { 
    background: #eee; 
    }
	th { 
    background-color: #FFDD33; 
    color: black; 
    font-weight: bold;
    font-size: 10px;
    }
	td{ 
    padding: 5px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 10px;
    }
    .tablepolos > .tdpolos{ 
    padding: 5px; 
    border: 1px solid #fff; 
    text-align: left; 
    font-size: 10px;
    }
	</style>';


$html .='<h4>Rincian</h4>';
$html .= '<table >
		<tr>
		<th  width="25">No</th>
		<th width="190">Rincian Anggaran</th>
		<th width="85" align="center">Anggaran </th>
		<th width="80"  align="center">CA Request </th>
		<th width="80"  align="center">Paid </th>
		<th width="80"  align="center">Settlement </th>
		<th width="100"  align="center">YTD Settlement </th>
		<th width="80"  align="center">Balance </th>
		<th width="200"  align="center">Remarks </th>
		</tr>
		<tbody>';
 $i = 1;
 $query = mysql_query("select * from ca_
							   where ca_no = '$_REQUEST[cano]' order by id_doc asc");
 while($row=mysql_fetch_array($query)) {
     $html .= '
    <tr><td align="right">'.$i.'</td>
        <td >'.$row['cost_name'].'</td>
        <td align="right">'.number_format($row['total']).'</td>
        <td align="right">'.number_format($row['ca_req']).'</td>
        <td align="right">'.number_format($row['paid']).'</td>
        <td align="right">'.number_format($row['settlement']).'</td>
        <td align="right">'.number_format($row['total_settlement']).'</td>
        <td align="right">'.number_format($row['balance']).'</td>
        <td align="right">'.$row['remarks'].'</td>
    </tr>';
     $total +=$row['total'];
     $ca_req +=$row['ca_req'];
     $paid +=$row['paid'];
     $settle +=$row['settlement'];
     $ts +=$row['total_settlement'];
     $balance +=$row['balance'];
     $i++;
 }
$html	.='</tbody>
    <tfoot>
        <tr>
          <td colspan="2">Total</td>
          <td align="right" align="right"><b>'.number_format($total).'</b></td>
          <td align="right"><b>'.number_format($ca_req).'</b></td>
          <td align="right"><b>'.number_format($paid).'</b></td>
          <td align="right"><b>'.number_format($settle).'</b></td>
          <td align="right"><b>'.number_format($ts).'</b></td>
          <td align="right"><b>'.number_format($balance).'</b></td>
          <td align="right"><b></b></td>
         </tr>
    </tfoot>
    </table>';
 $html .='<br/><br/>
<table class="tablepolos">
<tr >
    <td class="tdpolos" style="width:100px;">Paid To</td> <td class="tdpolos" >: '.$typepm.'</td>
</tr>';
if($_REQUEST['typepm'] == 2) {
    $html .= '<tr >
    <td class="tdpolos"  style="width:100px;">Rek No</td> <td class="tdpolos" >: '.$_REQUEST['rek_no'].'</td>
</tr>
<tr >
    <td class="tdpolos"  style="width:100px;">Bank</td> <td class="tdpolos" >: BCA</td>
</tr>';
}
$html.='
</table>';

$html2 ='<table border="1" width="200px" style="font-size:11px;" align="left" >';
$html2 .='
        <tr>
            <td>Cash Advance No </td><td> </td>
        </tr>
        <tr>
            <td>Payment No </td><td> </td>
        </tr>
        <tr>
            <td>Settlement No </td><td> </td>
        </tr>
</table>';

$y3 = $pdf->SetX();
$pdf->Ln(10);
$pdf->SetFillColor(255, 255, 255);
$pdf->writeHTMLCell('0', '20', '225', '55', $html2, '180', '1', '1', 'C', '', true);




 $html1 ='<table border="1"  align="center" >';
 $html1 .='<tr>
		<th  width="780px;">Approval</th>
		<th width="150px;">Receive</th>
		
		</tr><tbody>';
 $html1 .='<tr nobr="true">
		<td width="150px;"><br /><br /><br />(Bendahara)</td>
		<td width="200px;"><br /><br /><br />(Penatua Pendamping) </td>
		<td width="180px;"><br /><br /><br />(Ketua Bidang) </td>
		<td width="250px;"><br /><br /><br />(Internal Audit) </td>
		<td width="150px;"><br /><br /><br /></td>
		</tr>';
 $html1 .='</tbody></table>';

$pdf->writeHTML($html);
$y2 = $pdf->SetX();
//$dq = $pdf->writeHTML($html);
//$pdf->Ln(65);
$pdf->SetFillColor(255, 255, 255);

 $pdf->writeHTMLCell('0', '0', '19', $y2, $html1, '180', '1', '1', true, 'C', true);



// ---------------------------------------------------------
ob_clean();
// close and output PDF document
$pdf->Output('Detail Report.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
