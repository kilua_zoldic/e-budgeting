<form id="fmcareq" method="post">
<div class="easyui-layout" style="width:100%;height:704px;">
	<div data-options="region:'north'" style="height:152px;width: 100%;border:0">
		<div data-options="region:'west',split:true" title="West" style="width:100%;">
			<table cellpadding="5" style="float:left;">
				<tr >
					<td >Badan Pelayanan</td>
					<td>: <input name="name_tbd"  style="width:200px;" id="bd" class="easyui-combobox" data-options="prompt:'Badan Pelayanan.'" required="true"/></td>
				</tr>
				<tr >
					<td id="bp_hide">Bidang Pelayanan</td>
					<td>: <input name="name_tbp" style="width:200px;" id="bp" class="easyui-combobox" data-options="prompt:'Bidang Pelayanan.'" required="true"/></td>
				</tr>
				<tr>
					<td>Transaction Number</td> <td>: <input id="transnum" name="id_ca_master_temp" required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'Transaction Number.'"/></td>
				</tr>
<!--				<tr>-->
<!--					<td>Description</td> <td>: <input name="nama_kegiatan" id="nk" class="easyui-textbox" style="width:250px;" data-options="prompt:'Nama Kegiatan.'" disabled></td>-->
<!--				</tr>-->
			</table>
		</div>
		<div data-options="region:'east',split:true" title="West" style="width:100%;background-color: red;">
		<table cellpadding="5" style="float:right;">
			<tr>
				<td>Date</td> <td>: <input name="datenow" id="datenow" class="easyui-textbox" style="width:250px;" data-options="prompt:'Date.'" readonly></td>
			</tr>
			<tr>
				<td>BP</td> <td>: <input name="name_tbp" id="name_tbp" class="easyui-textbox" style="width:250px;" data-options="prompt:'Bidang Pelayanan.'" readonly></td>
			</tr>
			<tr>
				<td>BD</td> <td>: <input name="name_tbd" id="name_tbd" class="easyui-textbox" style="width:250px;" data-options="prompt:'Bidang Pelayanan.'" readonly></td>
			</tr>
			<tr>
				<td>
					Cash Advance No</td> <td>: <input name="cano" id="cano" class="easyui-textbox" style="width:250px;" data-options="prompt:'Cash Advance Number.'" readonly></td>
			</tr>
		</table>
		</div>
	</div>

	<div data-options="region:'center',border:false,plain:false" style="height:302px;" >
		<div class="easyui-tabs" data-options="border:false,plain:true" style="height:300px;" >
			<div title="Rincian" style="padding:5px;" >
				<table id="detkeg"
					   data-options="singleSelect:true,fit:true,fitColumns:true"
					   showFooter="true" idField="id_ca_temp" sortName="cost_name" pagination="true"
					   rownumbers="true" pageSize="50">
					<thead>
					<tr>
						<th data-options="field:'id_ca_temp'" hidden="true" width="80">ID</th>
						<th data-options="field:'cost_name',sortable:true" width="150">Rincian Anggaran</th>
						<th data-options="field:'total',formatter:formatPrice" width="150" >Anggaran</th>
						<th data-options="field:'ca_req',formatter:formatPrice,align:'right'" width="150">CA Request</th>
						<th data-options="field:'paid',formatter:formatPrice,align:'right'" width="150">Paid</th>
						<th data-options="field:'settle',formatter:formatPrice,align:'right'" width="150">Settlement</th>
						<th data-options="field:'balance',formatter:formatPrice,align:'center'" width="150">Balance</th>
						<th data-options="field:'cost_code_number',align:'center',editor:{type:'combobox',
										options:{
										valueField:'id_cost_code',
										textField:'cost_code_number',
										url:'modul/cashadvance_verify/cashadvance_verifyaction.php?act=cost_code',
										required:true
									}}" width="150">Cost Code No</th>
						<th data-options="field:'cost_code_name',align:'center'" width="150">Cost Code Name</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div data-options="region:'south',split:false,border:false" style="height:250px;">
		<table cellpadding="5" style="float:left;">
			<tr>
				<td>Paid To</td><td>: <select name="typepm" style="width:200px;" id="typepm" class="easyui-combobox" data-options="prompt:'Type.'" required="true" readonly>
						<option ></option>
						<option value="1">Cash</option>
						<option value="2">Transfer</option></select></td>
			</tr>
			<tr class="trf">
				<td>Rek No</td><td>: <input  id="rek_no"class="easyui-textbox" style="width:200px;" data-options="prompt:'Rek No.'" name="rek_no" readonly/></td>
			</tr>

			<tr class="trf">
				<td>Bank </td><td>: <input id="bankid" style="width:200px;" data-options="prompt:'Bank.'" name="bank_name" readonly/></td>
			</tr>
			<tr class="trf">
				<td>A / n</td><td>: <input id="an" class="easyui-textbox" style="width:200px;" data-options="prompt:'A/n.'" name="an" readonly/></td>
			</tr>
<!--			<tr class="trf">-->
<!--				<td>Voucher No</td><td>: <input required="true" id="voucno" class="easyui-textbox" style="width:200px;" data-options="prompt:'Voucher No.'" name="voucher_no"/></td>-->
<!--			</tr>-->
		<tr>
		<td></td><td>
		<a href="javascript:void(0)" id="getcode" class="easyui-linkbutton" onclick="getCode()"  style="width:90px">Get Code</a>
		<a href="javascript:void(0)" id="confirm" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmcaapp()" style="width:90px">Confirm</a>
		<a id="printcash" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printcash()" style="width:90px">Print</a>
		<a id="printtrf" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printrf()" style="width:90px">Print</a>
		<a href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="clearca()" style="width:90px">Clear</a>
		</td>
		</tr>
		</table>
	</div>
</div>
</form>
<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#printcash').linkbutton('disable');
		$('#printtrf').linkbutton('disable');
//		$('#confirm').hide();
		$('#getcode').hide();
		$('#printcash').hide();
		$('#printtrf').hide();
		$('.hide').hide();
		$('.trf').hide();
	});
</script>
<script>
	$('#typepm').combobox({
		valueField: 'typepm',
		onSelect: function (rec) {
			$('.hide').hide();
			$('.trf').hide();
			if (rec.typepm == 1) {
				$('.hide').show();
				$('.trf').hide();

			}
			else if(rec.typepm == 2) {
				$('.trf').show();
				$('.hide').show();
				$('#bankid').combobox({
					url:'modul/bank/bankaction.php?act=listbank',
					valueField:'idbank',
					textField:'bank_name'
				});
				
			}
		}
	});
	$('#detkeg').datagrid({
		url:'',
		emptyMsg: 'No Records Found',
	});
	$('#bd').combobox({
		url:'modul/cashadvance_verify/cashadvance_verifyaction.php?act=tbdca',
		valueField:'id_tbd',
		textField:'name_tbd',
		onSelect: function(rec){
			$('#bp').combobox({
				url:'modul/cashadvance_verify/cashadvance_verifyaction.php?act=tbpca&id='+rec.id_tbd,
				valueField:'id_tbp',
				textField:'name_tbp',
				onSelect:function(reca){
					$('#transnum').combobox({
						url: 'modul/cashadvance_verify/cashadvance_verifyaction.php?act=listcamaster&id='+reca.id_tbp,
						valueField: 'id_ca_master_temp',
						textField: 'req_no',
						onSelect: function(rec){
							
//							var today = new Date();

							$('#name_tbp').textbox('setValue',rec.name_tbp);
							$('#name_tbd').textbox('setValue',rec.name_tbd);
							$('#cano').textbox('setValue',rec.ca_no);
//							$('#nk').textbox('setValue',rec.desc);
							$('#datenow').textbox('setValue',rec.create_date);
							$('#detkeg').edatagrid({
								url:'modul/cashadvance_verify/cashadvance_verifyaction.php?act=detcamaster&id='+rec.req_no,
								pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
								emptyMsg: 'No Records Found',
								updateUrl: 'modul/cashadvance_verify/cashadvance_verifyaction.php?act=update',
								onSuccess:function(index,row){
									$.messager.show({
										title:'Info',
										msg:row.success
									});
									$('#detkeg').edatagrid('reload');
								},
								onError: function(index,row){
									$.messager.show({
										title:'Info',
										msg: row.errorMsg
									});
								}
							}).edatagrid('enableCellEditing').edatagrid('gotoCell', {
								index: 0,
								field: 'id_ca_temp'
							});
							$('#typepm').combobox('setValue',rec.status_paid);
							var typ = $('#typepm').combobox('getValue');
							if (typ == 1) {
								$('.hide').show();
								$('.trf').hide();
							}
							else if(typ == 2) {
								$('.trf').show();
								$('.hide').show();
								$('#bankid').combobox({
									url:'modul/bank/bankaction.php?act=listbank',
									valueField:'idbank',
									textField:'bank_name'
								});
								$('#bankid').combobox('setValue',rec.id_bank);
								$('#rek_no').textbox('setValue',rec.rek_no);
								$('#an').textbox('setValue',rec.an);

							}
						}
					});
				}
			});
		}
	});


	function formatItem(row){
		var s = '<span style="font-weight:bold">' + row.req_no + '</span><br/>' +
			'<span style="color:#888"> Ref No : ' + row.refno + '</span><br/>';
		return s;
	}



	function clearca(){
		location.reload();
	}
	function printcash(){
		url = "modul/cashadvance_verifiy/cashadvance_verifiy_report.php?bp="+$('#name_tbp').textbox('getValue')+"&bd="+$('#name_tbd').textbox('getValue')+"&nk="+$('#nk').textbox('getValue')+
													"&transnum="+$('#transnum').textbox('getValue')+"&refno="+$('#refno').combobox('getValue')+"&typepm="+$('#typepm').combobox('getValue');
		window.open(url);
	}
	function printrf(){
		url = "modul/cashadvance/cashadvance_verifiy_report.php?bp="+$('#name_tbp').textbox('getValue')+"&bd="+$('#name_tbd').textbox('getValue')+"&nk="+$('#nk').textbox('getValue')+
													"&transnum="+$('#transnum').textbox('getValue')+"&refno="+$('#refno').combobox('getValue')+"&typepm="+$('#typepm').combobox('getValue')+
													"&bankid="+$('#bankid').combobox('getValue')+"&rek_no="+$('#rek_no').textbox('getValue')+"&an="+$('#an').textbox('getValue');
		window.open(url);
	}

	function getCode(){
			$.messager.confirm('Confirm', 'Are you sure you want to Confirm this ?', function (r) {
				if (r) {

					$('#fmcareq').form('submit',{
						url : 'modul/cashadvance_verify/cashadvance_verifyaction.php?act=getCode',
						onSubmit: function(){
							return $('#fmcareq').form('validate');
						},
						success: function(result){


							var result = eval('('+result+')');
							$('#cano').textbox('setValue', result.cano);
							$('#datenow').textbox('setValue', result.create_date);
							if (result.errorMsg){

								$.messager.show({
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								//$('#dlgxx').dialog('close');		// close the dialog
								$('#getcode').hide();
								$('#confirm').show();
								$.messager.show({
									title: 'Success',
									msg: result.success
								});
							}
						}
					});

				}

			});
	}

	function confirmcaapp(){
		var cekpaidto = $('#typepm').combobox('getValue');
		if(cekpaidto == '1' || cekpaidto == '2'){
		$.messager.confirm('Confirm', 'Are you sure you want to Confirm this ?', function (r) {
			if(cekpaidto == '1') {
				$('#printcash').show();
				$('#printtrf').hide();
				$('#printcash').linkbutton('enable');
			}
			else {
				$('#printtrf').show();
				$('#printcash').hide();
				$('#printtrf').linkbutton('enable');
			}
			$('#fmcareq').form('submit',{
				url : 'modul/cashadvance_verify/cashadvance_verifyaction.php?act=confirmcaapp',
				onSubmit: function(){
					return $('#fmcareq').form('validate');
				},
				success: function(result){

					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						//$('#dlgxx').dialog('close');		// close the dialog
						$('#confirm').hide();
						$('#confirm').linkbutton('disable');
						$('#cano').textbox('setValue', result.cano);
						$('#datenow').textbox('setValue', result.create_date);
//						location.reload();
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
					}
				}
			});

		});
	}
	}
</script>