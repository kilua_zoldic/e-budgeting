 <?php
//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

session_start();
include '../../config/koneksi.php';
require_once('../../assets/tcpdf/tcpdf.php');
require_once('../../control/class.php');
$company = new Report();
$datenow = date('d-M-Y');
$period = $_REQUEST['period'];
$lvl = $_REQUEST['lvl'];
$vt = $_REQUEST['vt'];
$bdd = $_GET['bd'];
$id_tbd = $_REQUEST['bd'];
if($id_tbd == 'all'){
$bd = "";
}
else {
$bd = "a.id_tbd='$id_tbd' and";
}

$bpd = $_REQUEST['bp'];
$id_tbp = $_GET['bp'];
if($id_tbp == 'all'){
$bp ="";
}
else {
$bp = "a.id_tbp='$id_tbp' and";
}
$id = $_REQUEST['period'];
$id_period = $company->GetPeriod();
// extend TCPF with custom functions

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('eBudgeting');
$pdf->SetAuthor('eBudgeting');
$pdf->SetTitle('');
$pdf->SetSubject('');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(15, PDF_MARGIN_TOP, 15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
// set font


// add a page

$pdf->AddPage('L', 'A4');

$pdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));

$pdf->SetFont('times', '', 8);


$html = '<style> 
	table{ 
    width: 850px; 
    border-collapse: collapse; 
    margin-top:55px;
    }
	tr:nth-of-type(odd) { 
    background: #eee; 
    }
	th { 
    background-color: #FFDD33; 
    color: black; 
    font-weight: bold; 
    }
	td{ 
    padding: 5px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 8px;
    }
	</style>';
if($lvl == '1'){
	if($vt == '1'){
		
$pdf->Cell(0, 1, 'Badan Pelayanan : Corporate', 0, 1, 'L');
$pdf->Cell(0, 1, 'ANGGARAN PROGRAM KERJA', 0, 1, 'C');
$pdf->Cell(0, 2, 'PERIODE: '.$company->GetPeriod('from_year',$id).' - '.$company->GetPeriod('to_year',$id), 0, 1, 'C');
$pdf->Cell(0, 1, 'Tanggal Print: '.$datenow, 0, 3, 'C');
$pdf->Ln(4);
// column titles
$pdf->SetFont('helvetica',  5);

//$pdf->Cell(0, 0, 'Marketing Expense Claim', 0, 1, 'C');
//$pdf->Ln(1);


/*quernya SELECT f.name_tbd,sum( b.cost_price * a.total_event * a.total_human ) + ( a.sub_total * a.total_human * a.total_event )  jum FROM document_tree a left join cost_type b on a.id_cost = b.id_cost 
left join tabel_bd f on a.id_tbd = f.id group by a.id_tbd
union 
SELECT f.name_tbd,f.name_tbd is null jum FROM tabel_bd f group by f.id*/

$html .= '<table >
		<tr>
		<th width="50" align="center">No</th>
		<th width="120" align="center">Badan Pelayanan </th>
		<th width="120" align="right">Anggaran Rutin </th>
		<th width="120" align="right">Anggaran Non Rutin</th>
		<th width="120" align="right">Total</th>
		</tr>
		<tbody>';
$i = 1;	
$query =mysql_query("SELECT f.name_tbd,sum(
						CASE WHEN a.jenis != '2'
						AND a.id_cost != '5'
						THEN b.cost_price * a.total_event * a.total_human
						ELSE 0
						END ) rutin, sum(
						CASE WHEN a.jenis != '2'
						AND a.id_cost = '5'
						THEN a.sub_total * a.total_event * a.total_human
						ELSE 0
						END ) rutinother, sum(
						CASE WHEN a.jenis = '2'
						AND a.id_cost != '5'
						THEN b.cost_price * a.total_event * a.total_human
						ELSE 0
						END ) nonrutin, sum(
						CASE WHEN a.jenis = '2'
						AND a.id_cost = '5'
						THEN a.sub_total * a.total_event * a.total_human
						ELSE 0
						END ) nonrutinother
					FROM document_tree a
					LEFT JOIN cost_type b ON a.id_cost = b.id_cost
					LEFT JOIN tabel_bd f ON a.id_tbd = f.id
					LEFT JOIN tabel_bp g ON f.id_tbp = g.id_tbp
					WHERE a.id_period = '$period'
					GROUP BY f.id
					UNION
SELECT f.name_tbd, f.name_tbd IS NULL rutin, f.name_tbd IS NULL rutinother, f.name_tbd IS NULL nonrutin, f.name_tbd IS NULL nonrutinother
FROM tabel_bd f where f.id not in (select id_tbd from document_tree) and f.name_tbd!=''");
		while($row=mysql_fetch_array($query)){
		$html.='<tr>
		<td align="center">'.$i.'</td>
		<td >'.$row['name_tbd'].'</td>
		<td align="right">'.number_format($row['rutin']+$row['rutinother']).'</td>
		<td align="right">'.number_format($row['nonrutin']+$row['nonrutinother']).'</td>
		<td align="right">'.number_format($row['rutin']+$row['rutinother']+$row['nonrutin']+$row['nonrutinother']).'</td>
		</tr>';
		$sumrutin +=$row['rutin']+$row['rutinother'];
		$sumnonrutin +=$row['nonrutin']+$row['nonrutinother'];
		$total +=$row['rutin']+$row['rutinother']+$row['nonrutin']+$row['nonrutinother'];
		
		$i++;
		}
		$html	.='</tbody>
			<tfoot>
		<tr>
		  <td colspan="2">Total</td>
		  <td align="right"><b>'.number_format($sumrutin).'</b></td>
		  <td align="right"><b>'.number_format($sumnonrutin).'</b></td>
		  <td align="right"><b>'.number_format($total).'</b></td>
		  </tr></tfoot></table>';
	}
	else {
		
	
$pdf->Cell(0, 1, 'Bidang Pelayanan : Corporate', 0, 1, 'L');
$pdf->Cell(0, 1, 'ANGGARAN PROGRAM KERJA', 0, 1, 'C');
$pdf->Cell(0, 2, 'PERIODE: '.$company->GetPeriod('from_year',$id).' - '.$company->GetPeriod('to_year',$id), 0, 1, 'C');
$pdf->Cell(0, 1, 'Tanggal Print: '.$datenow, 0, 3, 'C');
$pdf->Ln(4);
// column titles
$pdf->SetFont('helvetica',  5);

//$pdf->Cell(0, 0, 'Marketing Expense Claim', 0, 1, 'C');
//$pdf->Ln(1);

/*quernya SELECT f.name_tbd,sum( b.cost_price * a.total_event * a.total_human ) + ( a.sub_total * a.total_human * a.total_event )  jum FROM document_tree a left join cost_type b on a.id_cost = b.id_cost 
left join tabel_bd f on a.id_tbd = f.id group by a.id_tbd
union 
SELECT f.name_tbd,f.name_tbd is null jum FROM tabel_bd f group by f.id*/

$html .= '<table >
		<tr>
		<th width="50" align="center">No</th>
		<th width="120" align="center">Bidang Pelayanan </th>
		<th width="120" align="right">Anggaran Rutin</th>
		<th width="120" align="right">Anggaran Non Rutin</th>
		<th width="120" align="right">Total </th>
		</tr>
		<tbody>';
$i = 1;	
$query =mysql_query("SELECT g.name_tbp, sum(
						CASE WHEN a.jenis != '2'
						AND a.id_cost != '5'
						THEN b.cost_price * a.total_event * a.total_human
						ELSE 0
						END ) rutin, sum(
						CASE WHEN a.jenis != '2'
						AND a.id_cost = '5'
						THEN a.sub_total * a.total_event * a.total_human
						ELSE 0
						END ) rutinother, sum(
						CASE WHEN a.jenis = '2'
						AND a.id_cost != '5'
						THEN b.cost_price * a.total_event * a.total_human
						ELSE 0
						END ) nonrutin, sum(
						CASE WHEN a.jenis = '2'
						AND a.id_cost = '5'
						THEN a.sub_total * a.total_event * a.total_human
						ELSE 0
						END ) nonrutinother
					FROM document_tree a
					LEFT JOIN cost_type b ON a.id_cost = b.id_cost
					LEFT JOIN tabel_bd f ON a.id_tbd = f.id
					LEFT JOIN tabel_bp g ON a.id_tbp = g.id_tbp
					WHERE a.id_period='$period'
					GROUP BY g.id_tbp
					UNION
					SELECT g.name_tbp, g.name_tbp IS NULL rutin, g.name_tbp IS NULL rutinother, g.name_tbp IS NULL nonrutin, g.name_tbp IS NULL nonrutinother
					FROM tabel_bp g
					WHERE g.id_tbp not in (select id_tbp from document_tree)");
		while($row=mysql_fetch_array($query)){
		$html.='<tr>
		<td align="center">'.$i.'</td>
		<td >'.$row['name_tbp'].'</td>
		<td align="right">'.number_format($row['rutin']+$row['rutinother']).'</td>
		<td align="right">'.number_format($row['nonrutin']+$row['nonrutinother']).'</td>
		<td align="right">'.number_format($row['rutin']+$row['rutinother']+$row['nonrutin']+$row['nonrutinother']).'</td>
		</tr>';
		$sumrutin +=$row['rutin']+$row['rutinother'];
		$sumnonrutin +=$row['nonrutin']+$row['nonrutinother'];
		$total +=$row['rutin']+$row['rutinother']+$row['nonrutin']+$row['nonrutinother'];
		
		$i++;
		}
		$html	.='</tbody>
			<tfoot>
		<tr>
		  <td colspan="2">Total</td>
		  <td align="right"><b>'.number_format($sumrutin).'</b></td>
		  <td align="right"><b>'.number_format($sumnonrutin).'</b></td>
		  <td align="right"><b>'.number_format($total).'</b></td>
		  </tr></tfoot></table>';
	}
}
/* BADAN PELAYANAN*/
else if ($lvl == '2'){

$pdf->Cell(0, 1, 'Badan dPelayanan : '.$company->GetBadan('name_tbd',$bdd), 0, 1, 'L');
$pdf->Cell(0, 1, 'ANGGARAN PROGRAM KERJA', 0, 1, 'C');
$pdf->Cell(0, 2, 'PERIODE: '.$company->GetPeriod('from_year',$id).' - '.$company->GetPeriod('to_year',$id), 0, 1, 'C');
$pdf->Cell(0, 1, 'Tanggal Print: '.$datenow, 0, 3, 'C');
$pdf->Ln(4);
// column titles
$pdf->SetFont('helvetica',  5);

//$pdf->Cell(0, 0, 'Marketing Expense Claim', 0, 1, 'C');
//$pdf->Ln(1);


/*quernya SELECT f.name_tbd,sum( b.cost_price * a.total_event * a.total_human ) + ( a.sub_total * a.total_human * a.total_event )  jum FROM document_tree a left join cost_type b on a.id_cost = b.id_cost 
left join tabel_bd f on a.id_tbd = f.id group by a.id_tbd
union 
SELECT f.name_tbd,f.name_tbd is null jum FROM tabel_bd f group by f.id*/

$html .= '<table >
		<tr>
		<th width="50" align="center">No</th>
		<th width="120" align="center">Badan Pelayanan </th>
		<th width="120" align="right">Anggaran Rutin</th>
		<th width="120" align="right">Anggaran Non Rutin</th>
		<th width="120" align="right">Total</th>
		</tr>
		<tbody>';
$i = 1;	
$query =mysql_query("SELECT g.name_tbp, sum(
						CASE WHEN a.jenis != '2'
						AND a.id_cost != '5'
						THEN b.cost_price * a.total_event * a.total_human
						ELSE 0
						END ) rutin, sum(
						CASE WHEN a.jenis != '2'
						AND a.id_cost = '5'
						THEN a.sub_total * a.total_event * a.total_human
						ELSE 0
						END ) rutinother, sum(
						CASE WHEN a.jenis = '2'
						AND a.id_cost != '5'
						THEN b.cost_price * a.total_event * a.total_human
						ELSE 0
						END ) nonrutin, sum(
						CASE WHEN a.jenis = '2'
						AND a.id_cost = '5'
						THEN a.sub_total * a.total_event * a.total_human
						ELSE 0
						END ) nonrutinother
						FROM document_tree a
						LEFT JOIN cost_type b ON a.id_cost = b.id_cost
						LEFT JOIN tabel_bd f ON a.id_tbd = f.id
						LEFT JOIN tabel_bp g ON a.id_tbp = g.id_tbp
					WHERE ".$bd." a.id_period = '$period'
					GROUP BY g.id_tbp
");
		while($row=mysql_fetch_array($query)){
		$html.='<tr>
		<td align="center">'.$i.'</td>
		<td >'.$row['name_tbp'].'</td>
		<td align="right">'.number_format($row['rutin']+$row['rutinother']).'</td>
		<td align="right">'.number_format($row['nonrutin']+$row['nonrutinother']).'</td>
		<td align="right">'.number_format($row['rutin']+$row['rutinother']+$row['nonrutin']+$row['nonrutinother']).'</td>
		</tr>';
		$sumrutin +=$row['rutin']+$row['rutinother'];
		$sumnonrutin +=$row['nonrutin']+$row['nonrutinother'];
		$total +=$row['rutin']+$row['rutinother']+$row['nonrutin']+$row['nonrutinother'];
		
		$i++;
		}
		$html	.='</tbody>
			<tfoot>
		<tr>
		  <td colspan="2">Total</td>
		  <td align="right"><b>'.number_format($sumrutin).'</b></td>
		  <td align="right"><b>'.number_format($sumnonrutin).'</b></td>
		  <td align="right"><b>'.number_format($total).'</b></td>
		  </tr></tfoot></table>';
}
else {
	

$pdf->Cell(0, 1, 'Bidang Pelayanan : '.$company->GetBidang('name_tbp',$bpd), 0, 1, 'L');
$pdf->Cell(0, 1, 'ANGGARAN PROGRAM KERJA', 0, 1, 'C');
$pdf->Cell(0, 2, 'PERIODE: '.$company->GetPeriod('from_year',$id).' - '.$company->GetPeriod('to_year',$id), 0, 1, 'C');
$pdf->Cell(0, 1, 'Tanggal Print: '.$datenow, 0, 3, 'C');
$pdf->Ln(4);
// column titles
$pdf->SetFont('helvetica',  5);

//$pdf->Cell(0, 0, 'Marketing Expense Claim', 0, 1, 'C');
//$pdf->Ln(1);

/*quernya SELECT f.name_tbd,sum( b.cost_price * a.total_event * a.total_human ) + ( a.sub_total * a.total_human * a.total_event )  jum FROM document_tree a left join cost_type b on a.id_cost = b.id_cost 
left join tabel_bd f on a.id_tbd = f.id group by a.id_tbd
union 
SELECT f.name_tbd,f.name_tbd is null jum FROM tabel_bd f group by f.id*/

$html .= '<table >
		<tr>
		<th width="50" align="center">No</th>
		<th width="120" align="center">Badan Pelayanan </th>
		<th width="120" align="right">Anggaran Rutin</th>
		<th width="120" align="right">Anggaran Non Rutin</th>
		<th width="120" align="right">Total</th>
		</tr>
		<tbody>';
$i = 1;	
$query =mysql_query("SELECT f.name_tbd, sum(
					CASE WHEN a.jenis != '2' and a.id_cost!='5'
					THEN b.cost_price * a.total_event * a.total_human 
					ELSE 0
					END ) rutin,
					sum(
					CASE WHEN a.jenis != '2' and a.id_cost='5'
					THEN a.sub_total * a.total_event * a.total_human 
					ELSE 0
					END ) rutinother,
					sum(
					CASE WHEN a.jenis = '2' and a.id_cost!='5'
					THEN b.cost_price * a.total_event * a.total_human
					ELSE 0
					END ) nonrutin,
					sum(
					CASE WHEN a.jenis = '2' and a.id_cost='5'
					THEN a.sub_total * a.total_event * a.total_human
					ELSE 0
					END ) nonrutinother
					FROM document_tree a
					LEFT JOIN cost_type b ON a.id_cost = b.id_cost
					LEFT JOIN tabel_bd f ON a.id_tbd = f.id
					LEFT JOIN tabel_bp g ON f.id_tbp = g.id_tbp
					WHERE ".$bp." a.id_period = '$period'
					GROUP BY f.id
UNION
SELECT f.name_tbd, f.name_tbd IS NULL rutin, f.name_tbd IS NULL rutinother, f.name_tbd IS NULL nonrutin, f.name_tbd IS NULL nonrutinother
FROM tabel_bd f where f.id not in (select id_tbd from document_tree) and f.name_tbd!=''");
		while($row=mysql_fetch_array($query)){
		$html.='<tr>
		<td align="center">'.$i.'</td>
		<td >'.$row['name_tbd'].'</td>
		<td align="right">'.number_format($row['rutin']+$row['rutinother']).'</td>
		<td align="right">'.number_format($row['nonrutin']+$row['nonrutinother']).'</td>
		<td align="right">'.number_format($row['rutin']+$row['rutinother']+$row['nonrutin']+$row['nonrutinother']).'</td>
		</tr>';
		$sumrutin +=$row['rutin']+$row['rutinother'];
		$sumnonrutin +=$row['nonrutin']+$row['nonrutinother'];
		$total +=$row['rutin']+$row['rutinother']+$row['nonrutin']+$row['nonrutinother'];
		
		$i++;
		}
		$html	.='</tbody>
			<tfoot>
		<tr>
		  <td colspan="2">Total</td>
		  <td align="right"><b>'.number_format($sumrutin).'</b></td>
		  <td align="right"><b>'.number_format($sumnonrutin).'</b></td>
		  <td align="right"><b>'.number_format($total).'</b></td>
		  </tr></tfoot></table>';
}		 
$pdf->writeHTML($html);
$pdf->SetFont('helvetica', '', 7);
//$dq = $pdf->writeHTML($html);

$pdf->SetFillColor(255, 255, 255);



// ---------------------------------------------------------
ob_clean();
// close and output PDF document
$pdf->Output('Detail Report.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
