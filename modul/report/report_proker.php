<?php
ob_start();
include '../../config/koneksi.php';
$idx = $_GET['id'];
$period = mysql_fetch_array(mysql_query("select * from period where status_period ='1'"));
$bidang = mysql_fetch_array(mysql_query("select * from tabel_bd a left join tabel_bp b on a.id_tbp = b.id_tbp where a.id='$idx'"));
$master_bidang = mysql_fetch_array(mysql_query("select * from tabel_bd where id='$bidang[parentId]'"));


/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

    // get the HTML
 ?>
 <style type="text/css">
 <!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    div.note {border: solid 1mm #DDDDDD;background-color: #EEEEEE; padding: 2mm; border-radius: 2mm; width: 100%; }
    ul.main { width: 95%; list-style-type: square; }
    ul.main li { padding-bottom: 2mm; }
    h1 { text-align: center; font-size: 20mm}
    h3 { text-align: center; font-size: 14mm}
-->

</style>
	<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header >
	<table class="page_header">
            <tr>
                <td style="width: 33%; text-align: left;">
                  <span>Badan Pelayanan : <?php echo $master_bidang['name_tbd'];?></span><br/>
				   <span>Bidang Pelayanan : <?php echo $bidang['name_tbp'];?></span>
                </td>
                <td style="width: 34%; text-align: center">
                  <span>ANGGARAN PROGRAM KERJA</span><br/>
				<span>PERIOD <?php echo $period['from_year'].'-'.$period['to_year'];?></span>
                </td>
                <td style="width: 33%; text-align: right">
                 </td>
            </tr>
        </table>
    </page_header>
	<table class="ok" >
		<tr>
		<th rowspan="2" width="250">Nama Kegiatan & Rincian Anggaran</th>
		<th width="120" colspan="2" align="center">Jumlah </th>
		<th width="150" colspan="2" align="center">Anggaran </th>
		<th width="75" rowspan="2" align="center">Ref No </th>
		<th width="150" rowspan="2" align="center">Waktu </th>
		</tr>
		<tr>
        <th width="60" align="center">Org / Brg</th>
        <th width="60" align="center">Event</th> 
		<th width="75" align="center">Sub</th>
        <th width="75" align="center">Total</th>
		</tr>
		<tbody>
		<?php 
 function display ($parent)
		{
		$id = $_GET['id'];
		//$id_tbp = '7';	
		$warnaGenap = "#FFFFFF";   // warna putih
		$warnaGanjil = "#e6ffe7";  // warna abu abu
			$result="select * from document_tree a left join cost_type b on a.id_cost = b.id_cost where  a.parentId='$parent'  ";
			$tampil = mysql_query($result);
				while ($row=mysql_fetch_array($tampil))
						{	
				$q = mysql_fetch_array(mysql_query("select SUM( b.cost_price * a.total_event * a.total_human ) tot, SUM( a.sub_total * a.total_human * a.total_event ) tot2 
					FROM document_tree a left join cost_type b on a.id_cost = b.id_cost where a.parentId='$row[id]' "));
					$sum = $q["tot"]+$q["tot2"];
							echo "<tr bgcolor='".$warna."'>";
										if($row["nama_kegiatan"] !==""){ 
										echo "<td><b>".$row["nama_kegiatan"]."</b></td>";
										}
										else {
										echo "<td>".$row["cost_name"]."</td>";	
										}
										echo "<td>".$row["total_human"]."</td>";	
										echo "<td>".$row["total_event"]."</td>";
										if($row["id_cost"] == 0){
										echo "<td></td>";
										}
										else if($row["id_cost"] == 5){
										echo "<td>".$row["sub_total"]*$row["total_human"]*$row["total_event"]."</td>";
										}
										else { 
										echo "<td>".$row["cost_price"]*$row["total_human"]*$row["total_event"]."</td>";	
										}
										if($row["nama_kegiatan"] ==""){
										echo "<td></td>";
										echo "<td></td>";
										}
										else {
										echo "<td>".$sum."</td>";
										echo "<td>".$row['refno']."</td>";
										}
										echo "<td>".$row["time"]."</td>";
										
										echo "</tr>";	
										display($row["id"]);
									 
						//$a += $q["tot"];
						}
	}
		echo display(0,0);
			
	?>
	</tbody>
		<tr>
		  <td colspan='4'>Total</td>
		  <td><b>	 </b></td>
		  <td></td>
		  <td></td>
		 </tr>
	</table>
	<page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 33%; text-align: left;">
                  
                </td>
                <td style="width: 34%; text-align: center">
                    page [[page_cu]]/[[page_nb]]
                </td>
                <td style="width: 33%; text-align: right">
                  
                </td>
            </tr>
        </table>
    </page_footer>
    </page>

	<?php
    $content = ob_get_clean();

    // convert to PDF
    require_once(dirname(__FILE__).'/../../assets/html2pdf_v4.03/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('L', 'A4', 'en');
		$html2pdf->pdf->Cell(0, 1, 'Bidang Pelayanan : ', 0, 1, 'L');
		$html2pdf->pdf->Cell(0, 1, 'Bidang :', 0, 1, 'L');
		$html2pdf->pdf->Cell(0, 1, 'ANGGARAN PROGRAM KERJA', 0, 1, 'C');
		$html2pdf->pdf->Cell(0, 2, 'PERIODE: ', 0, 1, 'C');
		$html2pdf->pdf->Cell(0, 1, 'Tanggal Print: ', 0, 3, 'C');
		$html2pdf->pdf->Ln(4);
        $html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($content, false);
        $html2pdf->Output('exemple05.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
