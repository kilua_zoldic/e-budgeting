

<div id="dlg" class="easyui-panel" style="width:100%;height:auto;" border="false" resizable="true"  toolbar="#dlg-toolbar" data-options="footer:'#ft'">
	
		<form id="fm" method="post" novalidate>
		<fieldset class="scheduler-border">
		<legend class="scheduler-border" >Report Program Kerja & Anggaran</legend>
		<div class="control-group">
		<table cellpadding="5">
		<tr>
		<td>Period</td>
		<td>: <input name="period" style="width:200px;" id="period" data-options="prompt:'Period.'" required="true"/></td>
		</tr>
		<tr>
		<td>Report Type</td>
		<td>: <select name="type" style="width:200px;" id="rt" class="easyui-combobox" data-options="prompt:'Type.'" required="true">
				<option ></option>
				<option value="1">Recap</option>
				<option value="2">Detail</option></select>
		</td>
		</tr>
		</table>
		</div>
		</fieldset>
		<br/>
		<br/>
		<br/>
		<table id="rekap" cellpadding="5">

		<tr id="lvl_hide" >
		<td id="lvl_hide">Level</td>
		<td>: <select name="lvl" style="width:200px;" id="lvl" class="easyui-combobox" data-options="prompt:'Level.'" required="true">
				<option></option>
				<option value="1">Corporate</option>
				<option value="2">Badan Pelayanan</option>
				<option value="3">Bidang Pelayanan</option>
				</select></td>
		</tr>
		<tr id="vt_hide">
		<td id="vt_hide">View Type</td>
		<td>: <select name="vt" style="width:200px;" id="vt" class="easyui-combobox" data-options="prompt:'View Type.'" required="true">
				<option></option>
				<option value="1">Badan Pelayanan</option>
				<option value="2">Bidang Pelayanan</option>
				</select></td>
		</tr>
		<tr id="bd_hide">
		<td id="bd_hide">Badan Pelayanan</td>
		<td>: <input name="name_tbd"  style="width:200px;" id="bd" class="easyui-combobox" data-options="prompt:'Badan Pelayanan.'" required="true"/></td>
		</tr>
		
		<tr id="bp_hide">
		<td id="bp_hide">Bidang Pelayanan</td>
		<td>: <input name="name_tbp" style="width:200px;" id="bp" class="easyui-combobox" data-options="prompt:'Bidang Pelayanan.'" required="true"/></td>
		</tr>
		<tr id="bdd_hide">
		<td id="bdd_hide">Badan Pelayanan</td>
		<td>: <input name="name_tbd" style="width:200px;" id="bdd"  data-options="prompt:'Badan Pelayanan.'" required="true"/></td>
		</tr>
		
		<tr id="bpd_hide">
		<td id="bpd_hide">Bidang Pelayanan</td>
		<td>: <input name="name_tbp" style="width:200px;" id="bpd" data-options="prompt:'Bidang Pelayanan.'" required="true"/></td>
		</tr>
		
		</table>
		</form>
</div>
<div id="ft">
<a href="javascript:void(0)" class="easyui-linkbutton sv1" iconCls="icon-pdf" onclick="generate1()" style="width:90px">Generate to PDF</a>
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-pdf" onclick="generate()" style="width:90px">Generate to PDF</a>
<a href="javascript:void(0)" class="easyui-linkbutton svex" iconCls="icon-excel" onclick="generateexcel()" style="width:90px">Generate to Excel</a>
</div>
<script>
$('#period').combobox({
	url:'control/view.php?act=period',
	valueField:'id_period',
	textField:'period',
});
$('.sv').hide();
$('.sv1').hide();
$('.svex').hide();
$('#rekap').hide();
$('#vt_hide').hide();
$('#bd_hide').hide();
$('#bp_hide').hide();
$('#bdd_hide').hide();
$('#bpd_hide').hide();
$('#lvl_hide').hide();
$('#rt').combobox({
	valueField:'type',
	onSelect: function(rec){
		if(rec.type == 1){
			$('#rekap').show();
			$('#bdd_hide').hide();
			$('.sv').hide();
			$('.svex').hide();
			$('.sv1').show();
			$('#bpd_hide').hide();
			$('#lvl_hide').show();
		}
		else if(rec.type == 2){
			$('#rekap').show();
			$('#lvl_hide').hide();
			$('#vt_hide').hide();
			$('#bd_hide').hide();
			$('#bp_hide').hide();
			$('#bdd_hide').show();
			$('.sv').show();
			$('.svex').show();
			$('.sv1').hide();
			$('#bdd').combobox({
				url:'control/view.php?act=tbdreport',
				valueField:'id',
				textField:'name_tbd',
				onSelect: function(rec){
				$('#bpd_hide').show();	
				$('#bpd').combobox({
				url:'control/view.php?act=tbpreport&id='+rec.id,
				valueField:'id_tbp',
				textField:'name_tbp',
				});
			
				}
			});
			
		}
	}
});

$('#lvl').combobox({
	valueField:'lvl',
	onSelect: function(rec){
		if(rec.lvl == 1){
			$('#vt_hide').show();
			$('#bd_hide').hide();
			$('#bp_hide').hide();
		}
		else if(rec.lvl == 2){
			$('#bd_hide').show();
			$('#vt_hide').hide();
			$('#bp_hide').hide();
			$('#bd').combobox({
				url:'control/view.php?act=tbdreport',
				valueField:'id',
				textField:'name_tbd'
			});
		}
		else if(rec.lvl == 3){
			$('#bd_hide').hide();
			$('#vt_hide').hide();
			$('#bp_hide').show();
			$('#bp').combobox({
				url:'control/view.php?act=tbp',
				valueField:'id_tbp',
				textField:'name_tbp',
				});
		}
		
	}
});

function generate(){
	 url = "modul/report/pdf_report.php?period="+$('#period').combobox('getValue')+"&type="+$('#rt').combobox('getValue')+"&bdd="+$('#bdd').combobox('getValue')+"&bpd="+$('#bpd').combobox('getValue');
       window.open(url);    
	}
function generate1(){
	 url = "modul/report/pdf_report_1.php?period="+$('#period').combobox('getValue')+"&type="+$('#rt').combobox('getValue')+"&vt="+$('#vt').combobox('getValue')+"&lvl="+$('#lvl').combobox('getValue')+"&bd="+$('#bd').combobox('getValue')+"&bp="+$('#bp').combobox('getValue');
       window.open(url);    

	}
function generateexcel(){
	  url = "modul/report/ex.php?period="+$('#period').combobox('getValue')+"&type="+$('#rt').combobox('getValue')+"&bdd="+$('#bdd').combobox('getValue')+"&bpd="+$('#bpd').combobox('getValue');
      window.open(url);    

	}
</script>