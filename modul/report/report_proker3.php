<?php
ob_start();
include '../../config/koneksi.php';
$idx = $_GET['id'];
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

    // get the HTML
 ?>

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	    <table class="table table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Firstname</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Anna</td>
      </tr>
      <tr>
        <td>2</td>
        <td>Debbie</td>
      </tr>
      <tr>
        <td>3</td>
        <td>John</td>
      </tr>
    </tbody>
  </table>
	<?php
    $content = ob_get_clean();

    // convert to PDF
    require_once(dirname(__FILE__).'/../../assets/html2pdf_v4.03/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('L', 'A4', 'en');
        $html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->pdf->IncludeJS('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js');
		$html2pdf->pdf->IncludeJS('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js');
		$html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($content, false);
        $html2pdf->Output('exemple05.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
