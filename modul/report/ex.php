<?php
include '../../config/koneksi.php';
include '../../control/class.php';
$company = new Report();
$companyc = new Master();
$datenow = date('d-M-Y');
include 'ax.php';
$file="report_e-budgeting.xls";
$html = '<style> 
	table{ 
    width: 850px; 
    border-collapse: collapse; 
    margin-top:55px;
    }
	tr:nth-of-type(odd) { 
    background: #eee; 
    }
	th { 
    background-color: #FFDD33; 
    color: black; 
    font-weight: bold; 
    }
	td{ 
    padding: 5px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 12px;
    }
	</style>';
								
	
$display = display(0);
$gx	= displayc(0);
$displaynonrutin = displaynonrutin(0);  
$g	= displaycnonrutin(0); 
$html .='<table width="150px;" cellstyle="0">
		<tr><td>Total RUTIN  </td> <td>:'.number_format($gx).' </td></tr>
		<tr><td>Total Non RUTIN </td> <td>: '.number_format($g).'</td></tr>
		<tr><td>Grand Total </td> <td>: '.number_format($g+$gx).'</td></tr>
		</table><br/><br/>';	
$html .='<tr><td>RUTIN </td></tr>';
$html .= '<table >
		<tr>
		<th rowspan="2" width="25">No</th>
		<th rowspan="2" width="250">Nama Kegiatan & Rincian Anggaran</th>
		<th width="120" colspan="2" align="center">Jumlah </th>
		<th width="150" colspan="2" align="center">Anggaran </th>
		<th width="75" rowspan="2" align="center">Ref No </th>
		<th width="150" rowspan="2" align="center">Waktu </th>
		<th width="250" rowspan="2" align="center">Sasaran & Tujuan </th>
		</tr>
		<tr>
        <th width="60" align="center">Org / Brg</th>
        <th width="60" align="center">Event</th> 
		<th width="75" align="right">Sub</th>
        <th width="75" align="right">Total</th>
		</tr>
		<tbody>';
	

			
		$html	.= $display;  
		  	
		$html	.='</tbody>
			<tfoot>
		<tr>
		  <td colspan="5">Total</td>
		  <td align="right"><b>'.number_format($gx).'</b></td>
		  <td></td>
		  <td></td>
		  <td></td>
		 </tr></tfoot></table>';
		 $html.='<br/><br/><br/><br/>';
		 $html .='<tr><td>NON RUTIN</td></tr>';
		$html .= '<table >
		<tr>
		<th rowspan="2" width="25">No</th>
		<th rowspan="2" width="250">Nama Kegiatan & Rincian Anggaran</th>
		<th width="120" colspan="2" align="center">Jumlah </th>
		<th width="150" colspan="2" align="center">Anggaran </th>
		<th width="75" rowspan="2" align="center">Ref No </th>
		<th width="150" rowspan="2" align="center">Waktu </th>
		<th width="250" rowspan="2" align="center">Sasaran & Tujuan </th>
		</tr>
		<tr>
        <th width="60" align="center">Org / Brg</th>
        <th width="60" align="center">Event</th> 
		<th width="75" align="right">Sub</th>
        <th width="75" align="right">Total</th>
		</tr>
		<tbody>';
	

			
		$html	.=  $displaynonrutin;	
		$html	.='</tbody>
			<tfoot>
		<tr>
		  <td colspan="5">Total</td>
		  <td align="right"><b>'.number_format($g).'</b></td>
		  <td></td>
		  <td></td>
		  <td></td>
		 </tr></tfoot></table>';
	
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");	 
	 echo $html;
	 
	 ?>
