 <?php
//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

session_start();
include '../../config/koneksi.php';
require_once('../../assets/tcpdf/tcpdf.php');
require_once('../../control/class.php');
$company = new Report();
$datenow = date('d-M-Y');
$period = $_REQUEST['period'];
$lvl = $_REQUEST['lvl'];
$vt = $_REQUEST['vt'];
$bdd = $_GET['bd'];
$id_tbd = $_REQUEST['bd'];
if($id_tbd == 'all'){
$bd = "";
}
else {
$bd = "a.id_tbd='$id_tbd' and";
}

$bpd = $_REQUEST['bp'];
$id_tbp = $_GET['bp'];
if($id_tbp == 'all'){
$bp ="";
}
else {
$bp = "a.id_tbp='$id_tbp' and";
}
$id = $_REQUEST['period'];
$id_period = $company->GetPeriod();
// extend TCPF with custom functions

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('EMS PARAMITRA');
$pdf->SetAuthor('EMS PARAMITRA');
$pdf->SetTitle('Forcasted Sales Report');
$pdf->SetSubject('Forcasted Sales Report');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, PDF_MARGIN_TOP, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
// set font


// add a page

$pdf->AddPage('L', 'A4');

$pdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));

$pdf->SetFont('times', '', 8);

$pdf->Cell(0, 1, 'Badan Pelayanan : Corporate', 0, 1, 'L');
$pdf->Cell(0, 1, 'ANGGARAN PROGRAM KERJA', 0, 1, 'C');
$pdf->Cell(0, 2, 'PERIODE: '.$company->GetPeriod('from_year',$id).' - '.$company->GetPeriod('to_year',$id), 0, 1, 'C');
$pdf->Cell(0, 1, 'Tanggal Print: '.$datenow, 0, 3, 'C');
$pdf->Ln(4);
// column titles
$pdf->SetFont('helvetica',  5);

//$pdf->Cell(0, 0, 'Marketing Expense Claim', 0, 1, 'C');
//$pdf->Ln(1);



$html = '<style> 
	table{ 
    width: 850px; 
    border-collapse: collapse; 
    margin-top:55px;
    }
	tr:nth-of-type(odd) { 
    background: #eee; 
    }
	th { 
    background-color: #3498db; 
    color: white; 
    font-weight: bold; 
    }
	td{ 
    padding: 5px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 11px;
    }
	
		</style>';

$html .= '<table >
	<tr>
	<th width="40" align="center">No</th>
	<th width="250" align="center">Cost Type</th>
	';
 $all_property = array();
$query = mysql_query("select name_tbp,id_tbp from tabel_bp order by id_tbp asc");
 while($row=mysql_fetch_array($query)){
	 $html.='<th width="100" align="center">'.$row['name_tbp'].'</th>';
	 array_push($all_property, $row['id_tbp']);
		}
 $html .= '</tr><tbody>';
 $i = 1;
 $query = mysql_query("select concat(a.cost_name,'-',b.note)as cost_type,c.name_tbp,c.id_tbp from cost_type a left join document_tree b on a.id_cost = b.id_cost left join tabel_bp c on b.id_tbp = c.id_tbp where b.id_cost != '0' group by a.id_cost desc");

 while($row=mysql_fetch_array($query)){
	 $html .= '<tr>';
	 $html .= '<td>' . $i . '</td>'; //get items using property value
	 $html .= '<td>' . $row['cost_type'] . '</td>'; //get items using property value
//	 foreach ($all_property as $item) {
		 $zxc = mysql_query("select sum( a.cost_price * b.total_event * b.total_human)as total from cost_type a left join document_tree b on a.id_cost = b.id_cost left join tabel_bp c on b.id_tbp = c.id_tbp where c.id_tbp ='$row[id_tbp]' group by a.id_cost desc");
		 $ok = mysql_fetch_array($zxc);
		 $html .= '<td>' .$ok['total'] . '</td>';
//	 }

	 $html .= '</tr>';
	 $i++;
 }

 $html.='</tbody></table>';
 
$pdf->writeHTML($html);
$pdf->SetFont('helvetica', '', 7);
//$dq = $pdf->writeHTML($html);

$pdf->SetFillColor(255, 255, 255);



// ---------------------------------------------------------
ob_clean();
// close and output PDF document
$pdf->Output('Detail Report.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
