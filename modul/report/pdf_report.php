 <?php
//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

session_start();
include '../../config/koneksi.php';
include 'rep_function.php';
require_once('../../assets/tcpdf/tcpdf.php');
require_once('../../control/class.php');
$company = new Report();
$companyc = new Master();
$datenow = date('d-M-Y');
$period = $_REQUEST['period'];
$bdd = $_REQUEST['bdd'];
$bpd = $_REQUEST['bpd'];
$id = $_REQUEST['period'];
$id_period = $company->GetPeriod();
// extend TCPF with custom functions

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('eBudgeting');
$pdf->SetAuthor('eBudgeting');
$pdf->SetTitle('Detail Report');
$pdf->SetSubject('Detail Report');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20, PDF_MARGIN_TOP, 15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
// set font


// add a page

$pdf->AddPage('L', 'A4');

$pdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));

$pdf->SetFont('times', '', 8);

$pdf->Cell(0, 1, 'Bidang Pelayanan : '.$company->GetBadan('desc_tbd',$bdd), 0, 1, 'L');
$pdf->Cell(0, 1, 'Bidang : '.$company->GetBidang('name_tbp',$bpd), 0, 1, 'L');
$pdf->Cell(0, 1, 'ANGGARAN PROGRAM KERJA', 0, 1, 'C');
$pdf->Cell(0, 2, 'PERIODE: '.$company->GetPeriod('from_year',$id).' - '.$company->GetPeriod('to_year',$id), 0, 1, 'C');
$pdf->Cell(0, 1, 'Tanggal Print: '.$datenow, 0, 3, 'C');
$pdf->Ln(4);
// column titles
$pdf->SetFont('helvetica',  5);

//$pdf->Cell(0, 0, 'Marketing Expense Claim', 0, 1, 'C');
//$pdf->Ln(1);



$html = '<style> 
	table{ 
    width: 850px; 
    border-collapse: collapse; 
    margin-top:55px;
    }
	tr:nth-of-type(odd) { 
    background: #eee; 
    }
	th { 
    background-color: #FFDD33; 
    color: black; 
    font-weight: bold; 
    }
	td{ 
    padding: 5px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 8px;
    }
	</style>';
$rutin = mysql_fetch_array(mysql_query("select SUM( b.cost_price * a.total_event * a.total_human ) tot
									FROM document_tree a left join cost_type b on a.id_cost = b.id_cost where a.id_tbd='$bdd' 
									and a.id_tbp='$bpd' and a.id_period='$period' and a.jenis!='2'"));
									$sumrutin = $rutin["tot"];
$nontrutin = mysql_fetch_array(mysql_query("select 
											SUM( a.sub_total * a.total_human * a.total_event ) tot2 
									FROM document_tree a left join cost_type b on a.id_cost = b.id_cost where a.id_tbd='$bdd' 
									and a.id_tbp='$bpd' and a.id_period='$period' and a.jenis!='1'"));
									$sumnontrutin = $nontrutin["tot2"];									
	
$display = display(0);
$gx	= displayc(0);
$displaynonrutin = displaynonrutin(0);  
$g	= displaycnonrutin(0); 
$html .='<table width="150px;" cellstyle="0">
		<tr><td>Total RUTIN  </td> <td>: '.number_format($gx).'</td></tr>
		<tr><td>Total Non RUTIN </td> <td>: '.number_format($g).'</td></tr>
		<tr><td>Grand Total </td> <td>: '.number_format($gx+$g).'</td></tr>
		</table><br/><br/>';	
$html .='<tr><td>RUTIN</td></tr>';
$html .= '<table >
		<tr>
		<th rowspan="2" width="25">No</th>
		<th rowspan="2" width="250">Nama Kegiatan & Rincian Anggaran</th>
		<th width="120" colspan="2" align="center">Jumlah </th>
		<th width="150" colspan="2" align="center">Anggaran </th>
		<th width="75" rowspan="2" align="center">Ref No </th>
		<th width="100" rowspan="2" align="center">Waktu </th>
		<th width="200" rowspan="2" align="center">Sasaran & Tujuan </th>
		</tr>
		<tr>
        <th width="60" align="center">Org / Brg</th>
        <th width="60" align="center">Event</th> 
		<th width="75" align="right">Sub</th>
        <th width="75" align="right">Total</th>
		</tr>
		<tbody>';
	

			
		$html	.= $display;  
		  	
		$html	.='</tbody>
			<tfoot>
		<tr>
		  <td colspan="5">Total</td>
		  <td align="right"><b>'.number_format($gx).'</b></td>
		  <td></td>
		  <td></td>
		  <td></td>
		 </tr></tfoot></table>';
		 $html.='<br/><br/><br/><br/>';
		 $html .='<tr><td>NON RUTIN</td></tr>';
		$html .= '<table >
		<tr>
		<th rowspan="2" width="25">No</th>
		<th rowspan="2" width="250">Nama Kegiatan & Rincian Anggaran</th>
		<th width="120" colspan="2" align="center">Jumlah </th>
		<th width="150" colspan="2" align="center">Anggaran </th>
		<th width="75" rowspan="2" align="center">Ref No </th>
		<th width="100" rowspan="2" align="center">Waktu </th>
		<th width="200" rowspan="2" align="center">Sasaran & Tujuan </th>
		</tr>
		<tr>
        <th width="60" align="center">Org / Brg</th>
        <th width="60" align="center">Event</th> 
		<th width="75" align="right">Sub</th>
        <th width="75" align="right">Total</th>
		</tr>
		<tbody>';
	

			
		$html	.=  $displaynonrutin;	
		$html	.='</tbody>
			<tfoot>
		<tr>
		  <td colspan="5">Total</td>
		  <td align="right"><b>'.number_format($g).'</b></td>
		  <td></td>
		  <td></td>
		  <td></td>
		 </tr></tfoot></table>';
	 
$pdf->writeHTML($html);
$pdf->SetFont('helvetica', '', 7);
//$dq = $pdf->writeHTML($html);

$pdf->SetFillColor(255, 255, 255);



// ---------------------------------------------------------
ob_clean();
// close and output PDF document
$pdf->Output('Detail Report.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
