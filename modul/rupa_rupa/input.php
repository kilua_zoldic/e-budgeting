<form>
    <div class="easyui-layout" style="width:100%;height:600px;">
        <div data-options="region:'north'" style="height:550px;width: 100%;border:0">
            <table cellpadding="5" id="konten" style="float:left;">
                <tr>
                    <td>Date</td> <td>: <input name="time" id="time" class="easyui-datetimebox" style="width:250px;" data-options="prompt:'Date.'" ></td>
                </tr>
                <tr>
                    <td>Kategori </td> <td>: <input id="cano" required="true" style="width:250px;" data-options="prompt:'Kategori .'" name="id"/></td>
                </tr>
                <tr id="tts">
                    <td>Total</td> <td>: <input name="jmlh" id="jmlh" class="easyui-textbox" style="width:150px;" data-options="prompt:'Jumlah.'" ></td>
                    <td>Currency</td> <td>:  <select class="easyui-combobox" name="curr"  labelPosition="top" style="width:100%;">
                            <option value="1">RP</option>
                            <option value="2">Dollar</option>
                        </select></td>
                </tr>
                <tr>
                    <td>Method</td> <td>:  <select class="easyui-combobox" name="mthd"  labelPosition="top" style="width:250px;">
                            <option value="1">Tunai</option>
                            <option value="2">Cek</option>
                            <option value="2">Transfer</option>
                            <option value="2">EDC</option>
                        </select></td>
                </tr>
                <tr>
                    <td>Optional</td>

                </tr>
                <tr>
                    <td>No Anggota</td> <td>: <input name="jmlh" id="jmlh" class="easyui-textbox" style="width:250px;" data-options="prompt:'No Anggota.'" ></td>

                </tr>
                <tr>
                    <td>Nama</td> <td>: <input name="jmlh" id="jmlh" class="easyui-textbox" style="width:250px;" data-options="prompt:'Nama.'" ></td>

                </tr>
                <tr>
                    <td>Keterangan</td> <td>: <input name="jmlh" id="jmlh" class="easyui-textbox" style="width:250px;" data-options="prompt:'Keterangan.'" ></td>

                </tr>
                <tr>
                    <td></td><td>
                        <a href="javascript:void(0)" class="easyui-linkbutton tambah" iconCls="icon-ok" style="width:90px">Tambah</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmca()" style="width:90px">Confirm</a>
                        <a id="printcash" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printcash()" style="width:90px">Print</a>
                        <a id="printtrf" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printrf()" style="width:90px">Print</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="clearca()" style="width:90px">Clear</a>
                    </td>
                </tr>
            </table>
        </div>

    </div>

</form>
<script type="text/javascript" src="modul/cashadvance/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#printcash').linkbutton('disable');
        $('#printtrf').linkbutton('disable');
        $('#printcash').hide();
        $('#printtrf').hide();
        $('.hide').hide();
        $('.trf').hide();
    });
</script>
<script>
    var nomor = 1;
    $(".tambah").click(function(){
        nomor ++;
        if(nomor > 10){
            return false;
        }
        $('#konten #tts:first').after(
            '<tr class="baris"> <td>Total</td><td>: '
            +'<input name="nomor[]" value="'+ nomor +'" type="hidden">'    
            + '<input class="easyui-textbox" name="jmlh'+ nomor +'" type="text" style="width:145px;height: 18px;" data-options=""></td>'
            + '<td>Currency</td> <td>:  <select class="easyui-combobox'+ nomor +'" name="curr"  labelPosition="top" style="width:100%;">'
            +'<option value="1">RP</option><option value="2">Dollar</option></select></td>'
            + '<td><input type="button" id="hapus" value="Hapus"></tr>'
        );
    });
    $(document).on("click","#hapus",function(){
        $(this).parents(".baris").remove();
    });
    $('#typepm').combobox({
        valueField: 'typepm',
        onSelect: function (rec) {
            $('.hide').hide();
            $('.trf').hide();
            if (rec.typepm == 1) {
                $('.hide').show();
                $('.trf').hide();

            }
            else if(rec.typepm == 2) {
                $('.trf').show();
                $('.hide').show();
                $('#bankid').combobox({
                    url:'modul/bank/bankaction.php?act=listbank',
                    valueField:'idbank',
                    textField:'bank_name'
                });

            }
        }
    });
    $('#detkeg').datagrid({
        url:'',
        emptyMsg: 'No Records Found',
    });

    $('#cano').combotree({
        url: 'modul/ar_kategori/arkataction.php?act=listparentkat&stat=1',
        valueField: 'idar_kategori',
        textField: 'kategori'
    });

    function confirmca(){
        //to get the loaded data
        //alert(url);

        $.messager.confirm('Confirm', 'Are you sure you want to Confirm this ?', function (r) {
            if (r) {

            }

        });

    }
    function clearca(){
        $('#detkeg').datagrid({
            url:'',
            emptyMsg: 'No Records Found',
        });
        $('#refno').combobox('setValue','');
        $('#name_tbd').textbox('setValue','');
        $('#datenow').textbox('setValue','');
        $('#name_tbp').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#cano').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
    function printcash(){
        url = "modul/cashadvance/pdf_report.php?bp="+$('#name_tbp').textbox('getValue')+"&bd="+$('#name_tbd').textbox('getValue')+"&nk="+$('#nk').textbox('getValue')+
            "&cano="+$('#cano').textbox('getValue')+"&refno="+$('#refno').combobox('getValue')+"&typepm="+$('#typepm').combobox('getValue');
        window.open(url);
    }
    function printrf(){
        url = "modul/cashadvance/pdf_report.php?bp="+$('#name_tbp').textbox('getValue')+"&bd="+$('#name_tbd').textbox('getValue')+"&nk="+$('#nk').textbox('getValue')+
            "&cano="+$('#cano').textbox('getValue')+"&refno="+$('#refno').combobox('getValue')+"&typepm="+$('#typepm').combobox('getValue')+
            "&bankid="+$('#bankid').combobox('getValue')+"&rek_no="+$('#rek_no').textbox('getValue')+"&an="+$('#an').textbox('getValue');
        window.open(url);
    }
</script>