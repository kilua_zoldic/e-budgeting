<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 12/18/16
 * Time: 3:47 PM
 */
?>

<form>
        <div class="easyui-layout" style="width:100%;height:800px;">
            <div data-options="region:'north'" style="height:180px;width: 100%;border:0">
                <div data-options="region:'west',split:true" title="West" style="width:100%;">
                    <table cellpadding="5" style="float:left;">
                        <tr>
                            <td>Payment No</td> <td>: <input id="cashno" required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'Payment No.'" name="cash_advance"/></td>
                        </tr>
                        <tr>
                            <td>No Ref</td> <td>: <input id="refno" class="easyui-textbox" required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'Ref No.'" name="refno" disabled/></td>

                            <td>Description</td> <td>: <input id="desc"  class="easyui-textbox" required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'Description.'" name="refno" disabled/></td>
                        </tr>
                        <tr>
                            <td>BP</td> <td>: <input id="bp"  class="easyui-textbox" required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'BP.'" name="refno" disabled/></td>

                            <td>BD</td> <td>: <input id="bd"  class="easyui-textbox" required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'BP.'" name="refno" disabled/></td>
                        </tr>
                        <tr>
                        <td>Anggaran</td> <td>: <input id="ta"  class="easyui-numberbox" required="true" style="width:250px;" data-options="precision:2,groupSeparator:',',decimalSeparator:'.',prompt:'Anggaran.'" name="refno" disabled/></td>

                            <td>Cash Advance Ammount</td> <td>: <input id="ca" class="easyui-numberbox" required="true" style="width:250px;" data-options="precision:2,groupSeparator:',',decimalSeparator:'.',prompt:'Cash Advance Ammout.'" name="refno" disabled/></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div data-options="region:'south',split:false,border:false" style="height:300px;">
                <table cellpadding="5" style="float:left;">

                    <tr>
                        <td>Pengembalian</td><td>: <input id="cashback" class="easyui-textbox" ></td>
                    </tr>
                    <tr>
                        <td>Paid To</td><td>: <select name="typepm" style="width:200px;" id="typepm" class="easyui-combobox" data-options="prompt:'Type.'" required="true">
                                <option ></option>
                                <option value="1">Cash</option>
                                <option value="2">Transfer</option></select></td>
                    </tr>
                    <tr class="trf">
                        <td>Rek No</td><td>: <input  id="rek_no"class="easyui-textbox" style="width:200px;" data-options="prompt:'Rek No.'" name="rek_no"/></td>
                    </tr>

                    <tr class="trf">
                        <td>Bank </td><td>: <input id="bankid" style="width:200px;" data-options="prompt:'Bank.'" name="bank_name"/></td>
                    </tr>
                    <tr class="trf">
                        <td>A / n</td><td>: <input id="an" class="easyui-textbox" style="width:200px;" data-options="prompt:'A/n.'" name="an"/></td>
                    </tr>
                    <tr><td></td>
                        <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmca()" style="width:90px">Confirm</a>
                            <a id="printcash" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printcash()" style="width:90px">Print</a>
                            <a id="printtrf" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printrf()" style="width:90px">Print</a>
                        </td>
<!--                        <td>   <a href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="clearca()" style="width:90px">Clear</a></td>-->
                    </tr>
                </table>
            </div>
        <div data-options="region:'center',border:false,plain:false" style="height:300px;" >
<!--            <div class="easyui-tabs" data-options="border:false,plain:true" style="height:200px;" >-->
<!--                <div title="Rincian" style="padding:5px;" >-->
                <table id="detkeg"
                       data-options="showFooter: true,singleSelect:true,fit:true,fitColumns:true" style="width:100%;height:300px;">
                    <thead>
                    <tr>
                        <th data-options="field:'id'" hidden="true" width="80">ID</th>
                        <th data-options="field:'cost_name'" width="250">Nama Kegiatan</th>
                        <th data-options="field:'sub_total',align:'right',formatter:formatPrice" width="130">Anggaran</th>
                        <th data-options="field:'paid',align:'right',formatter:formatPrice" width="130">Paid</th>
                        <th data-options="field:'attr1',align:'right',editor:{type:'numberbox',options:{precision:2}}" width="130">Settlement</th>
                        <th data-options="field:'ts',align:'right',formatter:formatPrice" width="130">YTD Settlement</th>
<!--                        <th data-options="field:'ca',align:'right',formatter:formatPrice" width="130">CA Request</th>-->
                        <th data-options="field:'status',align:'right',formatter:formatPrice" width="130">Balance</th>
                        <th data-options="field:'remarks',align:'center',editor:{type:'text'}" width="280">Remarks</th>
                       </tr>
                    </thead>
                </table>
<!--                    </div>-->
<!--                </div>-->
            </div>

        </div>
</form>
<script type="text/javascript" src="modul/settlement/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#printcash').hide();
        $('#printtrf').hide();
        $('#printcash').linkbutton('disable');
        $('#printtrf').linkbutton('disable');
        $('.hide').hide();
        $('.trf').hide();
    });
</script>
<script>
    $('#typepm').combobox({
        valueField: 'typepm',
        onSelect: function (rec) {
            $('.hide').hide();
            $('.trf').hide();
            if (rec.typepm == 1) {
                $('.hide').show();
                $('.trf').hide();

            }
            else if(rec.typepm == 2) {
                $('.trf').show();
                $('.hide').show();
                $('#bankid').combobox({
                    url:'modul/bank/bankaction.php?act=listbank',
                    valueField:'idbank',
                    textField:'bank_name'
                });
                $('#bankid').combobox('setValue',1);
            }
        }
    });
    $('#detkeg').datagrid({
    });
    function modal(){
        $('#dlgpayment').dialog('open').dialog('setTitle','List Cash Advance ' );
        $('#calist').datagrid({
            url:'modul/payment/listca.json',
        });
    }
    $('#cashno').combobox({
        url: 'modul/settlement/listca.json',
        valueField: 'id',
        textField: 'cash_advance',
        onSelect: function(rec){
            $('#bp').textbox('setValue',rec.bp);
            $('#bd').textbox('setValue',rec.bd);
            $('#ca').numberbox('setValue',rec.ca);
            $('#ta').numberbox('setValue',rec.total);
            $('#desc').textbox('setValue',rec.desc);
            $('#refno').textbox('setValue',rec.refno);
//			$('#time').textbox('setValue',rec.wktu);
//			$('#dcost').textbox('setValue',rec.desc_cost);
//			$('#jenis').combobox('setValue',rec.jenis);
//			$('#total').numberbox('setValue','17000000');
//			$('#rev').numberbox('setValue','10000000');
            $('#detkeg').datagrid({
                //url:'modul/cashadvance/cashadvanceaction.php?act=detkeg&id='+rec.id,
                url:'modul/settlement/detca.json',
                pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
//				onDblClickRow:function(index,row){
//					//editproker(index);
//					detrincian(index,row);
//				},
//				view: myview,
                emptyMsg: 'No Records Found',
            });
            $('#detkeg').datagrid('enableCellEditing').datagrid('gotoCell', {
                index: 0,
                field: 'id'
            });
            $('#typepm').combobox('setValue','2');
            var typ = $('#typepm').combobox('getValue');
            if (typ == 1) {
                $('.hide').show();
                $('.trf').hide();

            }
            else if(typ == 2) {
                $('.trf').show();
                $('.hide').show();
                $('#bankid').combobox({
                    url:'modul/bank/bankaction.php?act=listbank',
                    valueField:'idbank',
                    textField:'bank_name'
                });
                $('#bankid').combobox('setValue','1');
                $('#rek_no').textbox('setValue','123456');
                $('#an').textbox('setValue','jhonny');

            }
        }

    });

    function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.cash_advance + '</span>';
        return s;
    }

    function confirmca(){
        //to get the loaded data
        //alert(url);
        var cekpaidto = $('#typepm').combobox('getValue');
        if(cekpaidto == '1') {
            $.messager.confirm('Confirm', 'Are you sure you want to Confirm this ?', function (r) {
                if (r) {
                    $('#cano').combobox('setValue', 'P-1612000001');
                    $('#printcash').show();
                    $('#printtrf').hide();
                    $('#printcash').linkbutton('enable');
                }

            });
        }
        else if(cekpaidto == '2') {
            $.messager.confirm('Confirm', 'Are you sure you want to Confirm this ?', function (r) {
                if (r) {
                    $('#cano').combobox('setValue', 'P-1612000001');
                    $('#printtrf').show();
                    $('#printcash').hide();
                    $('#printtrf').linkbutton('enable');
                }

            });
        }
        else {
            $.messager.show({
                title: 'Error',
                msg: 'Paid to required'
            });
        }
    }
    function printcash(){
        url = "modul/settlement/settlement_report.php?bp="+$('#bp').textbox('getValue')+"&bd="+$('#bd').textbox('getValue')+"&nk="+$('#desc').textbox('getValue')+
            "&cano="+$('#cashno').combobox('getValue')+"&refno="+$('#refno').textbox('getValue')+"&typepm="+$('#typepm').combobox('getValue')+
            "&ta="+$('#ta').textbox('getValue')+"&ca="+$('#ca').textbox('getValue')+"&cashback="+$('#cashback').textbox('getValue');
        window.open(url);
    }
    function printrf(){
        url = "modul/settlement/settlement_report.php?bp="+$('#bp').textbox('getValue')+"&bd="+$('#bd').textbox('getValue')+"&nk="+$('#desc').textbox('getValue')+
            "&cano="+$('#cashno').combobox('getValue')+"&refno="+$('#refno').textbox('getValue')+"&typepm="+$('#typepm').combobox('getValue')+
            "&bankid="+$('#bankid').combobox('getValue')+"&rek_no="+$('#rek_no').textbox('getValue')+"&an="+$('#an').textbox('getValue')+
            "&ta="+$('#ta').textbox('getValue')+"&ca="+$('#ca').textbox('getValue')+"&cashback="+$('#cashback').textbox('getValue');
        window.open(url);
    }

    function clearca(){
        $('#detkeg').datagrid({
            url:'',
        });
        $('#refno').combobox('setValue','');
        $('#bp').textbox('setValue','');
        $('#bd').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#cano').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
</script>
