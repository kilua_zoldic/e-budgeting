<table id="ct" fit="true" border="false" style="width:100%;height:100%;" pageSize="50"
toolbar="#toolbarct" pagination="true"  maximizable="true"  fitColumns="true"
rownumbers="true" singleSelect="true"  idField="id_cost" showFooter="true" sortName="cost_name" sortOrder="asc"
>
<thead>
<tr>
<th data-options="field:'cost_name',width:150,sortable:true">Unit Name</th>
<th data-options="field:'unit',width:150,sortable:true">Unit</th>
<th data-options="field:'description',width:200,sortable:true">Note</th>
<th data-options="field:'cost_price',width:100,sortable:true,formatter:formatPrice">Unit Cost</th>
<th data-options="field:'cost_code_number',width:100,sortable:true">Cost Code Number</th>
<th data-options="field:'cost_code_name',width:100,sortable:true">Cost Code Name</th>
<th field="create_by" width="100" sortable="true" >Create By</th>
<th field="create_date" width="100" sortable="true" >Create Time</th>
<th field="update_by" width="100" sortable="true" >Update By</th>
<th field="update_date" width="100" sortable="true" >Update Date</th>
<th field="status_costtype" width="100" formatter="status_proker" sortable="true" >Status</th>
</tr>
</thead>
</table>
<div id="toolbarct">
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newcost()">New Cost</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deletecost()">Remove Cost</a>
<!--<table cellpadding="5">
	<tr>
	<td>Unit Name</td><td>:
		<input class="easyui-textbox" style="width:160px"  id="cost_name" data-options="prompt:'Unit Name.'"></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" style="width:90px" onclick="search_ct()">Search</a></td>
	</tr>
</table>-->	
</div>

<div id="dlg" class="easyui-dialog" style="width:70%;height:500px;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
	<div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
		<form id="fm" method="post" novalidate>
		<table cellpadding="5">
		<tr>
		<td>Unit Name </td>
		<td>: <input name="cost_name" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Unit Name.'" required="true"></td>
		</tr>
		<tr>
		<td>Unit  </td>
		<td>: <input name="unit" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Unit.'" required="true"></td>
		</tr>
		<tr>
		<td>Note</td>
		<td>:  <input name="description" style="width:300px;height:100px;" class="easyui-textbox db" data-options="multiline:true,prompt:'Note.'" ></td>
		</tr>
		<tr>
		<td>Unit Cost</td>
		<td>:  <input name="cost_price" style="width:200px;" class="easyui-numberbox db" data-options="precision:2,groupSeparator:'.',decimalSeparator:',',prompt:'Cost Price.'" required="true"></td>
		</tr>
		<tr>
		<td>Cost Code Number</td>
		<td>: <input name="id_cost_code"  style="width:200px;" id="ccn" class="easyui-combobox" data-options="formatter: formatCCN,prompt:'Cost Code Number.'" /></td>
		</tr>
		<tr>
		<td>Status</td>
		<td>:  <select  name="status_costtype" style="width:200px;" class="easyui-combobox dbc" data-options="prompt:'Status.'" required="true">
		<option></option>
		<option value="0">InActive</option>
		<option value="1">Active</option>
		</select>
		</td>
		</tr>
		
		</table>
		
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savecost()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>
<script>
var url;
var dg = ct;
function formatCCN(row){
	var s = '<span style="font-weight:bold">' + row.cost_code_number + '</span><br/>' +
		'<span style="color:#888"> ' + row.cost_code_name + '</span><br/>';
	return s;
}
	$('#ct').datagrid({
		url: 'modul/costtype/costtypeaction.php?act=list',
	onDblClickRow:function(index,row){
				editcosttype(index);
			},
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
                }	
	});
			

$('#ct').datagrid('enableFilter',[{
				field:'status_costtype',
				type:'combobox',
				options:{
					panelHeight:'auto',
					data:[{value:'',text:'All'},{value:'1',text:'Active'},{value:'0',text:'InActive'}],
					onChange:function(value){
						if (value == ''){
							$('#ct').datagrid('removeFilterRule', 'status_costtype');
						} else {
							$('#ct').datagrid('addFilterRule', {
								field: 'status_costtype',
								op: 'equal',
								value: value
							});
						}
						$('#ct').datagrid('doFilter');
					}
				}
			}]);			
function newcost(){
	$('#dlg').dialog('open').dialog('setTitle','New Unit Cost');
	$('#fm').form('clear');
	$('.db').textbox('enable');
	$('.dbc').combobox('enable');
	$('#edt').linkbutton('disable');
	$('.sv').linkbutton('enable');
	$('#ccn').combobox({
		url:'modul/costcode/costcodeaction.php?act=listcostcode',
		valueField:'id_cost_code',
		textField:'cost_code_number',
	});
	url = 'modul/costtype/costtypeaction.php?act=create';
}

function editcosttype(){
	var row = $('#ct').datagrid('getSelected');
	if(row)
	{
	$('#dlg').dialog('open').dialog('setTitle','View / '+row.cost_name+ ' / ' +row.unit + ' / ' +row.description);
	$('#fm').form('load',row);
	$('#ccn').combobox({
		url:'modul/costcode/costcodeaction.php?act=listcostcode',
		valueField:'id_cost_code',
		textField:'cost_code_number',
	});
	$('#edt').linkbutton('enable');
	url = 'modul/costtype/costtypeaction.php?act=update&id='+row.id_cost;
	$('.db').textbox('disable');
	$('.dbc').combobox('disable');	
	$('.sv').linkbutton('disable');		
	$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
	}		
}

function savecost(){
	//to get the loaded data
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
		$('#fm').form('submit',{
			url: url,
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.errorMsg){
					$.messager.show({
						title: 'Error',
						msg: result.errorMsg
					});
				} else {
					$.messager.show({
						title: 'Success',
						msg: result.success
					});
					$('#dlg').dialog('close');		// close the dialog
					$('#ct').datagrid('reload');	// reload the user data
				}
			}
		});}
	});
}

function deletecost(){
var row = $('#ct').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/costtype/costtypeaction.php?act=delete',{id:row.id_cost},function(result){
if (result.success){
$('#ct').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function search_ct(){
$('#ct').datagrid('load',{
	cost_name: $('#cost_name').val()
	});
}
</script>