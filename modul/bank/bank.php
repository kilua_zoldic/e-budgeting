<table id="bank" fit="true" border="false" style="width:100%;height:100%;" pageSize="50"
toolbar="#toolbarbank" pagination="true"  maximizable="true"  fitColumns="true"
rownumbers="true" singleSelect="true"  idField="idbank" showFooter="true" sortName="bank_name" sortOrder="asc"
>
<thead>
<tr>
<th data-options="field:'bank_name',width:150,sortable:true">Bank Name</th>
<th field="status_bank" width="100" formatter="status_proker" sortable="true" >Status</th>
</tr>
</thead>
</table>
<div id="toolbarbank">
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newbank()">New Bank</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deletebank()">Remove Bank</a>
<!--<table cellpadding="5">
	<tr>
	<td>Unit Name</td><td>:
		<input class="easyui-textbox" style="width:160px"  id="cost_name" data-options="prompt:'Unit Name.'"></td>
		<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" style="width:90px" onclick="search_ct()">Search</a></td>
	</tr>
</table>-->	
</div>

<div id="dlg" class="easyui-dialog" style="width:50%;height:300px;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
	<div id="dlg-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
		<form id="fm" method="post" novalidate>
		<table cellpadding="5">
		<tr>
		<td>Bank Name </td>
		<td>: <input name="bank_name" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Bank Name.'" required="true"></td>
		</tr>
		<td>Status</td>
		<td>:  <select  name="status_bank" style="width:200px;" class="easyui-combobox dbc" data-options="prompt:'Status.'" required="true">
		<option></option>
		<option value="0">InActive</option>
		<option value="1">Active</option>
		</select>
		</td>
		</tr>
		
		</table>
		
		</form>
</div>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savebank()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>
<script>
var url;
var dg = bank;

	$('#bank').datagrid({
		url: 'modul/bank/bankaction.php?act=list',
	onDblClickRow:function(index,row){
		editbank(index);
			},
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
                }	
	});
			

$('#bank').datagrid('enableFilter',[{
				field:'status_bank',
				type:'combobox',
				options:{
					panelHeight:'auto',
					data:[{value:'',text:'All'},{value:'1',text:'Active'},{value:'0',text:'InActive'}],
					onChange:function(value){
						if (value == ''){
							$('#bank').datagrid('removeFilterRule', 'status_bank');
						} else {
							$('#bank').datagrid('addFilterRule', {
								field: 'status_bank',
								op: 'equal',
								value: value
							});
						}
						$('#bank').datagrid('doFilter');
					}
				}
			}]);			
function newbank(){
	$('#dlg').dialog('open').dialog('setTitle','New Bank');
	$('#fm').form('clear');
	$('.db').textbox('enable');
	$('.dbc').combobox('enable');
	$('#edt').linkbutton('disable');
	$('.sv').linkbutton('enable');
	
	url = 'modul/bank/bankaction.php?act=create';
}

function editbank(){
	var row = $('#bank').datagrid('getSelected');
	if(row)
	{
	$('#dlg').dialog('open').dialog('setTitle','View / '+row.bank_name);
	$('#fm').form('load',row);
	$('#edt').linkbutton('enable');
	url = 'modul/bank/bankaction.php?act=update&id='+row.idbank;
	$('.db').textbox('disable');
	$('.dbc').combobox('disable');	
	$('.sv').linkbutton('disable');		
	$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
	}		
}

function savebank(){
	//to get the loaded data
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
		$('#fm').form('submit',{
			url: url,
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.errorMsg){
					$.messager.show({
						title: 'Error',
						msg: result.errorMsg
					});
				} else {
					$.messager.show({
						title: 'Success',
						msg: result.success
					});
					$('#dlg').dialog('close');		// close the dialog
					$('#bank').datagrid('reload');	// reload the user data
				}
			}
		});}
	});
}

function deletebank(){
var row = $('#bank').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
if (r){
$.post('modul/bank/bankaction.php?act=delete',{id:row.idbank},function(result){
if (result.success){
$('#bank').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}

function search_ct(){
$('#bank').datagrid('load',{
	cost_name: $('#bank_name').val()
	});
}
</script>