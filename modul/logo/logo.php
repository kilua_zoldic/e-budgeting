<table id="logo" fit="true" border="false" fit="true" border="false" style="width:100%;height:100%;" pageSize="50"
	   toolbar="#toolbarlogo" pagination="true"  maximizable="true"  fitColumns="true"
	   rownumbers="true" singleSelect="true"  idField="idlogo" showFooter="true" sortName="nama_logo" sortOrder="asc"
>
	<thead>
	<tr>
		<th data-options="field:'x',width:50,formatter:image">Image</th>
		<th data-options="field:'nama_logo',width:150,sortable:true">Logo Name</th>
		<th field="status" width="100" formatter="status_proker" sortable="true" >Status</th>
	</tr>
	</thead>
</table>
<div id="toolbarlogo">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newlogo()">New Logo</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deletelogo()">Remove Logo</a>
	<!--<table cellpadding="5">
        <tr>
        <td>Unit Name</td><td>:
            <input class="easyui-textbox" style="width:160px"  id="cost_name" data-options="prompt:'Unit Name.'"></td>
            <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" style="width:90px" onclick="search_ct()">Search</a></td>
        </tr>
    </table>-->
</div>

<div id="dlg" class="easyui-dialog" style="width:70%;height:350px;padding:10px 20px"  resizable="true" maximizable="true" toolbar="#dlg-toolbar" closed="true"  buttons="#dlg-buttons">
	<div id="dlg-toolbar" style="padding:2px 0">
		<table cellpadding="0" cellspacing="0" style="width:100%">
			<tr>
				<td style="padding-left:2px">
					<a href="javascript:void(0)" class="easyui-linkbutton" id="edt" data-options="iconCls:'icon-edit',plain:true" onclick="editfield()" >Edit</a>
				</td>
			</tr>
		</table>
	</div>
	<form id="fm" method="post" enctype="multipart/form-data" novalidate>
		<table cellpadding="5">
			<tr>
				<td>Image File</td>
				<td>: <input name="file" class="f1 easyui-filebox db"  style="width:200px;" ></td>
			</tr>
			<tr>
				<td>Logo Name  </td>
				<td>: <input name="nama_logo" style="width:200px;" class="easyui-textbox db" data-options="prompt:'Logo Name.'" required="true"></td>
			</tr>
			<tr>
				<td>Status</td>
				<td>:  <select  name="status" style="width:200px;" class="easyui-combobox dbc" data-options="prompt:'Status.'" required="true">
						<option></option>
						<option value="0">InActive</option>
						<option value="1">Active</option>
					</select>
				</td>
			</tr>

		</table>

	</form>
</div>
<div id="dlg-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="savecost()" style="width:90px">Save</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>
<script>
	var url;
	var dg = logo;

	$(dg).datagrid({
		url: 'modul/logo/logoaction.php?act=list',
		onDblClickRow:function(index,row){
			editlogo(index);
		},
		onHeaderContextMenu: function(e, field){
			e.preventDefault();
			if (!cmenu){
				createColumnMenu(dg);
			}
			cmenu.menu('show', {
				left:e.pageX,
				top:e.pageY
			});
		}
	});


	$(dg).datagrid('enableFilter',[{
		field:'status',
		type:'combobox',
		options:{
			panelHeight:'auto',
			data:[{value:'',text:'All'},{value:'1',text:'Active'},{value:'0',text:'InActive'}],
			onChange:function(value){
				if (value == ''){
					$(dg).datagrid('removeFilterRule', 'status');
				} else {
					$(dg).datagrid('addFilterRule', {
						field: 'status',
						op: 'equal',
						value: value
					});
				}
				$(dg).datagrid('doFilter');
			}
		}
	}]);
	function newlogo(){
		$('#dlg').dialog('open').dialog('setTitle','New Unit Cost');
		$('#fm').form('clear');
		$('.db').textbox('enable');
		$('.dbc').combobox('enable');
		$('#edt').linkbutton('disable');
		$('.sv').linkbutton('enable');

		url = 'modul/logo/logoaction.php?act=create';
	}

	function editlogo(){
		var row = $(dg).datagrid('getSelected');
		if(row)
		{
			$('#dlg').dialog('open').dialog('setTitle','View / '+row.cost_name+ ' / ' +row.unit + ' / ' +row.description);
			$('#fm').form('load',row);
			$('#edt').linkbutton('enable');
			url = 'modul/logo/logoaction.php?act=update&id='+row.idlogo;
			$('.db').textbox('disable');
			$('.f1').textbox('clear');
			$('.ims').textbox('disable');
			$('.dbc').combobox('disable');
			$('.sv').linkbutton('disable');
			$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
		}
	}

	function savecost(){
		//to get the loaded data
		$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
				$('#fm').form('submit',{
					url: url,
					onSubmit: function(){
						return $(this).form('validate');
					},
					success: function(result){
						var result = eval('('+result+')');
						if (result.errorMsg){
							$.messager.show({
								title: 'Error',
								msg: result.errorMsg
							});
						} else {
							$.messager.show({
								title: 'Success',
								msg: result.success
							});
							$('#dlg').dialog('close');		// close the dialog
							$(dg).datagrid('reload');	// reload the user data
						}
					}
				});}
		});
	}

	function deletelogo(){
		var row = $(dg).datagrid('getSelected');
		if (row){
			$.messager.confirm('Confirm','Are you sure you want to Delete this ?',function(r){
				if (r){
					$.post('modul/logo/logoaction.php?act=delete',{id:row.idlogo},function(result){
						if (result.success){
							$(dg).datagrid('reload'); // reload the user data
						} else {
							$.messager.show({ // show error message
								title: 'Error',
								msg: result.errorMsg
							});
						}
					},'json');
				}
			});
		}
	}
	function image(val,row,index){
		if(row.image_logo != 'null'){
			return '<img src="images/logo/'+row.image_logo+'" style="width:50px;height:50px;">';
		}
		else {
			return '<img src="images/noimage.png" style="width:50px;height:50px;">';
		}
	}
	function search_ct(){
		$(dg).datagrid('load',{
			cost_name: $('#cost_name').val()
		});
	}
</script>