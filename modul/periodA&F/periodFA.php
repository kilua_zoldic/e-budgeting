    <table id="period" style="width:100%;height:100%;" fit="true" border="false" fitColumns="true"
			rownumbers="true" pageSize="50" showFooter="true"  pagination="true" singleSelect="true"
            idField="id_period" periodField="from" toolbar="#toolbar">
        <thead>
            <tr>
                <th field="id_period" hidden="true" width="100" >ID</th>
				<th field="periodenya" width="100" sortable="true">Period</th>
				<th field="status_period" width="100" sortable="true" formatter="status_period">Status</th>
                <th field="create_by" width="100" sortable="true" >Create By</th>
                <th field="create_date" width="100" sortable="true" >Create Time</th>
                <th field="update_by" width="100" sortable="true" >Update By</th>
                <th field="update_date" width="100" sortable="true" >Update Date</th>
             
            </tr>
        </thead>
    </table>
<div id="dlg-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton sv" iconCls="icon-ok" onclick="saveperiod()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
</div>
<div id="dlgedit" class="easyui-dialog" style="width:30%;height:150px;padding:10px 20px"   closed="true"  buttons="#dlgedit-buttons">
		<form id="fmedit" method="post" novalidate>
		<table>
		<tr>
		<td>Status</td>
		<td>:  <select  name="status_period" style="width:200px;" class="easyui-combobox" data-options="prompt:'Status.'" required="true">
		<option></option>
		<option value="0">Open</option>
		<option value="1">Closed</option>
		</select>
		</td>
		</tr>
		
		</table>
		
		</form>
</div>
<div id="dlgedit-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton " iconCls="icon-ok" onclick="updateperiod()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgedit').dialog('close')" style="width:90px">Close</a>
</div>
<script>
var dg = period;
var url;


function status_period(val,row){
	if (val == '1' ){
		return '<span>Open</span>';
		} 
	else {
		return '<span>Closed</span>';
		}
	
}

$('#period').datagrid({
	url:'modul/period/periodaction.php?act=list',
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				editperiod(index);
			},
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
    },
	onBeforeLoad:function(param){

	}
	});
	


function editperiod(){
		var row = $('#period').datagrid('getSelected');
	if(row)
		{

		$('#dlgedit').dialog('open').dialog('setTitle','View / '+row.from_year+ ' / ' +row.to_year);
		$('#fmedit').form('load',row);
		//$('#edt').linkbutton('enable');
//		url = 'modul/period/periodaction.php?act=update&id='+row.id_period;
		url='';
		//$('.dbc').combobox('disable');
		//$('.sv').linkbutton('disable');		
		//$('#edt').linkbutton('<?php echo $company->privilege('edit');?>');
		}		
}
function editfield(){
	$('.dbc').combobox('enable');
	$('.sv').linkbutton('enable');
}
function updateperiod(){
			//to get the loaded data
		$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
			if (r){
			$('#fmedit').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$.messager.show({
							title: 'Success',
							msg: result.success
						});
						$('#dlgedit').dialog('close');		// close the dialog
						$('#period').datagrid('reload');	// reload the user data
						location.reload('modul/period/periodFA.php');
					}
				}
			});
		}
	});
}

</script>