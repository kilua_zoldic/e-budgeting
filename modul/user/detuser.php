<table id="dguser" style="width:100%;height:100%" url="modul/user/useraction.php?act=detail"
toolbar="#toolbaruser" pagination="true" border="false" fit="true" 
rownumbers="true" singleSelect="true"   idField="id_a" sortName="username" sortOrder="asc">
<thead>
<tr>

<th data-options="field:'id_a',width:80,sortable:true,hidden:true">ID User</th>
<th data-options="field:'username',width:200,sortable:true">Username</th>
<th data-options="field:'nama_lengkap',width:200,sortable:true">Full Name</th>
<th data-options="field:'email',width:200,sortable:true">Email </th>
<th data-options="field:'name',width:100,sortable:true">Role</th>
<th data-options="field:'tlp',width:100,sortable:true">Phone Number</th>
</tr>
</thead>
</table>

<div id="dlguser" class="easyui-dialog" style="width:100%;height:100%;padding:10px 20px" closed="true" buttons="#dlguser-buttons">
   <form id="fmuser" method="post" enctype="multipart/form-data" novalidate>
	<div class="easyui-layout" style="width:100%;height:600px;"  data-options="">
		<!--WEST-->
        <div data-options="region:'west'" style="width:50%;height:150px" border="false">
		<table cellpadding="5">
		
		
		<tr>
		<td>Password *</td>
		<td>: <input name="re_password" type="password" class="easyui-textbox" style="width:250px;"  data-options="prompt:'password'" required="true"></td>
		</tr>
		<tr>
		<td>Email </td>
		<td>: <input name="email" class="easyui-textbox" style="width:250px;"  data-options="prompt:'Email'" ></td>
		</tr>
		<tr>
		<td>Phone Number</td>
		<td>: <input name="tlp" style="width:250px;"  class="easyui-textbox" data-options="prompt:'Phone Number'" ></td>
		</tr>
		<tr>
			<td>Gender *</td><td>: <select name="jk" data-options="prompt:'Choose Sex'" style="width:250px;" class="easyui-combobox" required="true">
			<option selected></option>
			<option value="Male">Male</option>
			<option value="Female">Female</option>
			</select></td></tr>
		<tr>
		<td>Jabatan</td>
		<td>: <input name="jabatan" style="width:250px;"  class="easyui-textbox" data-options="prompt:'Jabatan'" ></td>
		</tr>
			

		</table>
		</div>
		<!--END OF WEST-->
		
		<!--EAST-->
		
        <div data-options="region:'east'" style="width:50%;height:150px" border="false">
		<table cellpadding="5">
		 	
		<tr>
		<td>Full Name *</td>
		<td>: <input name="nama_lengkap" data-options="prompt:'Full Name'" style="width:250px;"  class="easyui-textbox" required="true"></td>
		</tr>
		<tr>
			<td>Picture</td>
			<td>: <input name="file" class="f1 easyui-filebox" style="width:140px;"  ></input></td>
			</tr>
			<tr>
			<td>Note</td>
			<td>: <input name="noted" data-options="prompt:'Note'" data-options="multiline:true" style="width:250px;height:60px" style="width:250px;"  class="easyui-textbox" ></td>
			</tr>
			<tr>
		<td>Work Location</td>
		<td>: <input name="work_location" data-options="prompt:'Work Location'" style="width:250px;"  class="easyui-textbox" ></td>
		</tr>		
		<tr>
		<td>Theme</td>
		<td>: <input id="theme" name="theme" data-options="prompt:'Select Theme'" style="width:250px;" ></td>
		</tr>		
		</table>
		</div>
		</form>
		<!--END OF EAST-->
		
        
        </div>
</div>
<div id="dlguser-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savuser()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlguser').dialog('close')" style="width:90px">Close</a>
</div>

<script>
var url;


    $('#dguser').datagrid({
    view: detailview,
    detailFormatter:function(index,row){
    return '<div class="ddv" style="padding:5px 0"></div>';
    },
	onDblClickRow:function(index,row){
				edituser(index);
			},
    onExpandRow: function(index,row){
    var ddv = $(this).datagrid('getRowDetail',index).find('div.ddv');
    ddv.panel({
    border:false,
    cache:false,
    href:'modul/user/getdetail.php?id='+row.id_a,
    onLoad:function(){
    $('#dguser').datagrid('fixDetailRowHeight',index);
    }
    });
    $('#dguser').datagrid('fixDetailRowHeight',index);
    }
    });
	

function edituser(){
var row = $('#dguser').datagrid('getSelected');
if (row){
$('#dlguser').dialog('open').dialog('setTitle','Edit user');
$('#theme').combobox({
	url:'control/view.php?act=themedir',
	valueField:'theme',
	textField:'theme',	
});
$('#fmuser').form('load',row);

url = 'modul/user/useraction.php?act=updateself&id='+row.id_a;
}
}
 function savuser(){
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
			$('#fmuser').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlguser').dialog('close');		// close the dialog
						$('#dguser').datagrid('reload');	// reload the user data
						
					}
				}
			});
			}
			}
			);
}

</script>