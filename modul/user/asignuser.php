<table id="dguser" style="width:100%;height:100%" url="modul/user/useraction.php?act=listaccrole"
toolbar="#toolbaruser"  border="false" fit="true" 
rownumbers="true" singleSelect="true"   idField="id_a" sortName="username" sortOrder="asc">
<thead>
<tr>

<th data-options="field:'id_a',width:80,sortable:true,hidden:true">ID User</th>
<th data-options="field:'username',width:200,sortable:true">Username</th>
<th data-options="field:'nama_lengkap',width:200,sortable:true">Full Name</th>
<th data-options="field:'email',width:200,sortable:true">Email </th>
<th data-options="field:'jk',width:100,sortable:true">Sex</th>
<th data-options="field:'tlp',width:100,sortable:true">Phone Number</th>
</tr>
</thead>
</table>
<div id="toolbaruser">
<table cellpadding="5">
		<tr>
		<td>Username</td><td>:
			<input class="easyui-textbox" style="width:160px"  id="username" data-options="prompt:'Username.'"></td>
			<td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" style="width:90px" onclick="search_dguser()">Search</a></td>
		</tr>
		</table>
</div>
<div id="dlguser" class="easyui-dialog" style="width:100%;height:100%;padding:10px 20px" closed="true" buttons="#dlguser-buttons">
  
	<table id="tbd" style="width:100%;height:450px;" fit="true" border="false" fitColumns="true"
			rownumbers="true"  showFooter="true"  singleSelect="true" toolbar="#toolbarrole"
            idField="id" treeField="name_tbd">
        <thead>
            <tr>
                <th field="id" hidden="true" width="100" >ID</th>
                <th field="name_tbd" width="100" sortable="true">Name</th>
                <th field="desc_tbd" width="100" sortable="true" >Description</th>
                <th field="view" width="50" sortable="true" formatter="imgrol">View</th>
                <th field="ins" width="50" sortable="true" formatter="imgrol">Insert</th>
                <th field="edit" width="50" sortable="true" formatter="imgrol">Edit</th>
                <th field="delet" width="50" sortable="true" formatter="imgrol">Delete</th>
             
            </tr>
        </thead>
		</table>
		<div id="toolbarrole">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="expandAllgrid()" style="width:80px;">Expand All</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true"onclick="collapseAllgrid()" style="width:90px;">Collapse All</a>
		
		</div>
	
</div>
<div id="dlguser-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlguser').dialog('close')" style="width:90px">Close</a>
</div>

<div id="dlgrole" class="easyui-dialog" style="width:50%;height:60%;padding:10px 15px"  maximizable="true" toolbar="#dlgrole-toolbar" closed="true"  buttons="#dlgrole">
		<div id="dlgrole-toolbar" style="padding:2px 0">
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="padding-left:2px">
				   <a href="javascript:void(0)" class="easyui-linkbutton" id="edtrole" data-options="iconCls:'icon-edit',plain:true" onclick="editfieldrole()" >Edit</a>
                </td>
            </tr>
        </table>
    </div>
	<form id="fmrole" method="post" novalidate>
	<table cellpadding="5">
	
	<tr>
	<td>View</td>
	<td>: <input name="view" style="width:200px;" value="1" class="cb" type="checkbox"></td></tr>
	</tr>
	<tr>
	<td>Add</td>
	<td>: <input name="ins" style="width:200px;" value="1" class="cb" type="checkbox" ></td></tr>
	<tr>
	<td>Edit</td>
	<td>: <input name="edit" style="width:200px;" value="1" class="cb" type="checkbox" ></td></tr>
	<tr>
	<td>Remove</td>
	<td>: <input name="delet" style="width:200px;" value="1" class="cb"  type="checkbox" ></td></tr>
	</table>
	</form>
</div>
<div id="dlgrole">
<a href="javascript:void(0)" class="easyui-linkbutton svrole" iconCls="icon-ok" onclick="saverole()" style="width:90px">Save</a>                   
				
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgrole').dialog('close')" style="width:90px">Close</a>
</div>	
<script>
var url;
var urlrole;
var dg = tbd;
function expandAllgrid(){
	$('#tbd').treegrid('expandAll');
			
}
function collapseAllgrid(){
	$('#tbd').treegrid('collapseAll');
			
}

    $('#dguser').datagrid({
    
	onDblClickRow:function(index,row){
				edituser(index);
			},
    onExpandRow: function(index,row){
    var ddv = $(this).datagrid('getRowDetail',index).find('div.ddv');
    ddv.panel({
    border:false,
    cache:false,
    href:'modul/user/getdetail.php?id='+row.id_a,
    onLoad:function(){
    $('#dguser').datagrid('fixDetailRowHeight',index);
    }
    });
    $('#dguser').datagrid('fixDetailRowHeight',index);
    }
    });

function edituser(){
var row = $('#dguser').datagrid('getSelected');
if (row){
$('#dlguser').dialog('open').dialog('setTitle','Role Access');
//$('#fmuser').form('load',row);

	$('#tbd').treegrid({
	url:'modul/user/useraction.php?act=listrole&id='+row.id_a,
	pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
	onDblClickRow:function(index,row){
				insrole(index);
			},
	animate:true,
		 onLoadSuccess: function(row,data){
            $('#tbd').treegrid('collapseAll')
        },		
  			
	onHeaderContextMenu: function(e, field){
    e.preventDefault();
	if (!cmenu){
	createColumnMenu(dg);
	}
	cmenu.menu('show', {
	left:e.pageX,
	top:e.pageY
	});
                }		
	});	
url = 'modul/user/useraction.php?act=update&id='+row.id_a;
}
}
 function savuser(){
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
			$('#fmuser').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlguser').dialog('close');		// close the dialog
						$('#dguser').datagrid('reload');	// reload the user data
					}
				}
			});
			}
			}
			);
}

function destroyuser(){
var row = $('#dguser').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this user?',function(r){
if (r){
$.post('modul/user/useraction.php?act=delete',{id:row.id_a},function(result){
if (result.success){
$('#dguser').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}



function insrole(){
	//var random = Math.floor(Math.random()*9999);
	var row = $('#tbd').treegrid('getSelected');
	var rowx = $('#dguser').datagrid('getSelected');
	if(row.parentId == '0'){

	}
	else {
	$('#dlgrole').dialog('open').dialog('setTitle','New Access Control username '+rowx.username +' - '+ row.id_master_proker +'-'+ row.id);
	$('#fmrole').form('load',row);
	$('#edt').linkbutton('disable');
	console.log(row.id);
	if(row.id_master_proker == 0){
	urlrole = 'modul/user/useraction.php?act=createrole&id='+row.id+'&id_a='+rowx.id_a;
	}
	else {
	urlrole = 'modul/user/useraction.php?act=updaterole&id='+row.id_master_proker;
	}
	}
}

function saverole(){
//to get the loaded data
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
		$('#fmrole').form('submit',{
			url: urlrole,
			onSubmit: function(){
			return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.errorMsg){
					$.messager.show({
					title: 'Error',
					msg: result.errorMsg
					});
				} else {
					$.messager.show({
					title: 'Success',
					msg: result.success
					});
				$('#dlgrole').dialog('close');		// close the dialog
				$('#tbd').treegrid('reload');	// reload the user data
				}
			}
		});}
	});
}
function search_dguser(){
$('#dguser').datagrid('load',{
	username: $('#username').val()
	});
}
</script>
 <style type="text/css">
  .f1{
width:200px;
}
#fmuser{
margin:0;
padding:10px 30px;
}
.ftitle{
font-size:14px;
font-weight:bold;
padding:5px 0;
margin-bottom:10px;
border-bottom:1px solid #ccc;
}
.fitem{
margin-bottom:5px;
}
.fitem label{
display:inline-block;
width:80px;
}
.fitem input{
width:160px;
}
</style>

