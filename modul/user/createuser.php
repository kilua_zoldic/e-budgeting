 <div class="easyui-panel" fit="true" boder="false" style="width:100%;height:100%;padding:10px;" data-options="iconCls:'icon-save',footer:'#ft'">
   
		
		 <div class="easyui-layout" data-options="fit:true,border:false">
            <form id="fmuser" method="post" enctype="multipart/form-data" novalidate>
            <div data-options="region:'east'" style="width:50%;height:100%;padding:5px;border:0">
            <table cellpadding="5">
			
			<tr>
				<td>Picture</td>
				<td>: <input name="file" class="f1 easyui-filebox" style="width:140px;"  ></input></td>
				</tr>
			<tr>
			<td>Note</td>
			<td>: <input name="noted" data-options="prompt:'Note'" data-options="multiline:true" style="width:250px;height:60px" style="width:250px;"  class="easyui-textbox" ></td>
			</tr>	
				
				</table>
        </div>
       
	   <div data-options="region:'center'" style="width:50%;padding:5px;height:100%;border:0">
        <table cellpadding="9">
		<tr>
		<td>Username *</td>
		<td>: <input name="username" class="easyui-textbox ccd" style="width:250px;"  data-options="prompt:'Username'" required="true"></td>
		</tr>
		
		<tr>
		<td>Password *</td>
		<td>: <input name="re_password" type="password" class="easyui-textbox" style="width:250px;"  data-options="prompt:'password'" required="true"></td>
		
		</tr>
		<tr>
		<td>Email *</td>
		<td>: <input name="email" class="easyui-textbox" style="width:250px;"  data-options="prompt:'Email'" required="true"></td>
		</tr>
		
		
		<tr>
		<td>Role *</td>
		<td>:  <input class="easyui-combobox" required="true" style="width:250px;"
											name="id_level"
											data-options="
											url:'modul/user/useraction.php?act=level',
											method:'get',
											valueField:'id_level',
											textField:'name',
											panelHeight:'auto',
											prompt:'Role'
											">
			</input>
		</td>
		</tr> 
		<tr>
				<td>Status *</td><td>: <select name="aktif" data-options="prompt:'Choose'" style="width:250px;"  class="easyui-combobox" required="true">
				<option selected></option>
				<option value="1">Active</option>
				<option value="0">InActive</option>
				</select></td></tr>
		<tr>
		<td></td><td></td>
		</tr>
		<tr>
		<td>Full Name *</td>
		<td>: <input name="nama_lengkap" data-options="prompt:'Full Name'" style="width:250px;"  class="easyui-textbox" required="true"></td>
		</tr>
		
		<tr>
		<td>Phone Number</td>
		<td>: <input name="tlp" style="width:250px;"  class="easyui-textbox" data-options="prompt:'Phone Number'" ></td>
		</tr>
		<tr>
			<td>Gender *</td><td>: <select name="jk" data-options="prompt:'Choose Sex'" style="width:250px;" class="easyui-combobox" required="true">
			<option selected></option>
			<option value="Male">Male</option>
			<option value="Female">Female</option>
			</select></td></tr>
		<tr>
		<td>Work Location</td>
		<td>: <input name="work_location" data-options="prompt:'Work Location'" style="width:250px;"  class="easyui-textbox" ></td>
		</tr>
			

		</table>
            </div>
		</form>	
        </div>
	


</div>
  <div id="ft" style="padding:5px;">
  <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savuser()" style="width:90px">Save</a>
  </div>
  
<script>
 function savuser(){
		 $.messager.confirm('Save User', 'Are you confirm this?', function(r){
                if (r){
                  
           
			$('#fmuser').form('submit',{
				url: 'modul/user/useraction.php?act=create',
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#fmuser').form('clear');
							// reload the user data
						$.messager.show({
							title: 'Success',
							msg:result.success
						});	
					}
				}
			});
	     }
            });		
}
</script>  