<?php 
switch ($_GET['module']) {
default :
?>
<table id="dguser" style="width:100%;height:100%" url="modul/user/useraction.php?act=list"
toolbar="#toolbaruser" pagination="true" border="false" fit="true" 
rownumbers="true" singleSelect="true"   idField="id_a" sortName="username" sortOrder="asc">
<thead>
<tr>

<th data-options="field:'id_a',width:80,sortable:true,hidden:true">ID User</th>
<th data-options="field:'username',width:200,sortable:true">Username</th>
<th data-options="field:'nama_lengkap',width:200,sortable:true">Full Name</th>
<th data-options="field:'email',width:200,sortable:true">Email </th>
<th data-options="field:'name',width:100,sortable:true">Role</th>
<th data-options="field:'tlp',width:100,sortable:true">Phone Number</th>
</tr>
</thead>
</table>
<div id="toolbaruser">
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newuser()">New user</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyuser()">Remove user</a>
</div>
<div id="dlguser" class="easyui-dialog" style="width:100%;height:100%;padding:10px 20px" closed="true" buttons="#dlguser-buttons">
   <form id="fmuser" method="post" enctype="multipart/form-data" novalidate>
	<div class="easyui-layout" style="width:100%;height:600px;"  data-options="">
		<!--WEST-->
        <div data-options="region:'west'" style="width:50%;height:150px" border="false">
		<table cellpadding="5">
		<tr>
		<td>Username *</td>
		<td>: <input name="username" class="woi" style="width:250px;"  data-options="prompt:'Username'" required="true"><span id="kode_result"></span></td>
		</tr>
		
		<tr>
		<td>Password *</td>
		<td>: <input name="re_password" type="password" class="easyui-textbox" style="width:250px;"  data-options="prompt:'password'" required="true"></td>
		</tr>
		<tr>
		<td>Email </td>
		<td>: <input name="email" class="easyui-textbox" style="width:250px;"  data-options="prompt:'Email'" ></td>
		</tr>
		<tr>
		<td>Status *</td><td>: <select name="aktif" data-options="prompt:'Choose'" style="width:250px;"  class="easyui-combobox" required="true">
		<option selected></option>
		<option value="1">Active</option>
		<option value="0">InActive</option>
		<tr>
		<td>Phone Number</td>
		<td>: <input name="tlp" style="width:250px;"  class="easyui-textbox" data-options="prompt:'Phone Number'" ></td>
		</tr>
		<tr>
			<td>Gender *</td><td>: <select name="jk" data-options="prompt:'Choose Sex'" style="width:250px;" class="easyui-combobox" required="true">
			<option selected></option>
			<option value="Male">Male</option>
			<option value="Female">Female</option>
			</select></td></tr>
		<tr>
		<td>Jabatan</td>
		<td>: <input name="jabatan" style="width:250px;"  class="easyui-textbox" data-options="prompt:'Jabatan'" ></td>
		</tr>
			

		</table>
		</div>
		<!--END OF WEST-->
		
		<!--EAST-->
		
        <div data-options="region:'east'" style="width:50%;height:150px" border="false">
		<table cellpadding="5">
			
		<td>Role *</td>
		<td>:  <input id="level" required="true" style="width:250px;" name="id_level"/>
		</td>
		</tr> 	
		<tr>
		<td>Full Name *</td>
		<td>: <input name="nama_lengkap" data-options="prompt:'Full Name'" style="width:250px;"  class="easyui-textbox" required="true"></td>
		</tr>
		<tr>
			<td>Picture</td>
			<td>: <input name="file" type="file" style="width:140px;"  ></input></td>
			</tr>
			<tr>
			<td>Note</td>
			<td>: <input name="noted" data-options="prompt:'Note'" data-options="multiline:true" style="width:250px;height:60px" style="width:250px;"  class="easyui-textbox" ></td>
			</tr>
			<tr>
		<td>Work Location</td>
		<td>: <input name="work_location" data-options="prompt:'Work Location'" style="width:250px;"  class="easyui-textbox" ></td>
		</tr>		
			<tr>
		</table>
		</div>
		</form>
		<!--END OF EAST-->
		
        
        </div>
</div>
<div id="dlguser-buttons">
<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savuser()" style="width:90px">Save</a>
<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlguser').dialog('close')" style="width:90px">Close</a>
</div>

<script>
var url;
var urlrole;

function expandAllgrid(){
	$('#tbd').treegrid('expandAll');
			
}
function collapseAllgrid(){
	$('#tbd').treegrid('collapseAll');
			
}

    $('#dguser').datagrid({
    view: detailview,
    detailFormatter:function(index,row){
    return '<div class="ddv" style="padding:5px 0"></div>';
    },
	onDblClickRow:function(index,row){
				edituser(index);
			},
    onExpandRow: function(index,row){
    var ddv = $(this).datagrid('getRowDetail',index).find('div.ddv');
    ddv.panel({
    border:false,
    cache:false,
    href:'modul/user/getdetail.php?id='+row.id_a,
    onLoad:function(){
    $('#dguser').datagrid('fixDetailRowHeight',index);
    }
    });
    $('#dguser').datagrid('fixDetailRowHeight',index);
    }
    });
function check(){
	$(".woi").keyup(function (e) {

		//removes spaces from username
		//$(this).val($(this).val().replace(/\s/g, ''));

		var username = $(this).val();
		if(username.length < 2){$("#kode_result").html('');return;}

		if(username.length >= 2){
			$("#kode_result").html('<img src="images/ajax-loader.gif" />');
			$.post('modul/user/checking.php?act=kode', {'username':username}, function(data) {
				$("#kode_result").html(data);
			});
		}
	});
}
function newuser(){
$('#dlguser').dialog('open').dialog('setTitle','New  user');
check();
$('#level').combobox({
	url:'modul/user/useraction.php?act=level',
	valueField:'id_level',
	textField:'name',
});
$('#fmuser').form('clear');
$('#south').hide();
url = 'modul/user/useraction.php?act=create';
}
function edituser(){
var row = $('#dguser').datagrid('getSelected');
	check();
$('#level').combobox({
	url:'modul/user/useraction.php?act=level',
	valueField:'id_level',
	textField:'name',
});
if (row){
$('#dlguser').dialog('open').dialog('setTitle','Edit user');

$('#fmuser').form('load',row);

	
url = 'modul/user/useraction.php?act=update&id='+row.id_a;
}
}
 function savuser(){
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
			$('#fmuser').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlguser').dialog('close');		// close the dialog
						$('#dguser').datagrid('reload');	// reload the user data
					}
				}
			});
			}
			}
			);
}

function destroyuser(){
var row = $('#dguser').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this user?',function(r){
if (r){
$.post('modul/user/useraction.php?act=delete',{id:row.id_a},function(result){
if (result.success){
$('#dguser').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}



function insrole(){
	//var random = Math.floor(Math.random()*9999);
	var row = $('#tbd').treegrid('getSelected');
	var rowx = $('#dguser').datagrid('getSelected');
	$('#dlgrole').dialog('open').dialog('setTitle','New Access Control username '+rowx.username +' - '+ row.id_master_proker +'-'+ row.id);
	$('#fmrole').form('load',row);
	$('#edt').linkbutton('disable');
	console.log(row.id);
	if(row.id_master_proker == 0){
	urlrole = 'modul/user/useraction.php?act=createrole&id='+row.id+'&id_a='+rowx.id_a;
	}
	else {
	urlrole = 'modul/user/useraction.php?act=updaterole&id='+row.id_master_proker;
	}
}

function saverole(){
//to get the loaded data
	$.messager.confirm('Confirm','Are you sure you want to Save this ?',function(r){
	if (r){
		$('#fmrole').form('submit',{
			url: urlrole,
			onSubmit: function(){
			return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.errorMsg){
					$('.ccd').textbox('clear');
					$.messager.show({
					title: 'Error',
					msg: result.errorMsg
					});
				} else {
					$.messager.show({
					title: 'Success',
					msg: result.success
					});
				$('#dlgrole').dialog('close');		// close the dialog
				$('#tbd').treegrid('reload');	// reload the user data
				}
			}
		});}
	});
}
</script>
 <style type="text/css">
  .f1{
width:200px;
}
#fmuser{
margin:0;
padding:10px 30px;
}
.ftitle{
font-size:14px;
font-weight:bold;
padding:5px 0;
margin-bottom:10px;
border-bottom:1px solid #ccc;
}
.fitem{
margin-bottom:5px;
}
.fitem label{
display:inline-block;
width:80px;
}
.fitem input{
width:160px;
}
</style>

 <script>
function submitForm(){
			$('#fmuser').form('submit',{
				
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
	
				$.messager.alert('Info','info');

					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
							// close the dialog
						$('#fmuser').load('reload' );	// reload the user data
					}
				}
			});
}
function clearForm(){
$('#fmuser').form('clear');
}
</script>

<?php
break;
} ?>