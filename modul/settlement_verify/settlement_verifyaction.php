<?php
session_start();
if (empty($_SESSION['username']) AND empty($_SESSION['id_a'])){
	echo json_encode(array('errorMsg'=>'Some errors occured.'));
}
else{
	include '../../config/koneksi.php';
	include '../../control/class.php';
	include '../../control/api.php';
	$company = new Master();
	$api = new API();
	$id_period = $company->GetYear('id_period');

	 if ($_GET['act'] == 'getlist'){
		 $page = isset($_POST['page']) ? intval($_POST['page']) : 0;
		 $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		 $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'code_number';
		 $order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
		 $offset = ($page-1)*$rows;
		 $result = array();
		 $payment_no = $_REQUEST['code_number'];
		 $substr = substr($payment_no,0,1);
		 if($substr == 'P'){
		 $rs = mysql_query("select count(*) from payment_master_group where payment_no='$payment_no' and a.check_date is not null ");
		 $row = mysql_fetch_row($rs);
		 $result["total"] = $row[0];
		 $rs = mysql_query("select a.id_payment_master_group as id,a.ca_no as code_number,a.rek_no,c.bank_name,a.status_paid,a.paid,a.settlement,
							a.total_settlement,a.cash_advance,a.balance from payment_master_group a left join bank c on a.id_bank = c.idbank 
							where a.payment_no='$payment_no' and a.check_date is not null order by $sort $order limit $offset,$rows ");
		 }
		 else {
			 $rs = mysql_query("select count(*) from reimbustment_master where reim_no='$payment_no' ");
			 $row = mysql_fetch_row($rs);
			 $result["total"] = $row[0];
			 $rs = mysql_query("select a.id_reim_master as id,a.reim_no as code_number,a.rek_no,c.bank_name,a.status_paid,sum(b.paid) as paid, sum(b.settlement) as settlement,
							sum(b.total_settlement) as total_settlement,sum(b.ca_req) as cash_advance,sum(b.balance) as balance from reimbushment_master a left join bank c on a.id_bank = c.idbank 
							left join reimbushment_ b on a.id_reim_master = b.id_reim_master where a.reim_no='$payment_no' group by a.id_reim_master ");
		 }

		$items = array();
		while($row = mysql_fetch_object($rs)){
			array_push($items, $row);
		}
		$result= $items;

		echo json_encode($result);
	}
	else if ($_GET['act'] == 'getdetail'){
		$page = isset($_POST['page']) ? intval($_POST['page']) : 0;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'cost_name';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
		$offset = ($page-1)*$rows;
		$result = array();
		$id = $_REQUEST['id'];
		$code_number = $_REQUEST['code_number'];
		$substr = substr($code_number, 0, 1);
		if($substr == 'C') {
			$rs = mysql_query("select count(*) from payment_group_detail where id_payment_master_group='$id'  ");
			$row = mysql_fetch_row($rs);
			$result["total"] = $row[0];
			$rs = mysql_query("select a.id_payment_group_detail as id_det,a.id_payment_master_group as id,a.cost_name,a.total,a.paid,a.settlement,a.total_settlement,a.balance,
								a.remarks,b.payment_no as code_number from payment_group_detail a left join 
								payment_master_group b on a.id_payment_master_group = b.id_payment_master_group where a.id_payment_master_group='$id'  order by $sort $order limit $offset,$rows ");
		}
		else {
			$rs = mysql_query("select count(*) from reimbushment_ where id_reim_master='$id'  ");
			$row = mysql_fetch_row($rs);
			$result["total"] = $row[0];
			$rs = mysql_query("select b.reim_no as code_number,a.id_reim as id_det ,a.id_reim_master as id,a.cost_name,a.total,a.paid,a.settlement,a.total_settlement,a.balance,a.remarks from reimbushment_ a
								left join reimbushment_master b on a.id_reim_master = b.id_reim_master
								where a.id_reim_master='$id'  order by $sort $order limit $offset,$rows ");
		}
		$items = array();
		while($row = mysql_fetch_object($rs)){
			array_push($items, $row);
		}
		$result= $items;

		echo json_encode($result);
	}
	else if ($_GET['act'] == 'getlistpayment'){

		$rs = mysql_query("select payment_no as code_number from payment_master_group where settlement!='0' group by payment_no 
							UNION 
							select reim_no as code_number from reimbushment_master ");

		$items = array();
		while($row = mysql_fetch_object($rs)){
			array_push($items, $row);
		}
		$result= $items;

		echo json_encode($result);
	}
	else if ($_GET['act'] == 'updatedetail'){
		$id = intval($_REQUEST['id_det']);
		$id_payment_master_group = intval($_REQUEST['id_det']);
		$settlement = $_REQUEST['settlement'];
		$remarks = $_REQUEST['remarks'];

		$code_number = $_REQUEST['code_number'];
		$substr = substr($code_number, 0, 1);
		if($substr == 'P') {
			$sql = "update payment_group_detail set settlement='$settlement' , remarks='$remarks' where id_payment_group_detail='$id'";


			$result = @mysql_query($sql);
			if ($result) {
				mysql_query("update payment_master_group set settlement=settlement+$settlement where id_payment_master_group='$id_payment_master_group'");
				echo json_encode(array('success' => $_REQUEST['id_det']));
			} else {
				echo json_encode(array('isError' => true, 'errorMsg' => mysql_error()));
			}
		}
		else {
			$sql = "update reimbushment_ set settlement='$settlement' , remarks='$remarks' where id_reim='$id'";


			$result = @mysql_query($sql);
			if ($result) {
//				mysql_query("update reimbushment_master set settlement=settlement+$settlement where id_payment_master_group='$id_payment_master_group'");
				echo json_encode(array('success' => $_REQUEST['code_number']));
			} else {
				echo json_encode(array('isError' => true, 'errorMsg' => mysql_error()));
			}
		}
	}

	else if($_GET['act'] == 'confirmsettle'){
		$id= $_REQUEST['payment_no'];

		$sql = mysql_query("select * from payment_master_group where settlement = '0' and payment_no='$id'");
		$cek = mysql_num_rows($sql);
		$msg = 'kosong';
		if($cek > 0){
			echo json_encode(array('isError' => true,'errorMsg'=>'Settlement must not empty'));
		}
		else {
		echo json_encode(array('success'=>'Success'));
		}
	}

}