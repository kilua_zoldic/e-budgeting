<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 12/18/16
 * Time: 3:47 PM
 */
?>

<form  id="fmcasettle" method="post" novalidate>
        <div class="easyui-layout" style="width:100%;height:700px;">
            <div data-options="region:'north'" style="height:100px;width: 100%;border:0">
                <div data-options="region:'west',split:true" title="West" style="width:100%;">
                    <table cellpadding="5" style="float:left;">
                        <tr>
                            <td>Payment No</td> <td>: <input id="payment_no"  required="true" style="width:250px;" data-options="formatter: formatItem,prompt:'Payment No.'" name="payment_no"/></td>
                        </tr>

                    </table>
                </div>
            </div>
            <div data-options="region:'south',split:false,border:false" style="height:300px;">
                <table cellpadding="5" style="float:left;">

                    <tr><td></td>
                        <td><a href="javascript:void(0)" id="confirm" class="easyui-linkbutton" iconCls="icon-ok" onclick="confirmca()" style="width:90px">Confirm</a>
                            <a id="printcash" href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="printcash()" style="width:90px">Print</a>
                        </td>
                    </tr>
                </table>
            </div>
        <div data-options="region:'center',border:false,plain:false" style="height:300px;" >
                <table id="detkeg"
                       data-options="rownumbers:true,singleSelect:true,fit:true,fitColumns:true"
                       idField="id" sortName="code_number" pagination="true"
                       rownumbers="true" pageSize="50"  maximizable="true"
                       style="width:100%;height:300px;">
                    <thead>
                    <tr>
                        <th field="ck" checkbox="true"></th>
                        <th data-options="field:'id'," hidden="true" hwidth="80">ID</th>
                        <th data-options="field:'code_number',sortable:true" width="150">Ca / Reimbushment No</th>
                        <th data-options="field:'rek_no',sortable:true" width="150">No Rek</th>
                        <th data-options="field:'status_paid',formatter:payment_method,sortable:true" width="150">Payment Method</th>
                        <th data-options="field:'bank_name',sortable:true" width="150">Bank</th>
                        <th data-options="field:'paid',align:'right',formatter:formatPrice,sortable:true" width="130">Paid</th>
                        <th data-options="field:'settlement',align:'right',formatter:formatPrice,sortable:true" width="130">Settlement</th>
                        <th data-options="field:'total_settlement',align:'right',formatter:formatPrice,sortable:true" width="130">Total Settlement</th>
                        <th data-options="field:'cash_advance',align:'right',formatter:formatPrice,sortable:true" width="130">CA</th>
                        <th data-options="field:'balance',align:'right',formatter:formatPrice,sortable:true" width="150">Balance</th>
                       </tr>
                    </thead>
                </table>
<!--                    </div>-->
<!--                </div>-->
            </div>

        </div>
</form>

<div id="dlgsettle" class="easyui-dialog" style="width:100%;height:70%;padding:10px 20px"  resizable="true" maximizable="true" closed="true"  buttons="#dlgsettle-buttons">
    <table id="setlelist"
           data-options="singleSelect:true,fit:true,fitColumns:true"
           idField="id_det" sortName="cost_name" pagination="true"
           rownumbers="true" pageSize="50"  maximizable="true"
           style="width:100%;height:300px;">
        <thead>
        <tr>
        <th data-options="field:'id_det'" hidden="true" width="80">ID</th>
        <th data-options="field:'cost_name'" width="250">Nama Kegiatan</th>
        <th data-options="field:'total',align:'right',formatter:formatPrice" width="130">Anggaran</th>
        <th data-options="field:'paid',align:'right',formatter:formatPrice" width="130">Paid</th>
        <th data-options="field:'settlement',align:'right',formatter:formatPrice,editor:{type:'numberbox',options:{precision:2}}" width="130">Settlement</th>
        <th data-options="field:'total_settlement',align:'right',formatter:formatPrice" width="130">YTD Settlement</th>
        <th data-options="field:'balance',align:'right',formatter:formatPrice" width="130">Balance</th>
        <th data-options="field:'remarks',align:'center',editor:{type:'text'}" width="280">Remarks</th>
        </tr>
        </thead>
    </table>
</div>
<div id="dlgsettle-buttons">
     <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="closesettle();" style="width:90px">Close</a>
</div>

<script type="text/javascript" src="modul/settlement_verify/datagrid-cellediting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#printcash').hide();
        $('#printtrf').hide();
        $('#printcash').linkbutton('disable');
        $('#printtrf').linkbutton('disable');
        $('.hide').hide();
        $('.trf').hide();
    });
</script>
<script>

    $('#detkeg').datagrid({
        url:'',
    });
    function closesettle(){
        $('#dlgsettle').dialog('close');
        $('#setlelist').datagrid({
            url:'',
        });
    }
    function modal(index,row){
        var row = $('#detkeg').datagrid('getSelected');
        if(row) {
            $('#dlgsettle').dialog('open').dialog('setTitle', 'List Cash Advance'+row.id);
            $('#setlelist').edatagrid({
                url: 'modul/settlement_verify/settlement_verifyaction.php?act=getdetail&id=' + row.id+'&code_number='+row.code_number,
                updateUrl: 'modul/settlement_verify/settlement_verifyaction.php?act=updatedetail',
                onSuccess: function (index, row) {
                    $.messager.show({
                        title: 'Info',
                        msg: row.success
                    });
                    $('#setlelist').edatagrid('reload');
                    $('#detkeg').datagrid('reload');
                },
                onError: function (index, row) {
                    $.messager.show({
                        title: 'Info',
                        msg: row.errorMsg
                    });
                }
            }).edatagrid('enableCellEditing').edatagrid('gotoCell', {
                index: 0,
                field: 'id_payment_group_detail',
            });
        }
    }
    $('#payment_no').combobox({
        url: 'modul/settlement_verify/settlement_verifyaction.php?act=getlistpayment',
        valueField: 'code_number',
        textField: 'code_number',
        onSelect: function(rec){
            $('#detkeg').datagrid({
                url:'modul/settlement_verify/settlement_verifyaction.php?act=getlist&code_number='+rec.code_number,
                pageList: [10,20,50,100,150,200,250,300,350,400,450,500],
				onDblClickRow:function(index,row){
					//editproker(index);
                    modal(index,row);
				},
                emptyMsg: 'No Records Found',
            });
        }

    });

    function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.code_number + '</span>';
        return s;
    }

    function confirmca(){
            $.messager.confirm('Confirm', 'Are you sure you want to Confirm this ?', function (r) {
                if (r) {
                    $('#printcash').show();
                    $('#printcash').linkbutton('enable');

                    $('#fmcasettle').form('submit',{
                        url : 'modul/settlement_verify/settlement_verifyaction.php?act=confirmsettle',
                        onSubmit: function(){
                            return $('#fmcasettle').form('validate');
                        },
                        success: function(result){
                            var result = eval('('+result+')');
                            if (result.errorMsg){
                                $.messager.show({
                                    title: 'Error',
                                    msg: result.errorMsg
                                });
                            } else {
                                //$('#dlgxx').dialog('close');		// close the dialog
                                $('#confirm').hide();
                                $('#confirm').linkbutton('disable');
                                $.messager.show({
                                    title: 'Success',
                                    msg: result.success
                                });
                            }
                        }
                    });
                }

            });
        }

    function printcash(){
        url = "modul/settlement_verify/settlement_verifiy_report.php?payment_no="+$('#payment_no').combobox('getValue');
        window.open(url);
    }

    function clearca(){
        $('#detkeg').datagrid({
            url:'',
        });
        $('#refno').combobox('setValue','');
        $('#bp').textbox('setValue','');
        $('#bd').textbox('setValue','');
        $('#nk').textbox('setValue','');
        $('#cano').textbox('setValue','');
        $('#printca').linkbutton('disable');
    }
</script>
