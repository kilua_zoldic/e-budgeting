	/*	function test(){
	   $("#formID").validationEngine();

        $("#test").live('click', function () {
            var valid = $("#formID").validationEngine('validate');
            var vars = $("#formID").serialize();

            if (valid == true) {

              	jQuery("#myModalform").modal('show');

            } else {
					jQuery("#myModalform").modal('hide');
                $("#formID").validationEngine();
            }
        });
	}	
	*/
	function editcon(){
		jQuery.noConflict()(function($){
		var row = $('#docpers').datagrid('getSelected');
		if (row){	
		$('#dlgcon').dialog('open').dialog('setTitle','Edit Contact');
		$('#fmcon').form('clear');
		url = 'control/regisdoc.php?act=editdoc';
		}
		});
		}
	function delcon(){
		jQuery.noConflict()(function($){
		var row = $('#cperson').datagrid('getSelected');
			if (row){
					$.messager.confirm('Confirm','Are you sure you want to Delete this Contact?',function(r){
					if (r){
					$.post('control/regisdoc.php?act=deletedoc',{id:row.id},function(result){
					if (result.success){
					$('#cperson').datagrid('reload'); // reload the user data
					} else {
					$.messager.show({ // show error message
					title: 'Error',
					msg: result.errorMsg
					});
					}
					},'json');
					}
					});
					}
		});	
	}
	function savecon(){
		jQuery.noConflict()(function($){
		 $.messager.confirm('Save Contact', 'Are you confirm this?', function(r){
			if (r){
			$('#fmcon').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg,
							 style:{
							right:'',
							bottom:''
						}
						});
					} else {
						$('#dlgcon').dialog('close');		// close the dialog
						$('#cperson').datagrid('reload');	// reload the user data
					}
				}
			});
			 }
            });	
	});		
}	
	
	function formatstatus_type(val,row){
   
	if (row.status_doc == '1'){
    return '<span>Temporary</span>';
	}
	else if (row.status_doc == '2'){
    return '<span>Valid</span>';
	}
	}

	
	
	var initial = [
		    {initial:'Mr',initialname:'Mr'},
		    {initial:'Ms',initialname:'Ms'}
		];	
	function reloadcon(){
	jQuery.noConflict()(function($){
	$('#cperson').datagrid('reload');
	});
	}
	
	function closecon(){
	jQuery.noConflict()(function($){
	$('#dlgcon').dialog('close');
	});
	}
	function reloaddoc(){
	jQuery.noConflict()(function($){	
	$('#docpers').datagrid('reload');
	});
	}
	function reload(){
	jQuery.noConflict()(function($){
	$('#cperson').edatagrid('reload');
	});
	}	
	jQuery.noConflict()(function($){
	
      // Create the dropdown base
      $("<select />").appendTo("nav");
      
      // Create default option "Go to..."
      $("<option />", {
         "selected": "selected",
         "value"   : "",
         "text"    : "Please choose page"
      }).appendTo("nav select");
      
      // Populate dropdown with menu items
      $("nav a").each(function() {
       var el = $(this);
       $("<option />", {
           "value"   : el.attr("href"),
           "text"    : el.text()
       }).appendTo("nav select");
      });
      
	   // To make dropdown actually work
	   // To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
      $("nav select").change(function() {
        window.location = $(this).find("option:selected").val();
      });
	 
	 });
	jQuery.noConflict()(function($){
		
	$(document).ready(function()
		{
			
			$("#country").change(function()
				{
				var id=$(this).val();
				var dataString = 'id='+ id;
				
				$.ajax
				({
				type: "POST",
				url: "provincy.php",
				data: dataString,
				cache: false,
				success: function(html)
				{
				$(".provincy").html(html);
				}
				});

				});
		$(".local").hide();				
		$(".localiup").hide();				
		$(".interlocal").hide();				
		$(".inter").hide();				
		$("#foreign").change(function(){	
		var chat = $("#foreign").val();
		if (chat =='1'){
		$(".local").show();		
		$(".interlocal").show();		
		$(".localiup").show();		
		}
		else if (chat =='2'){
		$(".local").show();		
		$(".interlocal").show();		
		$(".localiup").hide();			
		}
		else if (chat =='3'){
		$(".interlocal").show();	
		$(".inter").show();	
		$(".local").hide();				
		$(".localiup").hide();	
		}
		else if (chat =='4'){
		$(".interlocal").show();	
		$(".inter").show();	
		$(".local").hide();				
		$(".localiup").hide();	
		}
		else { 
		$(".local").hide();				
		$(".localiup").hide();
	$(".interlocal").hide();				
		$(".inter").hide();			}
		});		
				});
			

	function tampilTabel()
	{
		if(status=="")
		{
			$('#tabel').slideDown();
			status="1";
		}
		else
		{
			$('#tabel').slideUp();
			status="";
		}
	}		
			});
	jQuery.noConflict()(function($){
		$('.chosen-select').chosen();	
		
		});
		
		jQuery.noConflict()(function($){
	
		$("#captcha").attr('maxlength','4');	
		$('#captcha').on('keypress keydown keyup', function(){
		  $(this).val($(this).val().replace(/([^\d]+)/, '')); 
		});
		});		
//upload document
$(function(){
		 $('#photoimg').on('change', function(){ 
			$("#preview").html();
			$("#preview").html('<img src="loader.gif" alt="Uploading...."/>');
			jQuery("#imageform").ajaxForm({
				target: '#preview'
			}).submit();
			});
			
			$('#trade').on('change', function(){ 
			$("#previewtrade").html();
			//$("#trade").hide();
			$("#previewtrade").html('<img src="loader.gif" alt="Uploading...."/>');
			jQuery("#tradeform").ajaxForm({
				target: '#previewtrade'
			}).submit();
			});
			
			$('#akper').on('change', function(){ 
			$("#previewakper").html();
			//$("#akper").hide();
			$("#previewakper").html('<img src="loader.gif" alt="Uploading...."/>');
			jQuery("#akperform").ajaxForm({
				target: '#previewakper'
			}).submit();
			});
			
			$('#siup').on('change', function(){ 
			$("#previewsiup").html();
			//$("#siup").hide();
			$("#previewsiup").html('<img src="loader.gif" alt="Uploading...."/>');
			jQuery("#siupform").ajaxForm({
				target: '#previewsiup'
			}).submit();
			});
			
			$('#npwp').on('change', function(){ 
			$("#previewnpwp").html();
			//$("#npwp").hide();
			$("#previewnpwp").html('<img src="loader.gif" alt="Uploading...."/>');
			jQuery("#npwpform").ajaxForm({
				target: '#previewnpwp'
			}).submit();
			});
			
			$('#tdp').on('change', function(){ 
			$("#previewtdp").html();
			//$("#tdp").hide();
			$("#previewtdp").html('<img src="loader.gif" alt="Uploading...."/>');
			jQuery("#tdpform").ajaxForm({
				target: '#previewtdp'
			}).submit();
			});
			
			$('#iup').on('change', function(){ 
			$("#previewiup").html('ok');
			//$("#iup").hide();
			$("#previewiup").html('<img src="loader.gif" alt="Uploading...."/>');
			jQuery("#iupform").ajaxForm({
				target: '#previewiup'
			}).submit();
			});
        }); 		