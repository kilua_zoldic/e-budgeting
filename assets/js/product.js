$(function(){
			//var dg = $('#dgproduct').datagrid();
			$('#dgproduct').datagrid({
			url:'modul/product/productaction.php?act=list',
			pagination:true,
			maximizable:true,
			singleSelect:true,
			fitColumns:true,
			rownumbers:true,
			collapsible:true,
			closable:true,
			idField:'id_product',
			sortName:'id_sort',
			sortOrder:'asc',
			columns :[[
				{field:'id_product',title:'id Product',width:100,hidden:true},
				{field:'code_product',title:'Product Code',width:100},
				{field:'product_name',title:'Product Name',width:100,formatter:tooltipproduct},
				//{field:'price',title:'Price',width:100,formatter:priceproduct},
				//{field:'date',title:'Date',width:100},
				{field:'aktif_product',title:'Status',width:100,formatter:stock_product}
					]],
			onBeforeEdit:function(index,row){
				var opts = $(this).edatagrid('options');
				var tr = opts.finder.getTr(this, index);
				tr.find('.easyui-tooltip').tooltip('destroy');
			},
			onAfterEdit:function(index,row){
				createTooltip();
			},
			onCancelEdit:function(index,row){
				createTooltip();
			},
			onLoadSuccess:function(){
				$(this).datagrid('enableDnd');
				createTooltip();
			},onDrop:function(destRow,sourceRow,point){
               // $('#dgm').edatagrid('selectAll');  
				var sort = destRow.sort;    
               var id_sort = sourceRow.id_sort;             
               var id_product = sourceRow.id_product;             
                       
                
						  $.post('modul/product/productaction.php?act=sortdrag',
						  {
			sort:sort,
			id_sort:id_sort,
			id_product:id_product,
			point:point,
				
				}), 
               
             
                $('#dgproduct').datagrid('unselectAll');
                $('#dgproduct').datagrid('reload');
               
             }
			
			});
			/*
			dg.datagrid('enableFilter', [{
			field:'date',
			type:'datebox',
			
			}] );
			
			  dg.datagrid('doFilter'); */
		});
		
function priceproduct(val,row){
	var x = accounting.formatMoney(val);
		return '<span style="float:right">'+x+'</span>';
}	
function stock_product(stock,row){
	if (stock == '1'){
		return '<span style="color:green;font-weight: bold;">Available</span>';
		} 

	else {
		return '<span style="color:red;font-weight: bold;">Not Available</span>';
	}
}
function aktif_product(aktif_product,row){
	if (aktif_product == '1'){
		return '<span style="color:green;font-weight: bold;">Active</span>';
		} 
	else {
		return '<span style="color:red;font-weight: bold;">InActive</span>';
	}
}	
function createTooltip(){
	
    $('#dgproduct').datagrid('getPanel').find('.easyui-tooltip').each(function(){
        var index = parseInt($(this).attr('data-p1'));
        $(this).tooltip({
            content: $('<div ></div>'),
			showEvent: 'click',
			position: 'right',
			onUpdate: function(cc){
                var row = $('#dgproduct').datagrid('getRows')[index];
                var content = '<div ><center><img style="height:120px;width:120px;" src="images/'+row.picture+'"></center>'+row.description+'</div>';
                cc.panel({
                    width:500,
					border: false,
                    content:content
                });
            },
            
onShow: function(){

var t = $(this);
t.tooltip('tip').unbind().bind('mouseenter', function(){
t.tooltip('show');
}).bind('mouseleave', function(){
t.tooltip('hide');
});
}
        });
	
    });
}	


function tooltipproduct(product_name,row,index){
	return '<span data-p1='+index+' class="easyui-tooltip" style="margin-top:180px;" >' + product_name + '</span>';
	
}	
		
var url;

 function savdep(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// close the dialog
						$('#dgproduct').datagrid('reload');	// reload the user data
					}
				}
			});
} 
function saveproedit(){
			$('#fmedit').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlgedit').dialog('close');		// close the dialog
						$('#dgproduct').datagrid('reload');	// reload the user data
					}
				}
			});
}


function newprod(){
	
CKEDITOR.instances.description.setData('');
$('#dlg').dialog('open').dialog('setTitle','New Product');
$('#fm').form('clear');
url = 'modul/product/productaction.php?act=create';
	$('#sort').combobox({
    url:'modul/product/productaction.php?act=sort',
    valueField:'id_sort',
	method:'get',
    textField:'sort' });
}
function editprod(){
var row = $('#dgproduct').datagrid('getSelected');
$('#fmedit').form('clear');
if (row){
$('#dlgedit').dialog('open').dialog('setTitle','Edit Product');
$('#fmedit').form('load',row);
url = 'modul/product/productaction.php?act=update&id='+row.id_product;
CKEDITOR.instances.descriptionx.setData(row.description);
$('#sort').combobox({
    url:'modul/product/productaction.php?act=editsort',
    valueField:'id_sort',
	method:'get',
    textField:'sort',
value:+row.sort	});
}
}

function destroyprod(){
var row = $('#dgproduct').datagrid('getSelected');
if (row){
$.messager.confirm('Confirm','Are you sure you want to Delete this Product?',function(r){
if (r){
$.post('modul/product/productaction.php?act=delete',{id:row.id_product},function(result){
if (result.success){
$('#dgproduct').datagrid('reload'); // reload the user data
} else {
$.messager.show({ // show error message
title: 'Error',
msg: result.errorMsg
});
}
},'json');
}
});
}
}