var ins = 'disable';
var edit = 'disable';
var svt = 'disable';
var delet = 'disable';

function editfield(){
	$('.db').textbox('enable');
	$('.dbc').combobox('enable');
	$('.sv').linkbutton('enable');
	$('.newchild').linkbutton('enable');
	$('.delchild').linkbutton('enable');
}

var myview = $.extend({},$.fn.datagrid.defaults.view,{
	onAfterRender:function(target){
		$.fn.datagrid.defaults.view.onAfterRender.call(this,target);
		var opts = $(target).datagrid('options');
		var vc = $(target).datagrid('getPanel').children('div.datagrid-view');
		vc.children('div.datagrid-empty').remove();
		if (!$(target).datagrid('getRows').length){
			var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
			d.css({
				position:'absolute',
				left:0,
				top:50,
				width:'100%',
				textAlign:'center'
			});
		}
	}
});
function imgrol(val,row){
	if (val == '1' ){
		if(row.parentId == 0){
		return '';
		}
		else {
		return '<span><img src="images/available.png" alt="Active" /></span>';
		} 
		} 
	else {
		if(row.parentId == 0){
		return '';
		}
		else {
		return '<span><img src="images/not-available.png" alt="InActive" /></span>';
		}
		}
	
}
function status_tbd(val,row){
	if (val == '1' ){
		return '<span>Active</span>';
		} 
	else {
		return '<span></span>';
		}
	
}
function status_proker(val,row){
	if (val == '1' ){
		return '<span>Active</span>';
		} 
	else  {
		return '<span>InActive</span>';
		}
	
}
function jenis(val,row){
	if (val == '1' ){
		return '<span>Rutin</span>';
		} 
	else if (val == '2' ){
		return '<span>Non Rutin</span>';
		}
	else {
		return '<span></span>';
	}
	
}
function payment_method(val,row){
	if (val == '1' ){
		return '<span>Cash</span>';
	}
	else if(val == '2' ){
		return '<span>Transfer</span>';
	}
	else {
		return '<span></span>';
	}

}
function status_master_menu_detail(val,row){
	if (val == '1' ){
		return '<span>Active</span>';
		} 
	else {
		return '<span>InActive</span>';
		}
	
}
var status_master_menu = [
		    {status:'1',status_text:'Active',selected: true},
		    {status:'0',status_text:'InActive'}
		];	
function status_sub(status_sub_menu,row,index){
	if(row.status_sub_menu == '1'){
		return '<span> Active</span>';
	}
	else {
	 return '<span> InActive</span>';
	}
}
	
var cmenu;
function createColumnMenu(dg){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $(dg).datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $(dg).datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
            });
            var fields = $(dg).datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $(dg).datagrid('getColumnOption', field);
                cmenu.menu('appendItem', {
                    text: col.title,
                    name: field,
                    iconCls: 'icon-ok'
                });
            }
        }
function formatPrice(val,row){
	var x = accounting.formatMoney(val);
		return '<span style="float:right">'+x+'</span>';
}
function call_combobox(id_x,url_list){
		
	$(id_x).combobox("textbox").bind("click", function(e){
	
	$(id_x).combobox('reload',url_list);
	});	
}

function call_combogrid(id_x,url_list){
		
	$(id_x).combogrid("textbox").bind("click", function(e){
	
	$(id_x).combogrid('reload',url_list);
	});	
}



function tampilkan(halaman){
   
    $.ajax({
        
        url      : 'http://localhost/ebudgeting/content.php',
        type     : 'POST',
        dataType : 'html',
        data     : 'content='+halaman,
        success  : function(jawaban){
            $('#content').html(jawaban);
        },
    })
	 
} 

function addTab(title, url){  
    if ($('#tt').tabs('exists', title)){  
        $('#tt').tabs('select', title);
    } else {  
        var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:99%;"></iframe>';
        $('#tt').tabs('add',{  
            title:title,  
            content:content,
			fit:true,
			closable:true	
        });  
    } 

}
Date.prototype.yyyymmdd = function() {
	var mm = this.getMonth() + 1; // getMonth() is zero-based
	var dd = this.getDate();

	return [this.getFullYear(),
		(mm>9 ? '' : '0') + mm,
		(dd>9 ? '' : '0') + dd
	].join('');
};

var date = new Date();

$(function(){
	$.fn.datebox.defaults.formatter=function(date){
		if (!date){return '';}
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	},
		$.fn.datebox.defaults.parser=function(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
	//$(".chosen").chosen();
	$("#app").hide();
	$('#menu_tree').tree({
		animate:true,
		 onLoadSuccess: function(node,data){
            $('#menu_tree').tree('collapseAll')
        }
	});
});
function expandAll(idgrid){
	$(idgrid).tree('expandAll');
			
}
function collapseAll(idgrid){
	$(idgrid).tree('collapseAll');
			
}
