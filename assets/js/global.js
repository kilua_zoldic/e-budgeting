var url;
var d = new Date();
var y =  d.getFullYear();
function call_sso(id_x,url_list){
		
	$(id_x).combobox("textbox").bind("click", function(e){
	
	$(id_x).combobox('reload',url_list);
	});	
}



$(function(){

	
	 $.fn.datebox.defaults.formatter=function(date){
        if (!date){return '';}
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
    },
   $.fn.datebox.defaults.parser=function(s){
        if (!s) return new Date();
        var ss = (s.split('-'));
        var y = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        var d = parseInt(ss[2],10);
        if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
            return new Date(y,m-1,d);
        } else {
            return new Date();
        }
    }
	

	
	/*	
	$('#id_salesperson').combobox("textbox").bind("click", function(e){
	
	$('#id_salesperson').combobox('reload','control/view.php?act=manager');

	});	
	$('#compcode').combobox("textbox").bind("click", function(e){
	
	$('#compcode').combobox('reload','modul/company/companyaction.php?act=listcomp');

	});	
	*/
})
function formataging(aging,row){
	if (30 >= aging ){
		return '<span style="color:green;font-weight: bold;">0 - 30</span>';
		} 
	else if(31 >= aging) {
		return '<span style="color:blue;font-weight: bold;">31 - 60</span>';
		}
	else if(60 >= aging) {
		return '<span style="color:black;font-weight: bold;">61 - 90</span>';
	}
	else if(91 >= aging) {
		return '<span style="color:red;font-weight: bold;">90+</span>';
	}
}
function closed(cld){

$(cld).dialog({
  onBeforeClose:function(){
    var p = $(this);
    $.messager.confirm('Confirm','Are you sure you want to close?',function(r){
      if (r){
        var opts = p.panel('options');
        var onBeforeClose = opts.onBeforeClose;
        opts.onBeforeClose = function(){};
       $(cld).dialog('close');
        opts.onBeforeClose = onBeforeClose;
      }
    });
    return false;
  }
}); 
}

function formatPrice(val,row){
	var x = accounting.formatMoney(val);
		return '<span style="float:right">'+x+'</span>';
}

var statusx = [
		    {status:'open',statname:'Open'},
		    {status:'closed',statname:'Closed'}
		];
var statusmilestone = [
		    {status:'open',statname:'Open'},
		    {status:'closed',statname:'Closed'}
		];		
var products = [
		    {approval:'0',apptext:'Pending'},
			{approval:'1',apptext:'Approved'},
			{approval:'4',apptext:'Win'},
		    {approval:'5',apptext:'Loss'},
		    {approval:'2',apptext:'Reject'}
		];
	
var app_sm = [
			
			{approval:'4',apptext:'Win'},
		    {approval:'5',apptext:'Loss'}
		];		
var current = [
		    {currency:'IDR',name:'IDR'},
		    {currency:'USD',name:'USD'},
		    {currency:'AUD',name:'AUD'}
		];	
var category = [
		    {category:'Hardware'},
		    {category:'License'},
		    {category:'Maintenance'},
		    {category:'Hardware'},
		    {category:'Services'},
		    {category:'Subscription Fee'},
		    {category:'Other'}
		];	
var aging_data = [
		    {aging:'1',aging_text:'0 - 30'},
		    {aging:'2',aging_text:'30 - 60'},
		    {aging:'3',aging_text:'61 - 90'},
		];