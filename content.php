<?php
session_start();
include "config/koneksi.php";
include "control/class.php";
$company = new Master();
$_SESSION['sessionid'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Project 1</title>
	<link rel="stylesheet" type="text/css" href="assets/themes/metro-blue/easyui.css">
	<link rel="stylesheet" type="text/css" href="assets/themes/icon.css">

	<!--<link rel="stylesheet" type="text/css" href="demo.css">-->
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/1.5jquery.easyui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/validate/css/validationEngine.jquery.css"/>
	<script src="assets/validate/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
	<script src="assets/validate/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="assets/js/jquery.easyui.patch.js"></script>
	<script type="text/javascript" src="assets/src/jquery.combobox.js"></script>
	<script type="text/javascript" src="assets/src/jquery.dialog.js"></script>
	<script type="text/javascript" src="assets/src/datagrid-detailview.js"></script>
	<script type="text/javascript" src="assets/src/datagrid-scrollview.js"></script>
	<script type="text/javascript" src="assets/src/datagrid-bufferview.js"></script>
	<script type="text/javascript" src="assets/src/datagrid-groupview.js"></script>
	<script type="text/javascript" src="assets/src/datagrid-filter.js"></script>
	<script type="text/javascript" src="assets/src/datagrid-dnd.js"></script>
	<script type="text/javascript" src="assets/src/edatagrid.js"></script>
	<script type="text/javascript" src="assets/js/accounting.js"></script>
	<script type="text/javascript" src="modul/pivot/jquery.pivotgrid.js"></script>
	<script type="text/javascript" src="assets/js/glob.js"></script>

	
	<link rel="stylesheet" type="text/css" href="assets/css/all.css"/>
	<!--<script src="assets/editor/ckeditor.js" type="text/javascript"></script>
	<script src="assets/editor/adapters/jquery.js" type="text/javascript"></script>
	<script src="assets/ckfinder/ckfinder.js" type="text/javascript"></script>-->
<script type="text/javascript">

$(document).ready(function() {
	$('#do_hide').hide();
	$('#ho_hide').hide();
	$('#cp_hide').hide();		
	$('#th_hide').hide();			  
	$('#te_hide').hide();	
$.extend($.fn.textbox.methods, {
	show: function(jq){
		return jq.each(function(){
			$(this).next().show();
		})
	},
	hide: function(jq){
		return jq.each(function(){
			$(this).next().hide();
		})
	}
})
	$('#standart').hide();
	$('#custom').hide();
 $.fn.datebox.defaults.formatter=function(date){
        if (!date){return '';}
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
    },
   $.fn.datebox.defaults.parser=function(s){
        if (!s) return new Date();
        var ss = (s.split('-'));
        var y = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        var d = parseInt(ss[2],10);
        if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
            return new Date(y,m-1,d);
        } else {
            return new Date();
        }
    }
	
	
	$('#add').linkbutton('<?php echo $company->privilege('ins');?>');
	$('#delete').linkbutton('<?php echo $company->privilege('delet');?>');
	$('#upload').linkbutton('<?php echo $company->privilege('upload');?>');
	$('#srch').linkbutton('<?php echo $company->privilege('srch');?>');
	
	
	});	



</script>
<style>

 
select option:disabled {
	color: #000;
}
.ui-button { margin-left: -1px; }
.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; }

	 
</style>
<style>
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
	
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 10px;
        border-bottom:none;
    }
</style>
</head>

<?php

$ax = $_GET['act'];

if($_GET['act']=='sub_menu'){ 
include 'modul/sub_menu/sub_menu.php';
}

else if($_GET['act']=='tree'){ 
include 'modul/tree/tree.php';
} 
else if($_GET['act']=='create_user'){ 
include 'modul/user/createuser.php';
}
else if($_GET['act']=='user'){ 
include 'modul/user/user.php';
}

else if($_GET['act']=='level'){ 
include 'modul/level/level.php';
}

else if($_GET['act']=='upmodul'){ 
include 'modul/upmodul/upload.php';
}
else if($_GET['act']=='acl'){ 
include 'modul/tree/acl.php';
}
else if($_GET['act']=='profile'){ 
include 'modul/user/detuser.php';
}
else if($_GET["act"]=="$ax"){ 
$q = mysql_query ("select * from tbl_menu where id = '$ax'");
while ($z = mysql_fetch_array($q)){

include  "modul/$z[menu]/$z[file_mod]";

}

} 
else {?>


<?php 
} 
 ?></html>